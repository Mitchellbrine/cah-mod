package mc.Mitchellbrine.cah.game;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Card;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.cardcast.CardcastReader;
import mc.Mitchellbrine.cah.network.BlackCardPacket;
import mc.Mitchellbrine.cah.network.CardPacket;
import mc.Mitchellbrine.cah.network.GamePacket;
import mc.Mitchellbrine.cah.network.LastSubmittedCardPacket;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.network.WhiteCardsPacket;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.Color;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Everything here MUST be handled on the server. If it isn't, it can cause issues with client-server desync. #PACKETSFTW
 * Created by Mitchellbrine on 2015.
 */
public class CAHGame {

	private List<Deck> decks;
	private String currentCard = "";
	
	private String host;

	private List<List<Card>> playedCards;
	private Map<String,int[]> indexesToRemove;

	private List<Card> possibleWhiteCards;
	private List<String> possibleBlackCards;
	private Map<String,Integer> blackCardPicks;
	private int inputNeeded;
	private Map<String, Integer> inputPlayed;

	private List<String> players;
	private Map<String, Integer> playerPoints;
	private Map<String, List<Card>> playerCards;
	private int cardCzar = -1;

	private World world;

	private int gameState = -1;
	public RoundState roundState = RoundState.NOT_FOUND;

	private int MAX_POINTS = 8;
	private static final int MAX_PLAYERS = 8;
	
	private static Random random;

	private static final CardcastReader reader = new CardcastReader();
	
	private static final Style gold = Style.EMPTY.withColor(Color.fromLegacyFormat(TextFormatting.GOLD));

	public CAHGame(World world) {
		decks = new ArrayList<Deck>();
		players = new ArrayList<String>();
		this.world = world;
		gameState = 0;
		random = new Random(world.getGameTime());
	}

	public void setMaxPoints(int max) {
		this.MAX_POINTS = max;
	}

	public void startGame() {
		buildDeck();
		broadcastMessage("Game beginning in 5 seconds!");
		scheduler.schedule(new Runnable() {
			@Override
			public void run() {
				System.out.println("Run the runnable!");
				startNewRound();
				gameState = 1;
			}
		}, 5, TimeUnit.SECONDS);
	}

	public void buildDeck() {
		possibleWhiteCards = new ArrayList<Card>();
		possibleBlackCards = new ArrayList<String>();
		blackCardPicks = new HashMap<String,Integer>();
		for (Deck deck : decks) {
			for (Card card : deck.getWhiteCards()) {
				possibleWhiteCards.add(card);
			}
			System.out.println("Added all the white cards from " + deck.getName());
			for (String card : deck.getBlackCards()) {
				possibleBlackCards.add(card);
				blackCardPicks.put(card, deck.getBlackCardPick(card));
			}
			System.out.println("Added all the black cards from " + deck.getName());
		}
	}

	public void startNewRound() {
		CAHMod.logger.info("Started new round method");
		//Map<String,List<String>> cardsToRemove = new HashMap<String, List<String>>();
		if (playedCards == null) {
			CAHMod.logger.info("New cards!");
			playedCards = new ArrayList<List<Card>>();
			playerPoints = new HashMap<String, Integer>();
			inputPlayed = new HashMap<String, Integer>();
			for (String player : players) {
				playerPoints.put(player,0);
			}
			playerCards = new HashMap<String, List<Card>>();
			indexesToRemove = new HashMap<String,int[]>();
		} else {

			/*for (List<Card> cardList : playedCards) {
				List<String> strings = new ArrayList<String>();
				String player = "";
				for (Card card : cardList) {
					if (players.contains(card.getCardHolder())) {
						strings.add(card.getCardMessage());
						player = card.getCardHolder();
					}
				}
				cardsToRemove.put(player,strings);
			}
			playedCards.clear();
			inputPlayed.clear();
		}

		for (String player : cardsToRemove.keySet()) {
			List<Card> cardsToRemoveFromInv = new ArrayList<Card>();
			for (String card : cardsToRemove.get(player)) {
				if (playerCards.containsKey(player)) {
					for (Card card1 : playerCards.get(player)) {
						if (card1.getCardMessage().equalsIgnoreCase(card)) {
							cardsToRemoveFromInv.add(card1);
						}
					}
				}
			}
			if (playerCards.containsKey(player)) {
				List<Card> cards = new ArrayList<Card>(playerCards.get(player));
				for (Card card : cardsToRemoveFromInv) {
					cards.remove(card);
				}
				playerCards.remove(player);
				playerCards.put(player,cards);
			} */

			playedCards.clear();
			inputPlayed.clear();
		}
			
			for (String player : playerCards.keySet()) {
				if (!getCzar().equalsIgnoreCase(player) && indexesToRemove.get(player) != null) {
					List<Card> cardsToRemove = new ArrayList<Card>();
					
					for (int i : indexesToRemove.get(player)) {
						System.out.println(player + " " + i);
						if (i >= 0) {
							possibleWhiteCards.add(playerCards.get(player).get(i));
							//playerCards.get(player);
							cardsToRemove.add(playerCards.get(player).get(i));
						}
					}
					
					for (Card card : cardsToRemove) {
						playerCards.get(player).remove(card);
					}
					
				}
			}
			
			CAHMod.logger.info(indexesToRemove.size());
			
			indexesToRemove.clear();

		CAHMod.logger.info("We have reached the point where cards should start to be rewarded.");

		Random pRandom = new Random(world.getGameTime());
		
		for (String player : players) {
			ArrayList<Card> playersCards = new ArrayList<Card>();
			CAHMod.logger.info("Starting awarding cards to " + player);
			if (playerCards.containsKey(player)) {
				playersCards = (ArrayList<Card>)playerCards.get(player);
				for (int i = 0; i < (8 - playersCards.size()); i++) {
					int cardIndex = pRandom.nextInt(possibleWhiteCards.size());
					playersCards.add(possibleWhiteCards.get(cardIndex));
					possibleWhiteCards.remove(cardIndex);
					CAHMod.logger.info("For player (" + player + "), we have added a new card.");
				}
			} else {
				CAHMod.logger.info("Player doesn't exist, starting cards distribution.");
				for (int i = 0; i < 8; i++) {
					CAHMod.logger.info("Starting finding card #" + i);
					CAHMod.logger.info("Possible White Cards to Select From:" + possibleWhiteCards.size());
					int cardIndex = pRandom.nextInt(possibleWhiteCards.size());
					CAHMod.logger.info(player + " got the index " + cardIndex);
					playersCards.add(possibleWhiteCards.get(cardIndex));
					possibleWhiteCards.remove(cardIndex);
					CAHMod.logger.info("Player " + player + " got a card with index " + cardIndex);
				}
			}
			playerCards.put(player, playersCards);
		}
		
		roundState = RoundState.PLAYING_CARDS;
		
		CAHMod.logger.info("Round State is now reset to beginning.");

		Random blackCardRandom = new Random(world.getGameTime());

		int index = blackCardRandom.nextInt(possibleBlackCards.size());

		this.currentCard = possibleBlackCards.get(index);
		
		CAHMod.logger.info("Black Card has been chosen.");

		if (this.blackCardPicks.get(this.currentCard) != null) {
			inputNeeded = this.blackCardPicks.get(this.currentCard);
		} else {
			inputNeeded = StringUtils.countMatches(this.currentCard,"______");
		
				if (inputNeeded == 0) {
					inputNeeded = StringUtils.countMatches(this.currentCard, "_____");
			
					if (inputNeeded == 0) {
						inputNeeded = StringUtils.countMatches(this.currentCard, "____");
				
						if (inputNeeded == 0) {
							inputNeeded = StringUtils.countMatches(this.currentCard, "___");
							
							if (inputNeeded == 0) {
					
							inputNeeded = StringUtils.countMatches(this.currentCard, "_");
								
								if (inputNeeded == 0)
									inputNeeded = 1;
							
							}
						}
					}	
				}
		}

		//broadcastMessage("Made it past the input needed, got " + inputNeeded);
		
		if (cardCzar < 0) {
			cardCzar = 0;
		} else {
			if (cardCzar < (players.size() - 1)) {
				cardCzar++;
			} else {
				cardCzar = 0;
			}
		}

		sendMessage("Console",players.get(cardCzar),"You are the Card Czar!");

		for (int i = 0; i < players.size();i++) {
			if (cardCzar == i) continue;

			sendMessage("Console",players.get(i),"The Card Czar is now " + players.get(cardCzar));
		}

		if (inputNeeded > 1) {
			broadcastMessage("This round requires you to play " + inputNeeded + " cards.");
		}

		for (String playerName : players) {
			System.out.println(playerName);
			if (!playerName.startsWith("~Bot")) {
				ServerPlayerEntity player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(playerName);			
				PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), new CardPacket(playerPoints.get(playerName),playerCards.get(playerName)));
				PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new GamePacket(true,gameState,roundState,cardCzar,cardCzar == players.indexOf(playerName)));
				PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new BlackCardPacket(currentCard,false));
				PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new WhiteCardsPacket(playedCards.size()));
				PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new LastSubmittedCardPacket(null));
				System.out.println("Sent the packets for " + player.getScoreboardName());
			} else {
				System.out.println("Assessing bot named " + playerName);
				// The bots choose their cards.
				for (int i = 0; i < inputNeeded;i++) {
					System.out.println(playerName + " has started to play card #" + (i + 1));
					playCard(playerName, random.nextInt(playerCards.get(playerName).size()));
					System.out.println("Finished playing card #" + (i + 1));
				}
				System.out.println("Got through bot named " + playerName);
			}
		}
		CAHMod.logger.info("Got through the round setup.");
	}

	public void endRound(List<Card> cards) {
		String winner = null;
		for (String player : players) {
			if (player.equalsIgnoreCase(cards.get(0).getCardHolder())) {
				winner = player;
			}
		}
		if (winner != null) {
			roundState = RoundState.POST_ROUND;
			if (playerPoints.containsKey(winner)) {
				broadcastMessage("The winning cards were: ");
				for (Card card : cards) {
					broadcastMessage("- " + card.getCardMessage());
				}
				if (playerPoints.get(winner) + 1 >= MAX_POINTS) {
					broadcastMessage(TextFormatting.GOLD + winner + " has won the game!");
					int newPoints = playerPoints.get(winner) + 1;
					playerPoints.remove(winner);
					playerPoints.put(winner,newPoints);
					endGame();
				} else {
					broadcastMessage(winner + " won the round!");
					int newPoints = playerPoints.get(winner) + 1;
					playerPoints.remove(winner);
					playerPoints.put(winner,newPoints);
					//CAHMod.logger.info("Reached the Schedule Method");
					scheduleNextRound();
				}
			} else {
				broadcastMessage(winner + " won the round!");
				playerPoints.put(winner,1);
				scheduleNextRound();
			}
		}
		for (List<Card> cardList : playedCards) {
			for (Card whiteCard : cardList) {
				whiteCard.removeCardBearer();
				this.possibleWhiteCards.add(whiteCard);
			}
		}
	}

	public void addPlayer(String username) {
		if (players.contains(username))
			sendMessage("Console",username,"You already joined the game, silly!");
		else {
			if (players.size() == MAX_PLAYERS) {
				sendMessage("Console", username, "The max amount of players has been reached!");
			} else {
				players.add(username);
				if (playerCards != null && !playerCards.containsKey(username)) {
					ArrayList<Card> playersCards = new ArrayList<Card>();
					for (int i = 0; i < 8; i++) {
						int cardIndex = new Random(world.getGameTime()).nextInt(possibleWhiteCards.size());
						playersCards.add(possibleWhiteCards.get(cardIndex));
						possibleWhiteCards.remove(cardIndex);
					}
					playerCards.put(username, playersCards);
				}
				broadcastMessage(username + " has joined the game");
				/**if (players.size() >= 3 && gameState == 0) {
					startGame();
				}*/
			}
		}
	}

	public void removePlayer(String username) {
		players.remove(username);

		if (playerCards != null && playerCards.containsKey(username)) {
			for (Card card : playerCards.get(username)) {
				possibleWhiteCards.add(card);
			}
		}
		if (playerPoints != null && playerPoints.containsKey(username)) {
			playerPoints.remove(username);
		}
		if (inputPlayed != null && inputPlayed.containsKey(username)) {
			ArrayList<List<Card>> cardsToRemove = new ArrayList<List<Card>>();
			for (List<Card> cardList : playedCards) {
				for (Card card : cardList) {
					if (card.getCardHolder().equalsIgnoreCase(username)) {
						cardsToRemove.add(cardList);
					}
				}
			}
			for (List<Card> card : cardsToRemove) {
				for (Card cards : card) {
					cards.removeCardBearer();
					possibleWhiteCards.add(cards);
				}
				playedCards.remove(card);
			}
			inputPlayed.remove(username);
		}
		broadcastMessage(username + " has left the game");
		invalidatePlayer(username);

		if (cardCzar == players.indexOf(username) && players.size() > 0) {

			if (cardCzar < 0) {
				cardCzar = 0;
			} else {
				if (cardCzar < players.size() - 1) {
					cardCzar++;
				} else {
					cardCzar = 0;
				}
			}

			sendMessage("Console",players.get(cardCzar),username + " has logged out, making you the Card Czar!");
			if (inputPlayed != null && inputPlayed.containsKey(players.get(cardCzar))) {
				ArrayList<List<Card>> cardsToRemove = new ArrayList<List<Card>>();
				for (List<Card> cardList : playedCards) {
					for (Card card : cardList) {
						if (card.getCardHolder().equalsIgnoreCase(players.get(cardCzar))) {
							cardsToRemove.add(cardList);
						}
					}
				}
				List<Card> playersCards = playerCards.get(players.get(cardCzar));
				for (List<Card> card : cardsToRemove) {
					for (Card cards : card) {
						cards.removeCardBearer();
						playersCards.add(cards);
					}
					playedCards.remove(card);
				}
				playerCards.remove(players.get(cardCzar));
				playerCards.put(players.get(cardCzar),playersCards);
				inputPlayed.remove(players.get(cardCzar));
			}
		}
		if (players.size() < 3) {
			broadcastMessage("There are not enough players to continue. Ending the game!");
			endGame();
		}
	}

	public void broadcastMessage(String message) {
		for (String player : players) {
			if (!player.startsWith("~Bot"))
				ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).sendMessage(new TranslationTextComponent("cah.broadcast", message),ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).getUUID());
		}
	}

	public void sendMessage(String sender, String player, String message) {
		if (!player.startsWith("~Bot"))
			ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).sendMessage(new TranslationTextComponent("cah.message",sender, message),ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).getUUID());
	}

	public void sendMessage(String sender, String player, String message, boolean gold) {
		if (gold) {
			if (!player.startsWith("~Bot"))
				ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).sendMessage(new TranslationTextComponent("cah.message", sender, message).setStyle(CAHGame.gold),ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).getUUID());
		} else {
			if (!player.startsWith("~Bot"))
				ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).sendMessage(new TranslationTextComponent("cah.message", sender, message),ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player).getUUID());
		}
	}

	final ScheduledExecutorService scheduler =
		Executors.newScheduledThreadPool(1);

	private void endGame() {
		scheduler.schedule(new Runnable() {
			@Override
			public void run() {
				invalidateAllPlayers();
				CAHMod.game = null;
				//System.out.println("Nullified the game.");
			}
		}, 5, TimeUnit.SECONDS);
	}

	public void playCard(String player, int index) {
		if (index >= playerCards.get(player).size()) {
			ServerPlayerEntity playerEntity = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player);			
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> playerEntity), new CardPacket(playerPoints.get(player),playerCards.get(player)));
			CAHMod.logger.error("Received erroneous card play, making sure " + player + " is up to date.");
			return;
		}
		playCard(player,playerCards.get(player).get(index));
	}
	
	public void playCard(String player, int index, String message) {
		if (playerCards.get(player) == null || playerCards.get(player).get(index) == null || !playerCards.get(player).get(index).getCardMessage().equalsIgnoreCase(message)) {
			ServerPlayerEntity playerEntity = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player);			
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> playerEntity), new CardPacket(playerPoints.get(player),playerCards.get(player)));
			CAHMod.logger.error("Received erroneous card play, making sure " + player + " is up to date.");
			return;
		}
		playCard(player,index);
	}

	public void playCard(String player, Card card) {
		if (card == null) {
			CAHMod.logger.fatal("Could not retrieve card");
			return;
		}
		if (inputPlayed.containsKey(player) && inputPlayed.get(player) == inputNeeded) {
			sendMessage("Console",player,"You have already played your cards this round!");
			return;
		}
		if (cardCzar == (players.indexOf(player))) {
			sendMessage("Console",player,"You are the Card Czar!");
			return;
		}
		
		/**
		 * This is how we submit multiple cards.
		 * 
		 * The following checks if we have submitted a card already and, if so, gets what the card is we have.
		 */
		
		List<Card> cardList = new ArrayList<Card>();
		boolean isNew = true;

		for (List<Card> cards : playedCards) {
			boolean markForBreak = false;
			for (Card card1 : cards) {
				if (card1.getCardHolder().equalsIgnoreCase(player)) {
					cardList = new ArrayList<Card>(cards);
					markForBreak = true;
					isNew = false;
					break;
				}
			}
			if (markForBreak) {
				break;
			}
		}
			
		/**
		 * This next step removes the current card from the ones that have been played.
		 */

		if (!isNew) {
			playedCards.remove(cardList);
			System.out.println("Removed player card list");
		}

		/**
		 * This officially sets the bearer of the new card to be played as the player submitting it.
		 * 
		 * It then adds this new card to the list.
		 */
		
		card.setCardBearer(player);
		cardList.add(card);

		/**
		 * With that, we officially add the card to the list of cards that have been played.
		 */
		
		playedCards.add(cardList);
		
		/**
		 * Oh, and we send it back to the client so they can get an accurate reading.
		 */
		
		if (!player.startsWith("~Bot")) {

			List<String> cardsForPacket = new ArrayList<String>();
			
			for (Card cardForPacket : cardList) {
				cardsForPacket.add(cardForPacket.getCardMessage());
			}
			ServerPlayerEntity playerEntity = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player);
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> playerEntity),new LastSubmittedCardPacket(cardsForPacket));
		}

		/**
		 * This checks how many cards have now been played and sets the new number if the number exists.
		 * 
		 * NOTE: This may be able to be replaced with the inputPlayed.replace(player,input) in the future.
		 */
		
		if (inputPlayed.containsKey(player)) {
			int input = inputPlayed.get(player) + 1;
			inputPlayed.replace(player, input);
		} else {
			inputPlayed.put(player,1);
		}

		/**
		 * Now, we handle the inputs that we need.
		 */
		
		int[] intArray = new int[inputNeeded];

		for (int i = 0; i < inputNeeded;i++) {
			intArray[i] = -1;
		}
		
		//System.out.println("Input needed: " + intArray.length);

		if (indexesToRemove.containsKey(player)) {
			for (int i = 0; i < indexesToRemove.get(player).length;i++) {
				intArray[i] = indexesToRemove.get(player)[i];
			}
			for (int i = 0; i < intArray.length;i++) {
				if (intArray[i] == -1) {
					intArray[i] = playerCards.get(player).indexOf(card);
					break;
				}
			}
		} else {
			if (playerCards.get(player) != null && intArray.length > 0)
				intArray[0] = playerCards.get(player).indexOf(card);
			else if (intArray.length > 0) {
				CAHMod.logger.fatal("For some reason, the cards for the player don't... exist?");
			} else if (playerCards.get(player) != null) {
				CAHMod.logger.fatal("Something else is afoot.");
			} else {
				CAHMod.logger.fatal("The array of player cards... don't exist?");
			}
		}
		indexesToRemove.put(player,intArray);

		CAHMod.game.sendMessage("Console", player, "You have submitted a card");

		// Now after everything, we check if we've reached the end.

		int amountOfReady = 0;

		for (Integer inputs : inputPlayed.values()) {
			if (inputs == inputNeeded) {
				amountOfReady++;
			}
		}

		if (amountOfReady == players.size() - 1) {
			finalCardInputted();
		}
	}

	public void finalCardInputted() {
		/**
		 * This is where I need to take all the cards from played cards out and get a random one, add it to played cards, and remove it from the index
		 */

		List<List<Card>> newList = new ArrayList<List<Card>>(playedCards);
		playedCards.removeAll(newList);

		Random random = new Random(world.getGameTime());

		do {
			int index = random.nextInt(newList.size());
			playedCards.add(newList.get(index));
			newList.remove(index);
		} while (newList.size() > 0);

		sendMessage("Console",players.get(cardCzar),"The cards are ready for you! Press 'Show Cards' to choose!",true);
		roundState = RoundState.CHOOSING_CARDS;
		if (!players.get(cardCzar).startsWith("~Bot")) {
			ServerPlayerEntity player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(players.get(cardCzar));
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), new GamePacket(true,gameState,roundState,cardCzar,true));
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), new WhiteCardsPacket(playedCards.size()));
		} else {
			endRound(playedCards.get(random.nextInt(playedCards.size())));
		}
	}

	public void addDeck(String id) {
		Deck deck = reader.loadSet(id);
		if (deck != null) {
			decks.add(deck);
		}
	}

	private void scheduleNextRound() {
		//CAHMod.logger.info("Reached schedule method");
		scheduler.schedule(new Runnable() {
			@Override
			public void run() {
				//CAHMod.logger.info("Executed the runnable");
				startNewRound();
			}
		}, 5, TimeUnit.SECONDS);
		//CAHMod.logger.info("Scheduled the runnable");
	}

	public List<Card> getPlayerCards(String playerName) {
		return playerCards.get(playerName);
	}

	public List<Card> getPlayedCards(int index) {
		return playedCards.get(index);
	}

	public boolean hasCards(String playerName) {
		return playerCards != null && playerCards.containsKey(playerName);
	}

	public boolean playerExists(String username) {
		return players.contains(username);
	}

	public String getBlackCard() {
		return this.currentCard;
	}

	// a.k.a. Force Quit
	protected void invalidateAllPlayers() {
		for (String player : players) {
			invalidatePlayer(player);
		}
	}

	private void invalidatePlayer(String player) {
		if (!player.startsWith("~Bot")) {
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player)), new GamePacket(false,-1,RoundState.NOT_FOUND,-1,false));
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player)), new CardPacket(0,null));
			PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(player)), new BlackCardPacket("missingno",false));
		}
	}

	public boolean playerHasInputted(String username) {
		return inputPlayed != null && inputPlayed.containsKey(username) && inputPlayed.get(username) >= inputNeeded;
	}

	public int getAmountInputted() {
		return playedCards != null ? playedCards.size() : -1;
	}

	public String getCzar() {
		return cardCzar != -1 ? players.get(cardCzar) : "";
	}

	public int getCzarIndex() { return cardCzar; }

	public int getPlayerPoints(String user) {
		return playerPoints.get(user);
	}

	public void setHost(String playerName) {
		host= playerName;
	}

	public String getHost() {
		return host;
	}
	
	public int getGameState() {
		return gameState;
	}
	
	public boolean setDecks(List<Deck> newDecks) {
		this.decks = newDecks;
		return decks.size() == newDecks.size();
	}
	
	public static enum RoundState {
		PLAYING_CARDS,
		CHOOSING_CARDS,
		POST_ROUND,
		NOT_FOUND;
		
		public static RoundState getState(int i) {
			switch (i) {
				case 0:
					return PLAYING_CARDS;
				case 1:
					return CHOOSING_CARDS;
				case 2:
					return POST_ROUND;
				case -1:
					return NOT_FOUND;
				default:
					return NOT_FOUND;
			}
		}
		
		public static int toInt(RoundState state) {
			switch (state) {
				case PLAYING_CARDS:
					return 0;
				case CHOOSING_CARDS:
					return 1;
				case POST_ROUND:
					return 2;
				case NOT_FOUND:
					return -1;
				default:
					return -1;
			}
		}
		
		@Override
		public String toString() {
			String defaultString = "NOT FOUND";
			switch (this) {
				case PLAYING_CARDS:
					return "Start of Round";
				case CHOOSING_CARDS:
					return "Choosing Winning Card";
				case POST_ROUND:
					return "End of Round";
				case NOT_FOUND:
					return defaultString;
				default:
					return defaultString;
			}
		}
	}
	
	@Override
	public String toString() {
		return "Cards vs. Crafters | " + players.size() + " players, " + possibleBlackCards.size() + " black cards, " + possibleWhiteCards.size() + " black cards | Game State: " + gameState;
	}

}
