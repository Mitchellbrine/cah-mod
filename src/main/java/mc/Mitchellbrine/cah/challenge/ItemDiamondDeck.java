package mc.Mitchellbrine.cah.challenge;

import net.minecraft.item.Foods;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.Color;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TranslationTextComponent;

public class ItemDiamondDeck extends ItemBaseDeck {

	public ItemDiamondDeck(String material) {
		super(material,new Item.Properties().tab(ItemGroup.TAB_MISC).stacksTo(1).food(Foods.GOLDEN_APPLE));
	}
	
	@Override
	public ITextComponent getName(ItemStack p_200295_1_) { // 0xC8FFFF 
		return new TranslationTextComponent(this.getDescriptionId(p_200295_1_)).withStyle(Style.EMPTY.withColor(Color.fromRgb(0xC8FFFF)));
	}

}
