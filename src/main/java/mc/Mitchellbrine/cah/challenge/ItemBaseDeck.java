package mc.Mitchellbrine.cah.challenge;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DrinkHelper;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

public class ItemBaseDeck extends Item {
	
	public String deckMaterial;
	
	public ItemBaseDeck(String material, Item.Properties properties) {
		super(properties);
		deckMaterial = material;
	}
	
	public int getUseDuration(ItemStack p_77626_1_) {
		return 32;
	}

	public UseAction getUseAnimation(ItemStack p_77661_1_) {
		return UseAction.BOW;
	}

	public ItemStack finishUsingItem(ItemStack p_77654_1_, World p_77654_2_, LivingEntity p_77654_3_) {
		PlayerEntity playerentity = p_77654_3_ instanceof PlayerEntity ? (PlayerEntity)p_77654_3_ : null;
		if (playerentity instanceof ServerPlayerEntity) {
			CriteriaTriggers.CONSUME_ITEM.trigger((ServerPlayerEntity)playerentity, p_77654_1_);
		}

			if (!playerentity.getPersistentData().contains("challengepacks")) {
				CompoundNBT challengePacks = new CompoundNBT();
				playerentity.getPersistentData().put("challengepacks",challengePacks);
			}
			
			if (!playerentity.getPersistentData().getCompound("challengepacks").contains(((ItemBaseDeck)p_77654_1_.getItem()).deckMaterial)) {
				playerentity.getPersistentData().getCompound("challengepacks").putBoolean(((ItemBaseDeck)p_77654_1_.getItem()).deckMaterial,true);
			} else {
				if (!playerentity.getPersistentData().getCompound("challengepacks").getBoolean(((ItemBaseDeck)p_77654_1_.getItem()).deckMaterial)) {
					playerentity.getPersistentData().getCompound("challengepacks").putBoolean(((ItemBaseDeck)p_77654_1_.getItem()).deckMaterial,true);
				}
			}
		return p_77654_1_;
	}
	
	public ActionResult<ItemStack> use(World p_77659_1_, PlayerEntity p_77659_2_, Hand p_77659_3_) {
		return DrinkHelper.useDrink(p_77659_1_, p_77659_2_, p_77659_3_);
	}


}
