package mc.Mitchellbrine.cah.challenge;

import mc.Mitchellbrine.cah.util.References;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
//import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemResources {

	private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, References.MODID);
	
	public static final RegistryObject<Item> deckIron = ITEMS.register("deckiron", () -> new ItemIronDeck("iron"));
	public static final RegistryObject<Item> deckGold = ITEMS.register("deckgold", () -> new ItemGoldDeck("gold"));
	public static final RegistryObject<Item> deckEmerald = ITEMS.register("deckemerald", () -> new ItemEmeraldDeck("emerald"));
	public static final RegistryObject<Item> deckDiamond = ITEMS.register("deckdiamond", () -> new ItemDiamondDeck("diamond"));
	
	public static void registerItems() {
		/**
		 * Shh, not yet.
		 * 
		 * ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
		 */
	}
	
}
