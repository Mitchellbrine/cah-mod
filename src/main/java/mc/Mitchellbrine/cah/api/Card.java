package mc.Mitchellbrine.cah.api;

/**
 * Created by Mitchellbrine on 2015.
 */
public class Card {

	private String cardMessage;
	private String cardBearer;
	private Deck deck;

	public Card(String cardMessage, Deck deck) {
		this.cardMessage = cardMessage;
		this.deck = deck;
	}

	public void setCardBearer(String username) {
		this.cardBearer = username;
	}

	public String getCardHolder() {
		return this.cardBearer;
	}

	public String getCardMessage() {
		return this.cardMessage;
	}
	
	public Deck getDeck() {
		return this.deck;
	}

	public void removeCardBearer() {
		this.cardBearer = "";
	}

}
