package mc.Mitchellbrine.cah.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraftforge.fml.loading.FMLPaths;

public class PackDatabase {

	public String id, name;
	
	private String baseURL, packsJsonPath, decksPath;
	
	protected Map<Integer, Deck> cardPacks;
	protected Map<String, Deck> cardPacksOld;
	
	public static final String officialBaseURL = "https://bitbucket.org/Mitchellbrine/cah-mod/raw/HEAD/official_packs.json";
	public static final String challengeBaseURL = "https://bitbucket.org/Mitchellbrine/cah-mod/raw/HEAD/challenge_packs.json";
	public static final String localBaseURL = FMLPaths.GAMEDIR.get().toString() + File.separatorChar;
	protected static final String defaultPackJson = "packs.json";
	protected static final String defaultPackFolder = "packs/";
		
	public PackDatabase(String id, String name, String baseURL, String packsJsonPath, String decksPath) {
		this.id = id;
		this.name = name;
		this.baseURL = baseURL;
		this.packsJsonPath = packsJsonPath;
		this.decksPath = decksPath;
		
		this.cardPacks = new HashMap<Integer,Deck>();
		this.cardPacksOld = new HashMap<String,Deck>();
		
	}
	
	public PackDatabase(String id, String name, String baseURL, boolean collectedJSON) {
		this.id = id;
		this.name = name;
		this.baseURL = baseURL;
		this.packsJsonPath = "";
		this.decksPath = "";
		
		this.cardPacks = new HashMap<Integer,Deck>();
		this.cardPacksOld = new HashMap<String,Deck>();
		
	}
	
	public PackDatabase(String id, String name, String baseURL, String decksPath) {
		this(id,name,baseURL,defaultPackJson,decksPath);
	}
	
	public PackDatabase(String id, String name, String baseURL) {
		this(id,name,baseURL,defaultPackJson,defaultPackFolder);
	}
	
	public PackDatabase(String id, String baseURL) {
		this(id,id,baseURL,defaultPackJson,defaultPackFolder);
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPackJSONFile() {
		return this.baseURL + this.packsJsonPath;
	}
	
	public String getPackFolder() {
		return this.baseURL + this.decksPath;
	}
	
	@SuppressWarnings({"rawtypes","unchecked"})
	public List<Deck> getDecks() {
		ArrayList<Deck> decks = (ArrayList<Deck>)new ArrayList(this.cardPacks.values());
		decks.addAll(cardPacksOld.values());
		return decks;
	}
		
	public Deck getDeckFromID(String ID) {
		return Integer.getInteger(ID) != null ? cardPacks.get(Integer.getInteger(ID)) : cardPacksOld.get(ID);
	}
	
	public Deck getDeckFromID(int ID) {
		return cardPacks.get(ID);
	}
	
	public Deck getDeckFromName(String name) {
		for (Deck deck : cardPacks.values()) {
			//System.out.println("Seeking: " + name + " | Found: " + deck.getName());
			if (deck.getName().equalsIgnoreCase(name))
				return deck;
		}
		for (Deck deck : cardPacksOld.values()) {
			//System.out.println("Seeking: " + name + " | Found: " + deck.getName());
			if (deck.getName().equalsIgnoreCase(name))
				return deck;
		}
		return null;
	}

	public void addDeck(Deck deck) {
		if (deck != null) {
			if (deck.getCode().equalsIgnoreCase("missingno"))
				this.cardPacks.put(deck.getId(), deck);
			else
				this.cardPacksOld.put(deck.getCode(), deck);
		}
	}
	
}
