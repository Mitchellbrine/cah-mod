package mc.Mitchellbrine.cah.api;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import mc.Mitchellbrine.cah.CAHMod;

import java.util.List;
import java.util.Map;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class CardpackHandler {

	private static final int GET_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(3);
	private static final JsonParser parser = new JsonParser();

	//private static boolean generated;
	
	private static final Logger logger = LogManager.getLogger("CvC-Cardpack Handler");
	
	public List<PackDatabase> packDatabases;
	
	public static CardpackHandler INSTANCE;
	
	public CardpackHandler() {
		INSTANCE = this;
		packDatabases = new ArrayList<PackDatabase>();
		//generated = false;
	}

	public static void registerCardpack(PackDatabase pack) {
		 if (!INSTANCE.packDatabases.add(pack))
			 logger.error("Pack Database could not be registered");
	}
	
	/**
	 * This should ONLY be launched after CommonSetupEvent and only on FMLClientSetupEvent and ONLY be CvC
	 */
	public static void finalGenerate() {
		for (PackDatabase base : INSTANCE.packDatabases) {
			if (!generatePacks(base))
				logger.error(String.format("Deck couldn't be generated from database %s",base.id));
		}
	}
	
	private static boolean generatePacks(PackDatabase database) {
		
		String decksJSON = null;
		
		try {
			decksJSON = INSTANCE.getUrlContent(database.getPackJSONFile());
		} catch (IOException ex) {
		}
			if (decksJSON == null) {
				logger.error("Database didn't exist");
				return false;
			}
			
			//System.out.println(decksJSON);
			//System.out.println(parser.parse(decksJSON));
		
		try {
			JsonObject base = parser.parse(decksJSON).getAsJsonObject();

			boolean isCombined = false;
			
			if (base.get("iscombined") != null)
				isCombined = base.get("iscombined").getAsBoolean();

			if (!isCombined) {
				logger.debug("Pack at " + decksJSON + " is not combined. Going through normal route.");
			JsonArray packs = base.get("packs").getAsJsonArray();
			
			for (JsonElement element : packs) {
				if (element != null && element.getAsJsonObject() != null) {
					JsonObject pack = element.getAsJsonObject();
					
					String id = pack.get("id").getAsString();
					
					logger.debug("Currently working on pack with ID " + id);
					
					try {
						String deckFile = INSTANCE.getUrlContent(database.getPackFolder() + id + ".json");
						
						if (deckFile == null) {
							logger.debug(String.format("Pack file for (%s) didn't exist, skipping...", id));
						}
						
						JsonObject deckJSON = parser.parse(deckFile).getAsJsonObject();
						
						final JsonObject packDetails = deckJSON.get("pack").getAsJsonObject();
						
						final String name = packDetails.get("name").getAsString();
						final String ID = packDetails.get("id").getAsString();
						if (name == null || ID == null || name.isEmpty() || !id.equalsIgnoreCase(ID)) {
							logger.debug(String.format("Name or ID of pack under previous ID %s does not exist...",id));
							continue;
						}
						final Deck deck = new Deck(name, ID).setDatabase(database);
						
						final JsonElement packDescription = packDetails.get("description");
						
						if (packDescription != null)
							deck.setDescription(packDescription.getAsString());

						final JsonElement packAuthors = packDetails.get("authors");
						
						if (packAuthors != null) {
							JsonArray authorsArray = packAuthors.getAsJsonArray();
							String[] authorStrings = new String[authorsArray.size()];
							
							for (int i = 0; i < authorsArray.size();i++) {
								authorStrings[i] = "" + authorsArray.get(i);
							}
							
							deck.setCreator(authorStrings);
						}
						
						final JsonArray blackCards = deckJSON.get("black").getAsJsonArray();
						if (blackCards != null) {
							for (JsonElement black : blackCards) {
								String text = ((JsonObject) black).get("content").getAsString();
								Integer pick = ((JsonObject)black).get("pick").getAsInt();
								deck.getBlackCards().add(text);
								deck.getBlackCardPicks().put(text, pick);
							}
						}

						final JsonArray whiteCards = (JsonArray) deckJSON.get("white");
						if (whiteCards != null) {
							//List<String> strs = new ArrayList<String>();
							for (JsonElement white : whiteCards) {
										String cardCastString = ((JsonPrimitive)white).getAsString();
										StringBuilder newString = new StringBuilder();

										newString.append(cardCastString.substring(0, 1).toUpperCase());
										newString.append(cardCastString.substring(1));

										if (Character.isLetterOrDigit(cardCastString.charAt(cardCastString.length() - 1))) {
											newString.append('.');
										}

										//strs.add(newString.toString());
									//}
									//String text = StringUtils.join(strs, "");
									Card card = new Card(newString.toString(),deck);
									deck.getWhiteCards().add(card);
								//}
							}
						}
						if (Integer.getInteger(ID) != null) {
							database.cardPacks.put(Integer.getInteger(ID), deck);
							System.out.println("Registered a deck in database " + database.id + " with internal ID of " + deck.getId());
						} else {
							database.cardPacksOld.put(ID, deck);
							logger.debug("Added pack with ID " + ID + " to older cardpacks map");
						}
					} catch (IOException ex2) {
						logger.error(String.format("Pack (%s) threw an error, skipping...",id));
						continue;
					}
					
				}
			}
		} else {
			JsonArray packs = base.get("decks").getAsJsonArray();
			
			for (int i = 0; i < packs.size(); i++) {
				JsonElement element = packs.get(i);
				if (element != null && element.getAsJsonObject() != null) {
					JsonObject pack = element.getAsJsonObject();
					
					final String name = pack.get("name").getAsString();
					if (name == null) {
						logger.error(String.format("Name of pack does not exist..."));
						continue;
					}
					final Deck deck = new Deck(name, i).setDatabase(database);
					
					//logger.debug("Created new deck with name of " + name + " and ID of " + i);
					
					final JsonArray blackCards = pack.get("black").getAsJsonArray();
					if (blackCards != null) {
						for (JsonElement black : blackCards) {
							String text = ((JsonObject) black).get("text").getAsString();
							Integer pick = ((JsonObject)black).get("pick").getAsInt();
							deck.getBlackCards().add(text);
							deck.getBlackCardPicks().put(text, pick);
						}
					}

					final JsonArray whiteCards = (JsonArray) pack.get("white");
					if (whiteCards != null) {
						//List<String> strs = new ArrayList<String>();
						for (JsonElement white : whiteCards) {
									JsonObject whiteCard = (JsonObject)white;
									String cardCastString = whiteCard.get("text").getAsString();
									StringBuilder newString = new StringBuilder();

									newString.append(cardCastString.substring(0, 1).toUpperCase());
									newString.append(cardCastString.substring(1));

									if (Character.isLetterOrDigit(cardCastString.charAt(cardCastString.length() - 1))) {
										newString.append('.');
									}

									//strs.add(newString.toString());
								//}
								//String text = StringUtils.join(strs, "");
								Card card = new Card(newString.toString(),deck);
								deck.getWhiteCards().add(card);
							//}
						}
					}
					
					if (pack.get("official") != null) {
						deck.setOfficial(pack.get("official").getAsBoolean());
						//logger.info("Wow, %s is an official pack!",deck.getName());
					}
					
					database.cardPacks.put(i, deck);
					logger.info("Registered deck in " + database.id + " with ID " + i + " (" + name + ")");
					
				}
			}
		}
			
		} catch (JsonIOException ex2) {
			ex2.printStackTrace();
			return false;
		}
		return true;
	}
	
	private String getUrlContent(final String urlStr) throws IOException {
		
		InputStream is;
		
		if (!urlStr.startsWith("https://") && !urlStr.startsWith("http://") ) {
			String fileStr = urlStr;
			if (fileStr.startsWith("file://") || fileStr.startsWith("file:\\")) {
				fileStr = fileStr.substring(7);
			}
			File file = new File(fileStr);
			is = new FileInputStream(file);
		} else {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(false);
		conn.setRequestMethod("GET");
		conn.setInstanceFollowRedirects(true);
		conn.setReadTimeout(GET_TIMEOUT);
		conn.setConnectTimeout(GET_TIMEOUT);

		final int code = conn.getResponseCode();
		if (HttpURLConnection.HTTP_OK != code) {
			CAHMod.logger.error(String.format("Got HTTP response code %d from AllBadCards for %s", code, urlStr));
			return null;
		}
		
		
		/**String contentType = conn.getContentType();
		if (!"application/json".equals(contentType)) {
			CAHMod.logger.error(String.format("Got content-type %s from Cardcast for %s", contentType, urlStr));
			return null;
		}*/

		is = conn.getInputStream();
		}
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(isr);
		StringBuilder builder = new StringBuilder(4096);
		String line;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append('\n');
		}
		reader.close();
		isr.close();
		is.close();

		return builder.toString();
	}
	
	public PackDatabase getDatabase(String id) {
		for (PackDatabase pack : INSTANCE.packDatabases)
			if (pack.id.equalsIgnoreCase(id))
				return pack;
		return null;
	}
	
	public void scanLocalPacks() {
		
		String cardpacksJsonPath = PackDatabase.localBaseURL + "cardpacks-local.json";
		File cardpacksJsonFile = new File(cardpacksJsonPath);
		
		String cardpackFolderPath = PackDatabase.localBaseURL + "card-packs" + File.separator + "local" + File.separator;
		File cardpackFolder = new File(cardpackFolderPath);

		boolean existed = true;
		
		if (!cardpackFolder.exists()) {
			cardpackFolder.mkdirs();
			existed = false;
		}

		List<String> localPackIDs = new ArrayList<String>();
		Map<String, String> localPackNames = new HashMap<String, String>();
		Map<String, Integer> localPackWhiteCards = new HashMap<String, Integer>();
		Map<String, Integer> localPackBlackCards = new HashMap<String, Integer>();
		
		Map<String, String[]> localPackAuthors = new HashMap<String, String[]>();
		Map<String, String> localPackDescription = new HashMap<String, String>();
		Map<String, String> localPackCredits = new HashMap<String, String>();

		
		if (existed) {
		File[] cardpackFiles = cardpackFolder.listFiles();		
		
		for (File file : cardpackFiles) {
			if (!file.getName().endsWith(".json"))
				continue;
			
			if (file.isDirectory())
				continue;
				
			try {			
				String fileContents = INSTANCE.getUrlContent(file.getPath());

				JsonObject deckJSON = parser.parse(fileContents).getAsJsonObject();
				
				final JsonObject packDetails = deckJSON.get("pack").getAsJsonObject();
				
				final String name = packDetails.get("name").getAsString();
				final String ID = packDetails.get("id").getAsString();
				if (name == null || ID == null || name.isEmpty() || !(ID + ".json").equalsIgnoreCase(file.getName())) {
					logger.error(String.format("Name or ID of pack under previous ID %s does not exist...",ID));
					continue;
				}
				
				localPackIDs.add(ID);
				localPackNames.put(ID, name);
				
				final JsonArray blackCards = deckJSON.get("black").getAsJsonArray();
				if (blackCards != null) {
					localPackBlackCards.put(ID, blackCards.size());
				}

				final JsonArray whiteCards = (JsonArray) deckJSON.get("white");
				if (whiteCards != null) {
					localPackWhiteCards.put(ID, whiteCards.size());
				}
				
				// Additional (optional) information
				
				final String description = packDetails.get("description").getAsString();
				
				if (description != null)
					localPackDescription.put(ID, description);
				
				final JsonArray authorsJSON = packDetails.get("authors").getAsJsonArray();
				
				if (authorsJSON != null) {
					String[] authors = new String[authorsJSON.size()];
					for (int i = 0; i < authorsJSON.size();i++) {
						if (authorsJSON.get(i).getAsString() != null) {
							authors[i] = authorsJSON.get(i).getAsString();
						}
					}
				}
				
				final String credits = packDetails.get("credits").getAsString();
				
				if (credits != null)
					localPackCredits.put(ID, credits);
				
				CardpackHandler.logger.info(String.format("Successfully loaded %s pack. There are now %s packs loaded locally.", ID,localPackIDs.size()));	
			} catch (Exception ex) {
				CardpackHandler.logger.error(String.format("Could not read deck %s, skipping...", file.getName()));
				continue;
			}
		}
		
		try {
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			
			cardpacksJsonFile.createNewFile();
			PrintWriter writer = new PrintWriter(cardpacksJsonFile);
			JsonObject obj = new JsonObject();
			JsonArray packs = new JsonArray();
			
			obj.addProperty("iscombined", false);
		
			for (String packID : localPackIDs) {
				JsonObject pack = new JsonObject();
				if (localPackNames.get(packID) == null || localPackBlackCards.get(packID) == null || localPackWhiteCards.get(packID) == null)
					continue;
			
				pack.addProperty("name", localPackNames.get(packID));
				pack.addProperty("id", packID);
				
				if (localPackDescription.get(packID) != null)
					pack.addProperty("description", localPackDescription.get(packID));

				if (localPackAuthors.get(packID) != null) {
					JsonArray authors = new JsonArray();
					for (String author : localPackAuthors.get(packID)) {
						authors.add(author);
					}
					
					pack.add("authors", authors);
				}
				
				if (localPackCredits.get(packID) != null)
					pack.addProperty("credits", localPackCredits.get(packID));
				
				JsonObject quantity = new JsonObject();
				quantity.addProperty("black", localPackBlackCards.get(packID));
				quantity.addProperty("white", localPackWhiteCards.get(packID));
				quantity.addProperty("total", localPackBlackCards.get(packID) + localPackWhiteCards.get(packID));
				pack.add("quantity", quantity);
			
				packs.add(pack);
			}
			
			obj.add("packs", packs);
			
			JsonElement je = jp.parse(obj.toString());
		
			writer.println(gson.toJson(je));
			writer.close();
			
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} else { //CardpackHandler.logger.info("Let's get creative!"); 
		}
		// And now that we have everything in place.
		
		CardpackHandler.registerCardpack(new PackDatabase("local","Local Packs",PackDatabase.localBaseURL,"cardpacks-local.json","card-packs" + File.separator + "local" + File.separator));
	}
	
	public void scanClientPacks(String clientID, String clientName) {
		
		String cardpacksJsonPath = PackDatabase.localBaseURL + "cardpacks-" + clientID + ".json";
		File cardpacksJsonFile = new File(cardpacksJsonPath);
		
		String cardpackFolderPath = PackDatabase.localBaseURL + "card-packs" + File.separator + clientID + File.separator;
		File cardpackFolder = new File(cardpackFolderPath);

		boolean existed = true;
		
		if (!cardpackFolder.exists()) {
			cardpackFolder.mkdirs();
			existed = false;
		}

		List<String> localPackIDs = new ArrayList<String>();
		Map<String, String> localPackNames = new HashMap<String, String>();
		Map<String, Integer> localPackWhiteCards = new HashMap<String, Integer>();
		Map<String, Integer> localPackBlackCards = new HashMap<String, Integer>();
		
		Map<String, String[]> localPackAuthors = new HashMap<String, String[]>();
		Map<String, String> localPackDescription = new HashMap<String, String>();
		Map<String, String> localPackCredits = new HashMap<String, String>();

		
		if (existed) {
		File[] cardpackFiles = cardpackFolder.listFiles();		
		
		for (File file : cardpackFiles) {
			if (!file.getName().endsWith(".json"))
				continue;
			
			if (file.isDirectory())
				continue;
				
			try {			
				String fileContents = INSTANCE.getUrlContent(file.getPath());

				JsonObject deckJSON = parser.parse(fileContents).getAsJsonObject();
				
				final JsonObject packDetails = deckJSON.get("pack").getAsJsonObject();
				
				final String name = packDetails.get("name").getAsString();
				final String ID = packDetails.get("id").getAsString();
				if (name == null || ID == null || name.isEmpty() || !(ID + ".json").equalsIgnoreCase(file.getName())) {
					logger.error(String.format("Name or ID of pack under previous ID %s does not exist...",ID));
					continue;
				}
				
				localPackIDs.add(ID);
				localPackNames.put(ID, name);
				
				final JsonArray blackCards = deckJSON.get("black").getAsJsonArray();
				if (blackCards != null) {
					localPackBlackCards.put(ID, blackCards.size());
				}

				final JsonArray whiteCards = (JsonArray) deckJSON.get("white");
				if (whiteCards != null) {
					localPackWhiteCards.put(ID, whiteCards.size());
				}
				
				// Additional (optional) information
				
				final JsonElement description = packDetails.get("description");
				
				if (description != null)
					localPackDescription.put(ID, description.getAsString());
				
				final JsonElement authorsJSON = packDetails.get("authors");
				
				if (authorsJSON != null) {
					JsonArray JSONauthors = authorsJSON.getAsJsonArray();
					String[] authors = new String[JSONauthors.size()];
					for (int i = 0; i < JSONauthors.size();i++) {
						if (JSONauthors.get(i).getAsString() != null) {
							authors[i] = JSONauthors.get(i).getAsString();
						}
					}
				}
				
				final JsonElement credits = packDetails.get("credits");
				
				if (credits != null)
					localPackCredits.put(ID, credits.getAsString());
				
				CardpackHandler.logger.info(String.format("Successfully loaded %1$s pack. There are now %2$s packs loaded.", ID,localPackIDs.size()));	
			} catch (Exception ex) {
				CardpackHandler.logger.error(String.format("Could not read deck %1$s, skipping...", file.getName()));
				ex.printStackTrace();
				continue;
			}
		}
		
		try {

			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			
			if (cardpacksJsonFile.exists()) {
				File oldVersion = new File(cardpacksJsonPath.substring(cardpacksJsonPath.length() - 5).concat("-old.json"));
				if (oldVersion.exists()) {
					CardpackHandler.logger.info("Deleting old version of " + clientID + " database.");
					oldVersion.delete();
				}
				cardpacksJsonFile.renameTo(oldVersion);
			}
			
			cardpacksJsonFile.createNewFile();
			PrintWriter writer = new PrintWriter(cardpacksJsonFile);
			JsonObject obj = new JsonObject();
			JsonArray packs = new JsonArray();
			
			obj.addProperty("iscombined", false);
		
			for (String packID : localPackIDs) {
				JsonObject pack = new JsonObject();
				if (localPackNames.get(packID) == null || localPackBlackCards.get(packID) == null || localPackWhiteCards.get(packID) == null)
					continue;
			
				pack.addProperty("name", localPackNames.get(packID));
				pack.addProperty("id", packID);
				
				if (localPackDescription.get(packID) != null)
					pack.addProperty("description", localPackDescription.get(packID));

				if (localPackAuthors.get(packID) != null) {
					JsonArray authors = new JsonArray();
					for (String author : localPackAuthors.get(packID)) {
						authors.add(author);
					}
					
					pack.add("authors", authors);
				}
				
				if (localPackCredits.get(packID) != null)
					pack.addProperty("credits", localPackCredits.get(packID));
				
				JsonObject quantity = new JsonObject();
				quantity.addProperty("black", localPackBlackCards.get(packID));
				quantity.addProperty("white", localPackWhiteCards.get(packID));
				quantity.addProperty("total", localPackBlackCards.get(packID) + localPackWhiteCards.get(packID));
				pack.add("quantity", quantity);
			
				packs.add(pack);
			}
			
			obj.add("packs", packs);
			
			JsonElement je = jp.parse(obj.toString());
		
			writer.println(gson.toJson(je));
			writer.close();
			
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} else { // CardpackHandler.logger.info("Let's get creative!");
		}
		// And now that we have everything in place.
		
		CardpackHandler.registerCardpack(new PackDatabase(clientID,clientName,PackDatabase.localBaseURL,"cardpacks-" + clientID + ".json","card-packs" + File.separator + clientID + File.separator));
	}

	public void scanClientPacks(String clientID) {
		scanClientPacks(clientID, clientID);
	}

	
}
