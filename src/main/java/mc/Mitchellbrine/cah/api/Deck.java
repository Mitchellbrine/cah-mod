package mc.Mitchellbrine.cah.api;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Mitchellbrine on 2015.
 */
public class Deck {
	private final String name;
	private final String code;
	private String description;
	private String[] creator;
	private String databaseID;
	private Integer databaseIndex;
	private final Set<String> blackCards = new HashSet<String>();
	private final Map<String,Integer> blackCardPicks = new HashMap<String,Integer>();
	private final Set<Card> whiteCards = new HashSet<Card>();
	public static final String[] BLANK_CREATOR = new String[] {"missingno"};
	private boolean official;

	public Deck(final String name, final String code, final String description) {
		this.name = name;
		this.code = code;
		this.description = description;
		this.creator = BLANK_CREATOR;
		this.official = false;
	}
	
	public Deck(final String name, final String code) {
		this.name = name;
		this.code = code;
		this.description = "missingno";
		this.creator = BLANK_CREATOR;
		this.official = false;
	}
	
	public Deck(final String name, final String code, final String description, final boolean isOfficial) {
		this.name = name;
		this.code = code;
		this.description = description;
		this.creator = BLANK_CREATOR;
		this.official = isOfficial;
	}
	
	public Deck(final String name, final String code, final boolean isOfficial) {
		this.name = name;
		this.code = code;
		this.description = "missingno";
		this.creator = BLANK_CREATOR;
		this.official = isOfficial;
	}
	
	public Deck(final String name, final Integer index, final String description) {
		this.name = name;
		this.code = "missingno";
		this.description = description;
		this.databaseIndex = index;
		this.creator = BLANK_CREATOR;
		this.official = false;
	}
	
	public Deck(final String name, final Integer index) {
		this.name = name;
		this.code = "missingno";
		this.description = "missingno";
		this.databaseIndex = index;
		this.creator = BLANK_CREATOR;
		this.official = false;
	}
	
	public Deck(final String name, final Integer index, final String description, boolean isOfficial) {
		this.name = name;
		this.code = "missingno";
		this.description = description;
		this.databaseIndex = index;
		this.creator = BLANK_CREATOR;
		this.official = isOfficial;
	}
	
	public Deck(final String name, final Integer index, boolean isOfficial) {
		this.name = name;
		this.code = "missingno";
		this.description = "missingno";
		this.databaseIndex = index;
		this.creator = BLANK_CREATOR;
		this.official = isOfficial;
	}

	public int getId() {
		return databaseIndex != null ? databaseIndex : -1;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isActive() {
		return true;
	}

	public boolean isBaseDeck() {
		return false;
	}

	public int getWeight() {
		return Integer.MAX_VALUE;
	}

	public Set<String> getBlackCards() {
		return blackCards;
	}

	public Set<Card> getWhiteCards() {
		return whiteCards;
	}
	
	public String[] getCreator() {
		return creator;
	}
	
	public Map<String,Integer> getBlackCardPicks(){
		return blackCardPicks;
	}
	
	public Integer getBlackCardPick(String card) {
		return blackCardPicks.get(card);
	}
	
	public String[] setCreator(String[] newCreator) {
		return creator = newCreator;
	}
	
	public String getCreators() {
        String[] creatorList = creator;
        String creator = "";
        if (creatorList.length > 1) {
        	creator = creator + creatorList[0].substring(1, creatorList[0].length() - 1);
        	for (int i = 1; i < creatorList.length;i++) {
        		creator = creator + ", " + creatorList[i].substring(1, creatorList[i].length() - 1);
        	}
        } else if (creatorList.length == 1) {
        	creator = creatorList[0].substring(1, creatorList[0].length() - 1);
        }
        return creator;
	}
	
	public String setDescription(String desc) {
		return description = desc;
	}
	
	public Deck setDatabase(PackDatabase pack) {
		return setDatabase(pack.id);
	}
	
	public Deck setDatabase(String packID) {
		this.databaseID = packID;
		return this;
	}
	
	public String getDatabaseID() {
		return this.databaseID != null ? this.databaseID : "missingno";
	}
	
	public Deck setDeckIndex(Integer index) {
		this.databaseIndex = index;
		return this;
	}
	
	public boolean isOfficial() {
		return this.official;
	}
	
	boolean setOfficial(boolean isOfficial) {
		this.official = isOfficial;
		return this.official;
	}
	
}
