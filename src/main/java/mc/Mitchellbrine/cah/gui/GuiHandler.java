package mc.Mitchellbrine.cah.gui;

import mc.Mitchellbrine.cah.gui.creation.GuiCreateGame;
import mc.Mitchellbrine.cah.gui.ingame.GuiCardChoices;
import mc.Mitchellbrine.cah.gui.ingame.GuiControlScreen;
import mc.Mitchellbrine.cah.gui.ingame.GuiPlayerCards;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GuiHandler {

	public static final class IDS {
		public static final int CONTROL = 0;
		public static final int CARDS = 1;
		public static final int CHOICE = 2;
		public static final int CREATE = 3;
	}

	public static void openGui(int id, PlayerEntity player) {
		switch (id) {
			case IDS.CONTROL:
				Minecraft.getInstance().setScreen(new GuiControlScreen(player));
				break;
			case IDS.CARDS:
				Minecraft.getInstance().setScreen(new GuiPlayerCards(player));
				break;
			case IDS.CHOICE:
				Minecraft.getInstance().setScreen(new GuiCardChoices(player));
				break;
			case IDS.CREATE:
				Minecraft.getInstance().setScreen(new GuiCreateGame(player));
				break;
		}
	}

}
