package mc.Mitchellbrine.cah.gui.creation;

import mc.Mitchellbrine.cah.api.CardpackHandler;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.api.PackDatabase;
import mc.Mitchellbrine.cah.cardcast.CardcastReader;
import mc.Mitchellbrine.cah.client.CAHCachedData;
import mc.Mitchellbrine.cah.network.GameCreationWithCodesPacket;
import mc.Mitchellbrine.cah.network.PacketHandler;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.client.gui.widget.button.Button.ITooltip;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.INBT;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.matrix.MatrixStack;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GuiCreateGame extends Screen {

	private PlayerEntity player;

	private Button doneButton;
	//private TextFieldWidget codeText;
	private TextFieldWidget maxPlayers;
	//private Button addButton;
	private Button cancelButton;
	private Button officialPacks;
	private Button localPacks;
	private Button onlinePacks;
	@SuppressWarnings({"unused"})
	private Button challengePacks;
	
	private TextFieldWidget bots;
	
	//private String currentDeck;
	private String currentMax;
	private String botsNum;

	private List<Deck> decks;
	
	//private List<Deck> featuredDecks;
	
	private GuiCreateGame game;

	final CardcastReader reader = new CardcastReader();

	private static final String[] both = new String[]{"You cannot create this game for the following reasons: ","","- You do not have any decks loaded","and","- A game is already in-session"};
	private static final String[] noDecks = new String[] {"You cannot create this game for the following reason:","","- You do not have any decks loaded."};
	private static final String[] isGame = new String[] {"You cannot create this game for the following reason:","","- A game is already in session"};
	
	
	public GuiCreateGame(PlayerEntity player) {
		super(new StringTextComponent("Create CAH Game"));
		this.player = player;
		decks = new ArrayList<Deck>();
		//featuredDecks = new ArrayList<Deck>();
		
		//System.out.println(PackDatabase.localBaseURL);
	}

	
	public void init() {
		super.init();
	    this.minecraft.keyboardHandler.setSendRepeatsToGui(true);

		addButton(doneButton = new Button(this.width / 2 - 4 - 150, this.height / 4 + 120 + 35, 150, 20, new TranslationTextComponent("gui.done"),new createGame(), new checkForErrors()));
		addButton(cancelButton = new Button(this.width / 2 + 4, this.height / 4 + 120 + 35, 150, 20, new TranslationTextComponent("gui.cancel"),new cancelGame()));

		/**this.codeText = new TextFieldWidget(font, this.width / 2 - 75, 10,150,20,"CAH Codes");
		this.codeText.setMaxStringLength(5);
		codeText.setCanLoseFocus(true);
		codeText.setResponder((responder) -> {
			this.currentDeck = codeText.getText();
		});
		children.add(codeText);**/
		
		this.maxPlayers = new TextFieldWidget(font,this.width / 2 -25, this.height / 4 + 120 + 10, 20, 20,new StringTextComponent("Set Max Points"));
		this.maxPlayers.setMaxLength(2);
		this.maxPlayers.setVisible(true);
		maxPlayers.setCanLoseFocus(true);
		this.maxPlayers.setValue(this.currentMax == null || this.currentMax.isEmpty() ? "8" : this.currentMax);
		maxPlayers.setResponder((responder) -> {
			this.currentMax = maxPlayers.getValue();
		});
		children.add(maxPlayers);
		
		bots = new TextFieldWidget(font,this.width / 2 + 5, this.height / 4 + 120 + 10, 20, 20,new StringTextComponent("Set Bots Num"));
		this.bots.setMaxLength(2);
		this.bots.setValue(this.botsNum == null || this.botsNum.isEmpty() ? "0" : this.botsNum);
		//this.bots.setVisible(player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		//this.bots.setEditable(player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		this.bots.setCanLoseFocus(true);
		bots.setResponder((responder) -> {
			this.botsNum = bots.getValue();
		});
		children.add(bots);
		
		//this.setFocusedDefault(codeText);
		
		//addButton(addButton = new Button(this.width / 2 + 77,10,75,20,I18n.format("gui.cah.addPack"), new addPack()));
		
		addButton(officialPacks = new Button(this.width / 2 - 22 - 46,15,45,20,new StringTextComponent("Official"), new openOfficialPacks()));
		/**this.officialPacks.visible = (player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		this.officialPacks.active = (player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		*/
		addButton(localPacks = new Button(/**this.width / 2 +2*/ (this.width / 2) - 22,15,45,20,new StringTextComponent("Local"), new openLocalPacks()));
		/**this.localPacks.visible = (player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		this.localPacks.active = (player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		*/
	
		addButton(onlinePacks = new Button(/**this.width / 2 + 49*/ (this.width / 2) + 22 + 2,15,45,20,new StringTextComponent("Online"),new openOnlinePacks()));
		
		/**if (FeaturedPacks.getFeaturedPacks() != null) {
			for (Deck deck : FeaturedPacks.getFeaturedPacks()) {
				if (deck != null && !featuredDecks.contains(deck))
					featuredDecks.add(deck);
			}
		}*/
		
		INBT challengeNBT = player.getPersistentData().get("challengepacks");
		
		if (challengeNBT != null || player.getName().getString().equalsIgnoreCase("Dev")) {
			addButton(challengePacks = new Button((this.width / 2) + 45 + 2,15,45,20,new StringTextComponent("Challenge"),new openChallengePacks()));
			onlinePacks.x = (this.width / 2) + 1;
			localPacks.x = (this.width / 2) - 46;
			officialPacks.x = (this.width / 2) - (46 * 2);
		}
		
		markGuiDirty();
	}

	public void render(MatrixStack stack, int p_73863_1_, int p_73863_2_, float p_73863_3_) {
		this.renderBackground(stack);
		
		//this.scriptLine.drawTextBox();

		//drawRect(this.width / 2 - 151, 50, this.width / 2 + 152, 188, -16777216);
		
		//codeText.render(p_73863_1_, p_73863_2_, p_73863_3_);
		maxPlayers.render(stack, p_73863_1_, p_73863_2_, p_73863_3_);
		bots.render(stack, p_73863_1_, p_73863_2_, p_73863_3_);
		
		font.drawShadow(stack, maxPlayers.getMessage(), maxPlayers.x - font.width(maxPlayers.getMessage()) - 15, maxPlayers.y + (maxPlayers.getHeight() / 4), 0xFFFFFF);
		font.drawShadow(stack,bots.getMessage(), bots.x + bots.getWidth() + 15, bots.y + (bots.getHeight() / 4), 0xFFFFFF);
		
		String createGame = "Create Game:";
		font.drawShadow(stack,new StringTextComponent(createGame), (this.width / 2) - (font.width(createGame) / 2), 5, 0xFFFFFF);
		
		String packsAdded = "Packs Added:";
		font.drawShadow(stack,new StringTextComponent(packsAdded), (this.width / 2) - (font.width(packsAdded) / 2), 40, 0xFFFFFF);
		
		int yPosition = 52;
		/**for (Deck deck : game.featuredDecks) {
			font.drawStringWithShadow("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)", this.width / 2 - 150, yPosition, /*0x3ADF00*/ /**0xFFFFFF);
			yPosition += 10;			
		}*/
		
		
		/**for (Deck deck : CardpackHandler.INSTANCE.getDatabase("official").getDecks()) {
			font.drawStringWithShadow("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)", this.width / 2 - 150, yPosition, /*0x3ADF00*/ /**0xFFFFFF);
			yPosition += 10;
		}*/
		
		for (Deck deck : game.decks) {
			font.drawShadow(stack,new StringTextComponent("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)"), this.width / 2 - 150, yPosition, /*0x3ADF00*/ 0xFFFFFF);
			yPosition += 10;
		}

		super.render(stack,p_73863_1_, p_73863_2_, p_73863_3_);
		
		if (p_73863_1_ >= maxPlayers.x && p_73863_1_ <= (maxPlayers.x + maxPlayers.getWidth()) && p_73863_2_ >= maxPlayers.y && p_73863_2_ <= (maxPlayers.y + maxPlayers.getHeight())) {
			List<String> strings = new ArrayList<String>();
			strings.add("Max Points");
			renderStringTooltip(stack,strings,p_73863_1_,p_73863_2_);
		}
		
		if (bots.isMouseOver(p_73863_1_, p_73863_2_)) {
			renderTooltip(stack,new StringTextComponent("# of Bots"), p_73863_1_,p_73863_2_);
		}
		
	}

	private void renderStringTooltip(MatrixStack stack, List<String> asList, int p_73863_1_, int p_73863_2_) {
		List<ITextComponent> componentList = new ArrayList<ITextComponent>();
		for (String string : asList) {
			componentList.add(new StringTextComponent(string));
		}
		renderComponentTooltip(stack,componentList,p_73863_1_,p_73863_2_);
		
	}


	public void tick()
	{
		//this.scriptLine.updateCursorCounter();
		super.tick();
		//codeText.tick();
		maxPlayers.tick();
		bots.tick();
		this.doneButton.active = (this.decks.size() > 0) && !CAHCachedData.serverExists;
		if (!this.maxPlayers.getValue().isEmpty()) {
			try {
				Integer.parseInt(this.currentMax);
				
				if (Integer.parseInt(this.currentMax) < 1)
					this.maxPlayers.setValue("1");
			} catch (NumberFormatException ex) {
				this.maxPlayers.setValue("8");
			}
		}
		
		if (!this.bots.getValue().isEmpty()) {
			try {
				Integer.parseInt(this.botsNum);
				
				if (Integer.parseInt(this.botsNum) < 0)
					this.bots.setValue("0");
			} catch (NumberFormatException ex) {
				this.bots.setValue("0");
			}
		}
	}
	

	public boolean keyPressed(int charac, int arg2, int arg3) {
		//codeText.keyPressed(charac, arg2, arg3);
		maxPlayers.keyPressed(charac, arg2, arg3);
		bots.keyPressed(charac, arg2, arg3);
		if (charac == GLFW.GLFW_KEY_ESCAPE) {
			cancelButton.onPress();
			return true;
		} else if (charac == GLFW.GLFW_KEY_ENTER) {
			/**if (!this.codeText.getText().isEmpty()) {
				addButton.onPress();
			}
			else {**/
				doneButton.onPress();

			//}
			return true;
		} else {
			return super.keyPressed(charac, arg2, arg3);
		}
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	protected class createGame implements IPressable {

		@Override
		public void onPress(Button button) {
			//List<String> packCodes = new ArrayList<String>();
			/**for (Deck deck : decks) {
				//System.out.println("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)");
				packCodes.add(deck.getCode());
			}*/
			//PacketHandler.INSTANCE.sendToServer(new GameCreationPacket(player.getScoreboardName(),Integer.parseInt(currentMax),botsNum != null ? Integer.parseInt(botsNum) : 0,packCodes));
			if (decks.size() > 0 && doneButton.active) {
				PacketHandler.INSTANCE.sendToServer(new GameCreationWithCodesPacket(player.getScoreboardName(),Integer.parseInt(currentMax),botsNum != null ? Integer.parseInt(botsNum) : 0, decks));
				minecraft.setScreen(null);
			}

		}
		
	}
	
	/**protected class addPack implements IPressable {

		@Override
		public void onPress(Button button) {
			//if (!codeText.getText().isEmpty()) {
				//loadDeck(codeText);
			//}
		}
		
	}
	
	/**public Deck loadDeck(String deckCode) {
		return reader.loadSet(deckCode);
	}*/
	
	public Deck loadDeck(Deck deck) {
		if (deck != null) {
			decks.add(deck);
			markGuiDirty();
			return deck;
		}
		return null;
	}
	
	public Deck removeDeck(Deck deck) {
		if (deck != null && decks.contains(deck)) {
			decks.remove(deck);
			markGuiDirty();
			return deck;
		}
		return null;
	}
	
	/**
	private Deck loadDeck(TextFieldWidget textField) {
		Deck deck = reader.loadSet(textField.getText());
		if (deck != null) {
			decks.add(deck);
			textField.setText("");
			markGuiDirty();
		}
		return deck;
	}**/
	
	public List<Deck> getDecks() {
		return decks;
	}
	
	protected class cancelGame implements IPressable {

		@Override
		public void onPress(Button button) {
			onClose();
			minecraft.setScreen(null);
		}
		
	}
	
	protected class GuiTextFieldButton extends Button {
		
		public TextFieldWidget textField;
		public IPressable hook;
		
		public GuiTextFieldButton(TextFieldWidget field, Button.IPressable iHook) {
			super(field.x,field.y,field.getWidth(),field.getHeight(),new StringTextComponent(""), iHook);
		}
					
		}

	public class utilizeGuiTextHooks implements IPressable {
	
		public TextFieldWidget textF;
	
		public utilizeGuiTextHooks(TextFieldWidget text) {
			textF = text;
		}
	
		public void onPress(Button button) {
			textF.onClick(minecraft.mouseHandler.xpos(),minecraft.mouseHandler.ypos());
		}
	}
	
	public class openOfficialPacks implements IPressable {
		public void onPress(Button button) {
			//minecraft.displayGuiScreen(new GuiofficialPacks(player,game));
			PackDatabase database = CardpackHandler.INSTANCE.getDatabase("official");
			minecraft.setScreen(new GuiDatabaseSelection(player,database.name,GuiCreateGame.this,database));
		}
	}
	
	public class openLocalPacks implements IPressable {
		public void onPress(Button button) {
			//minecraft.displayGuiScreen(new GuiofficialPacks(player,game));
			PackDatabase database = CardpackHandler.INSTANCE.getDatabase("local");
			minecraft.setScreen(new GuiDatabaseSelection(player,database.name,GuiCreateGame.this,database));
		}
	}
	
	public class openOnlinePacks implements IPressable {
		public void onPress(Button button) {
			PackDatabase database = CardpackHandler.INSTANCE.getDatabase("online");
			minecraft.setScreen(new GuiOnlineDatabaseSelection(player,"online",GuiCreateGame.this,database));
		}
	}
	
	public class openChallengePacks implements IPressable {
		public void onPress(Button button) {
			PackDatabase database = CardpackHandler.INSTANCE.getDatabase("challenge");
			minecraft.setScreen(new GuiChallengeDatabaseSelection(player,"challenge",GuiCreateGame.this,database));
		}
	}
	
	public class checkForErrors implements ITooltip {

		@Override
		public void onTooltip(Button button, MatrixStack stack, int mouseX, int mouseY) {
			
			if (!button.active) {
				if (decks.size() == 0) {
					if (!CAHCachedData.serverExists) {
						renderStringTooltip(stack,Arrays.asList(noDecks), mouseX, mouseY);
					} else {
						renderStringTooltip(stack,Arrays.asList(both), mouseX, mouseY);
					}
				} else {
					if (CAHCachedData.serverExists) {
						renderStringTooltip(stack,Arrays.asList(isGame), mouseX, mouseY);						
					}
				}
			}
			
		}
		
	}
	
	public void markGuiDirty() {
		this.game = this;
	}

}
