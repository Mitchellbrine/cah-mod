package mc.Mitchellbrine.cah.gui.creation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.api.PackDatabase;
import mc.Mitchellbrine.cah.gui.component.ChallengeDeckList;
import mc.Mitchellbrine.cah.gui.component.ChallengeDeckList.ChallengeDeckEntry;
import mc.Mitchellbrine.cah.gui.component.GuiCreationExtension;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.EnchantmentNameParts;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.LanguageMap;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.client.gui.ScrollPanel;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.loading.StringUtils;

public class GuiChallengeDatabaseSelection extends GuiCreationExtension {

	private static String stripControlCodes(String value) { return net.minecraft.util.StringUtils.stripColor(value); }

    private enum SortType implements Comparator<Deck>
    {
        NORMAL,
        A_TO_Z{ @Override protected int compare(String name1, String name2){ return name1.compareTo(name2); }},
        Z_TO_A{ @Override protected int compare(String name1, String name2){ return name2.compareTo(name1); }};

        Button button;
        protected int compare(String name1, String name2){ return 0; }
        @Override
        public int compare(Deck o1, Deck o2) {
            String name1 = StringUtils.toLowerCase(stripControlCodes(o1.getCode()));
            String name2 = StringUtils.toLowerCase(stripControlCodes(o2.getCode()));
            return compare(name1, name2);
        }

        ITextComponent getButtonText() {
            return new TranslationTextComponent("fml.menu.mods."+StringUtils.toLowerCase(name()));
        }
    }
    
    private static final int PADDING = 6;
    
	public PackDatabase database;

	private final List<Deck> unsortedDecks;
	private List<Deck> decks; 
    private InfoPanel modInfo;
    private ChallengeDeckList.ChallengeDeckEntry selected = null;
    private int listWidth;
    private Button addDeckButton;
    private Button removeDeckButton;
    private ChallengeDeckList deckList;

    private int buttonMargin = 1;
    private int numButtons = SortType.values().length;
    private String lastFilterText = "";

    private TextFieldWidget search;

    private boolean sorted = false;
    private SortType sortType = SortType.NORMAL;
    
    public boolean iron,gold,emerald,diamond;

	
	public GuiChallengeDatabaseSelection(PlayerEntity player, String guiName, GuiCreateGame parent, PackDatabase database) {
		super(player, guiName, parent);
		
		this.database = database;
		this.unsortedDecks = Collections.unmodifiableList(database.getDecks());
		this.decks = Collections.unmodifiableList(this.unsortedDecks);
	}
	
    class InfoPanel extends ScrollPanel {
        private List<IReorderingProcessor> lines = Collections.emptyList();

        InfoPanel(Minecraft mcIn, int widthIn, int heightIn, int topIn)
        {
            super(mcIn, widthIn, heightIn, topIn, deckList.getRight() + PADDING);
        }

        void setInfo(List<String> lines)
        {
            this.lines = resizeContent(lines);
        }

        void clearInfo()
        {
            this.lines = Collections.emptyList();
        }

        private List<IReorderingProcessor> resizeContent(List<String> lines)
        {
            List<IReorderingProcessor> ret = new ArrayList<IReorderingProcessor>();
            for (String line : lines)
            {
                if (line == null)
                {
                    ret.add(null);
                    continue;
                }

                ITextComponent chat = ForgeHooks.newChatWithLinks(line, false);
                int maxTextLength = this.width - 12;
                if (maxTextLength >= 0)
                {
                    ret.addAll(LanguageMap.getInstance().getVisualOrder(font.getSplitter().splitLines(chat, maxTextLength, Style.EMPTY)));                }
            }
            return ret;
        }

        @Override
        public int getContentHeight()
        {
            int height = 50;
            height += (lines.size() * font.lineHeight);
            if (height < this.bottom - this.top - 8)
                height = this.bottom - this.top - 8;
            return height;
        }

        @Override
        protected int getScrollAmount()
        {
            return font.lineHeight * 3;
        }

        @SuppressWarnings("deprecation")
		@Override
        protected void drawPanel(MatrixStack stack, int entryRight, int relativeY, Tessellator tess, int mouseX, int mouseY)
        {

            for (IReorderingProcessor line : lines)
            {
                if (line != null)
                {
                    RenderSystem.enableBlend();
                    if ((selected.getDeck().getName().equalsIgnoreCase("Iron Deck") && iron) || (selected.getDeck().getName().equalsIgnoreCase("Gold Deck") && gold) || (selected.getDeck().getName().equalsIgnoreCase("Emerald Deck") && emerald) || (selected.getDeck().getName().equalsIgnoreCase("Diamond Deck") && diamond)) {
                    font.drawShadow(stack, line, left + PADDING, relativeY, 0xFFFFFF);
                    } else {
                    	font.drawWordWrap((EnchantmentNameParts.getInstance().getRandomName(font, font.width(line))),left+PADDING,relativeY,width - (left + PADDING),0xFFFFFF);
                    }
                    RenderSystem.disableAlphaTest();
                    RenderSystem.disableBlend();
                }
                relativeY += font.lineHeight;
            }

            final Style component = findTextLine(mouseX, mouseY);
            if (component!=null) {
            	GuiChallengeDatabaseSelection.this.renderComponentHoverEffect(stack,component, mouseX, mouseY);
            }
        }

        private Style findTextLine(final int mouseX, final int mouseY) {
            double offset = (mouseY - top) + border + scrollDistance + 1;
            if (offset <= 0)
                return null;

            int lineIdx = (int) (offset / font.lineHeight);
            if (lineIdx >= lines.size() || lineIdx < 1)
                return null;

            IReorderingProcessor line = lines.get(lineIdx-1);
            if (line != null)
            {
                return font.getSplitter().componentStyleAtWidth(line, mouseX);
            }
            return null;
        }

        @Override
        public boolean mouseClicked(final double mouseX, final double mouseY, final int button) {
            final Style component = findTextLine((int) mouseX, (int) mouseY);
            if (component != null) {
            	GuiChallengeDatabaseSelection.this.handleComponentClicked(component);
                return true;
            }
            return super.mouseClicked(mouseX, mouseY, button);
        }

        @Override
        protected void drawBackground() {
        }
    }

	
	@Override
	public void init() {
		super.init();	
        for (Deck mod : unsortedDecks)
        {
            listWidth = Math.max(listWidth,font.width(mod.getName()) + 10);
            listWidth = Math.max(listWidth,font.width(mod.getCreators()) + 5);
        }
        listWidth = Math.max(Math.min(listWidth, width/3), 100);
        listWidth += listWidth % numButtons != 0 ? (numButtons - listWidth % numButtons) : 0;

        int modInfoWidth = this.width - this.listWidth - (PADDING * 3);
        int doneButtonWidth = Math.min(modInfoWidth, 200);
        int y = this.height - 20 - PADDING;
        this.addButton(new Button(((listWidth + PADDING + this.width - doneButtonWidth) / 2), y, doneButtonWidth, 20,
                new TranslationTextComponent("gui.done"), b -> GuiChallengeDatabaseSelection.this.onClose()));
        this.addButton(this.removeDeckButton = new Button(6,y,this.listWidth,20,new TranslationTextComponent("gui.cah.removePack"),new RemoveDeckButton()));
        y -= 20 + PADDING;
        this.addButton(this.addDeckButton = new Button(6, y, this.listWidth, 20,
                new TranslationTextComponent("gui.cah.addPack"), new DeckButton()));
        y -= 14 + PADDING + 1;
        search = new TextFieldWidget(font, PADDING + 1, y, listWidth - 2, 14, new TranslationTextComponent("fml.menu.mods.search"));

        int fullButtonHeight = PADDING + 20 + PADDING;
        this.deckList = new ChallengeDeckList(this, listWidth, fullButtonHeight, search.y - font.lineHeight - PADDING);
        this.deckList.setLeftPos(6);

        this.modInfo = new InfoPanel(this.minecraft, modInfoWidth, this.height - PADDING - fullButtonHeight, PADDING);

        children.add(search);
        children.add(deckList);
        children.add(modInfo);
        search.setFocus(false);
        search.setCanLoseFocus(true);

        final int width = listWidth / numButtons;
        int x = PADDING;
        addButton(SortType.NORMAL.button = new Button(x, PADDING, width - buttonMargin, 20, SortType.NORMAL.getButtonText(), b -> resortMods(SortType.NORMAL)));
        x += width + buttonMargin;
        addButton(SortType.A_TO_Z.button = new Button(x, PADDING, width - buttonMargin, 20, SortType.A_TO_Z.getButtonText(), b -> resortMods(SortType.A_TO_Z)));
        x += width + buttonMargin;
        addButton(SortType.Z_TO_A.button = new Button(x, PADDING, width - buttonMargin, 20, SortType.Z_TO_A.getButtonText(), b -> resortMods(SortType.Z_TO_A)));
        resortMods(SortType.NORMAL);
        updateCache();
        
		CompoundNBT challengePacks = player.getPersistentData().getCompound("challengepacks");
		
		iron = gold = emerald = diamond = false;
		
		if (challengePacks != null) {
			if (challengePacks.get("iron") != null && challengePacks.getBoolean("iron"))
				iron = true;
			if (challengePacks.get("gold") != null && challengePacks.getBoolean("gold"))
				gold = true;
			if (challengePacks.get("emerald") != null && challengePacks.getBoolean("emerald"))
				emerald = true;
			if (challengePacks.get("diamond") != null && challengePacks.getBoolean("diamond"))
				diamond = true;
		}
		
		emerald = true;
	
	}
	
	@Override
	public void tick() {
		super.tick();
		search.tick();
		deckList.setSelected(selected);

        if (!search.getValue().equals(lastFilterText))
        {
            reloadMods();
            sorted = false;
        }

        if (!sorted)
        {
            reloadMods();
            decks.sort(sortType);
            deckList.refreshList();
            if (selected != null)
            {
                selected = deckList.children().stream().filter(e -> e == selected).findFirst().orElse(null);
                updateCache();
            }
            sorted = true;
        }
       
	}
	
	@SuppressWarnings("resource")
	@Override
    public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks)
    {
		this.renderBackground(stack, 0);
        this.deckList.render(stack, mouseX, mouseY, partialTicks);
        if (this.modInfo != null)
            this.modInfo.render(stack, mouseX, mouseY, partialTicks);

        TranslationTextComponent textComponent = new TranslationTextComponent("fml.menu.mods.search");
        int x = deckList.getLeft() + ((deckList.getRight() - deckList.getLeft()) / 2) - (getFontRenderer().width(textComponent.getContents()) / 2);
        getFontRenderer().draw(stack,textComponent, x, search.y - getFontRenderer().lineHeight, 0xFFFFFF);
        this.search.render(stack,mouseX, mouseY, partialTicks);
        super.render(stack,mouseX, mouseY, partialTicks);
    }
	
	private void reloadMods()
    {
        this.decks = this.unsortedDecks.stream().
                filter(mi->StringUtils.toLowerCase(stripControlCodes(mi.getCode())).contains(StringUtils.toLowerCase(search.getValue()))).collect(Collectors.toList());
        lastFilterText = search.getValue();
    }

    private void resortMods(SortType newSort)
    {
        this.sortType = newSort;

        for (SortType sort : SortType.values())
        {
            if (sort.button != null)
                sort.button.active = sortType != sort;
        }
        sorted = false;
    }
    
    public void setSelected(ChallengeDeckEntry challengeDeckEntry)
    {
        this.selected = challengeDeckEntry == this.selected ? null : challengeDeckEntry;
        updateCache();
    }

    private void updateCache()
    {
        if (selected == null) {
            this.addDeckButton.active = false;
            this.removeDeckButton.active = false;
            this.modInfo.clearInfo();
            return;
        }
        
        Deck deck = selected.getDeck();
        
        boolean eligible = false;
        
        switch (deck.getName().toLowerCase()) {
        	case "iron deck":
        		eligible = iron;
        		break;
        	case "gold deck":
        		eligible = gold;
        		break;
        	case "emerald deck":
        		eligible = emerald;
        		break;
        	case "diamond deck":
        		eligible = diamond;
        		break;
        	default:
        		eligible = false;
        		break;
        			
        }
        
        this.addDeckButton.active = selected != null && !parentScreen.getDecks().contains(deck) && eligible;
        this.removeDeckButton.active = selected != null && parentScreen.getDecks().contains(deck);
        
        List<String> lines = new ArrayList<>();

        lines.add(deck.getName());
        if (deck.getCreator()!=Deck.BLANK_CREATOR) {
        	lines.add(I18n.get("cah.mod.database.author",deck.getCreators()));
        	lines.add(null);
        }
        lines.add(I18n.get("cah.mod.database.code", deck.getCode()));
        lines.add(null);

        lines.add("Black Cards: " + deck.getBlackCards().size());
        lines.add("White Cards: " + deck.getWhiteCards().size());
        if (deck.getDescription() != "missingno") {
        	lines.add(null);
        	lines.add(selected.getDeck().getDescription());
        }

       modInfo.setInfo(lines);
    }

    @Override
    public void resize(Minecraft mc, int width, int height)
    {
        String s = this.search.getValue();
        SortType sort = this.sortType;
        ChallengeDeckEntry selected = this.selected;
        this.init(mc, width, height);
        this.search.setValue(s);
        this.selected = selected;
        if (!this.search.getValue().isEmpty())
            reloadMods();
        if (sort != SortType.NORMAL)
            resortMods(sort);
        updateCache();
    }
    
	@Override
	public boolean isPauseScreen() {
		return false;
	}

    
    public <T extends ExtendedList.AbstractListEntry<T>> void buildDeckList(Consumer<T> modListViewConsumer, Function<Deck, T> newEntry)
    {
        decks.forEach(mod->modListViewConsumer.accept(newEntry.apply(mod)));
    }
    
    public FontRenderer getFontRenderer() {
    	return font;
    }
	
	public class DeckButton implements IPressable {

		public DeckButton() {
		}
		
		@Override
		public void onPress(Button p_onPress_1_) {
			if (selected != null) {
				Deck deck = selected.getDeck();
				CAHMod.logger.info(deck.getWhiteCards().size());
				if (deck != null && parentScreen.loadDeck(deck) != null) {
					p_onPress_1_.active = false;
					removeDeckButton.active = true;
				} else {
					p_onPress_1_.active = true;
					removeDeckButton.active = false;
				}
			}
		}
		
	}
	
	public class RemoveDeckButton implements IPressable {

		public RemoveDeckButton() {
		}
		
		@Override
		public void onPress(Button p_onPress_1_) {
			if (selected != null) {
				Deck deck = selected.getDeck();
				//CAHMod.logger.info(deck.getWhiteCards().size());
				if (deck != null && parentScreen.removeDeck(deck) != null) {
					p_onPress_1_.active = false;
					addDeckButton.active = true;
				} else {
					p_onPress_1_.active = true;
					addDeckButton.active = false;
				}
			}
		}
		
	}

}

