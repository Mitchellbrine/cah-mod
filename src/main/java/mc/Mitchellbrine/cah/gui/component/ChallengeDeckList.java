package mc.Mitchellbrine.cah.gui.component;

import com.mojang.blaze3d.matrix.MatrixStack;

import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.gui.creation.GuiChallengeDatabaseSelection;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.util.EnchantmentNameParts;

public class ChallengeDeckList extends ExtendedList<ChallengeDeckList.ChallengeDeckEntry> {

    private static String stripControlCodes(String value) { return net.minecraft.util.StringUtils.stripColor(value); }
    private static String[] stripControlCodes(String[] value) {
    	String[] result = new String[value.length];
    	for (int i = 0; i < result.length;i++)
    		result[i] = net.minecraft.util.StringUtils.stripColor(value[i]);
    	return result;
    }

    private final int listWidth;

    private GuiChallengeDatabaseSelection parent;

    public ChallengeDeckList(GuiChallengeDatabaseSelection parent, int listWidth, int top, int bottom)
    {
        super(parent.getMinecraft(), listWidth, parent.height, top, bottom, parent.getFontRenderer().lineHeight * 2 + 8);
        this.parent = parent;
        this.listWidth = listWidth;
        this.refreshList();
    }
    
    @Override
    protected int getScrollbarPosition()
    {
        return this.listWidth;
    }

    @Override
    public int getRowWidth()
    {
        return this.listWidth;
    }

    public void refreshList() {
        this.clearEntries();
        parent.buildDeckList(this::addEntry, mod->new ChallengeDeckEntry(mod, this.parent));
    }
    
    @Override
    protected void renderBackground(MatrixStack stack)
    {
        this.parent.parentScreen.renderBackground(stack,0);
    }
    
    public class ChallengeDeckEntry extends ExtendedList.AbstractListEntry<ChallengeDeckEntry> {
        private final Deck deck;
        private final GuiChallengeDatabaseSelection parent;

        ChallengeDeckEntry(Deck deck, GuiChallengeDatabaseSelection parent) {
            this.deck = deck;
            this.parent = parent;
        }
        
        @Override
        public void render(MatrixStack stack,int entryIdx, int top, int left, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean p_194999_5_, float partialTicks)
        {
            String name = stripControlCodes(deck.getName());
            String[] creatorList = stripControlCodes(deck.getCreator());
            String creator = "";
            FontRenderer font = this.parent.getFontRenderer();
            if (name.equalsIgnoreCase("Iron Deck") && parent.iron) {
            	font.draw(stack,font.plainSubstrByWidth(name, listWidth),left + 3, top + 2, 0xE1E1E1);
            } else if (name.equalsIgnoreCase("Iron Deck") && !parent.iron) {
            	font.drawWordWrap((EnchantmentNameParts.getInstance().getRandomName(font, font.width(name))),left+3,top+2,width,0xE1E1E1);
            }
            
            if (name.equalsIgnoreCase("Gold Deck") && parent.gold) {
            	font.draw(stack,font.plainSubstrByWidth(name, listWidth),left + 3, top + 2, 0xFFFF96);
            } else if (name.equalsIgnoreCase("Gold Deck") && !parent.gold) {
            	font.drawWordWrap((EnchantmentNameParts.getInstance().getRandomName(font, font.width(name))),left+3,top+2,width,0xFFFF96);
            }
            
            if (name.equalsIgnoreCase("Emerald Deck") && parent.emerald) {
            	font.draw(stack,font.plainSubstrByWidth(name, listWidth),left + 3, top + 2, 0xAFFFAF);
            } else if (name.equalsIgnoreCase("Emerald Deck") && !parent.emerald) {
            	font.drawWordWrap((EnchantmentNameParts.getInstance().getRandomName(font, font.width(name))),left+3,top+2,width,0xAFFFAF);
            }
            
            if (name.equalsIgnoreCase("Diamond Deck") && parent.diamond) {
            	font.draw(stack,font.plainSubstrByWidth(name, listWidth),left + 3, top + 2, 0xC8FFFF);
            } else if (name.equalsIgnoreCase("Diamond Deck") && !parent.diamond) {
            	font.drawWordWrap((EnchantmentNameParts.getInstance().getRandomName(font, font.width(name))),left+3,top+2,width,0xC8FFFF);
            }
            
            if (creatorList.length > 1) {
            	for (int i = 0; i < creatorList.length - 1;i++) {
            		creator.concat(creatorList[i] + ", ");
            	}
            	creator.concat(creatorList[creatorList.length - 1]);
            } else if (creatorList.length == 1) {
            	creator = creatorList[0];
            }
            if (!creator.equalsIgnoreCase("missingno"))
            	font.draw(stack,font.plainSubstrByWidth(creator, listWidth), left + 3 , top + 2 + font.lineHeight, 0xCCCCCC);
        }

        @Override
        public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_)
        {
            parent.setSelected(this);
            ChallengeDeckList.this.setSelected(this);
            return false;
        }

        public Deck getDeck()
        {
            return deck;
        }
    }

	
}
