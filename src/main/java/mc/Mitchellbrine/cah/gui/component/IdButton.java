package mc.Mitchellbrine.cah.gui.component;

import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.ITextComponent;

public class IdButton extends Button {

	public int ID;
	public boolean sent;
	
	public IdButton(int id, int widthIn, int heightIn, int width, int height, ITextComponent text, Button.IPressable onPress) {
		super (widthIn, heightIn, width, height, text, onPress);
		ID = id;
		sent = false;		
	}
	
	public IdButton(int id, int widthIn, int heightIn, int width, int height, ITextComponent text, Button.IPressable onPress, Button.ITooltip onTool) {
		super (widthIn, heightIn, width, height, text, onPress,onTool);
		ID = id;
		sent = false;
	}
	
}
