package mc.Mitchellbrine.cah.gui.ingame;

import mc.Mitchellbrine.cah.client.CAHCachedData;
import mc.Mitchellbrine.cah.game.CAHGame;
import mc.Mitchellbrine.cah.gui.component.IdButton;
import mc.Mitchellbrine.cah.network.BlackCardRequestPacket;
import mc.Mitchellbrine.cah.network.CardChosenPacket;
import mc.Mitchellbrine.cah.network.CardRequestPacket;
import mc.Mitchellbrine.cah.network.GameRequestPacket;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.network.WhiteCardsRequestPacket;
import mc.Mitchellbrine.cah.util.References;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GuiPlayerCards extends Screen {

	private PlayerEntity player;

	private ResourceLocation blackCard = new ResourceLocation(References.MODID + ":textures/gui/blackCard.png".toLowerCase());
	private ResourceLocation whiteCard = new ResourceLocation(References.MODID + ":textures/gui/whiteCard.png".toLowerCase());
	//private ResourceLocation blueCard = new ResourceLocation(References.MODID + ":textures/gui/selectedCard.png".toLowerCase());

	private IdButton card1;
	private IdButton card2;
	private IdButton card3;
	private IdButton card4;
	private IdButton card5;
	private IdButton card6;
	private IdButton card7;
	private IdButton card8;
	
	private StringTextComponent plus = new StringTextComponent("+");
	
	public GuiPlayerCards(PlayerEntity player) {
		super(new StringTextComponent("Players Deck"));
		this.player = player;
	}

	@Override
	public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(stack);

		GL11.glColor4f(1F,1F,1F,1F);


		for (int yy = 0; yy < 2;yy++) {
			for (int i = 0; i < 4; i++) {
					int cardNumber = i;
					if (yy != 0) {
						cardNumber = 4 + i;
					}
					if (CAHCachedData.playersCards != null && cardNumber < CAHCachedData.playersCards.size() && CAHCachedData.playersCards.get(cardNumber) != null && CAHCachedData.playersCards.get(cardNumber).getCardMessage() != null) {
						drawModalRectWithCustomSizedTexture((double)64 * i, (double)30 + (120 * yy), 42, 64,whiteCard);
					}
			}
		}

		drawCenteredString(stack,font,"Your Hand",width / 2, 4,0xFFFFFF);
		drawCenteredString(stack,font,"Points: " + CAHCachedData.points,width / 2, 12,0xFFFFFF);

		for (int yy = 0; yy < 2;yy++) {
			int yHeight = 65 + (240 * yy);
			for (int i = 0; i < 4; i++) {
				GL11.glScalef(0.5F, 0.5F, 0.5F);
				int cardNumber = i;
				if (yy != 0) {
					cardNumber = 4 + i;
				}
				if (CAHCachedData.playersCards != null && cardNumber < CAHCachedData.playersCards.size() && CAHCachedData.playersCards.get(cardNumber) != null && CAHCachedData.playersCards.get(cardNumber).getCardMessage() != null) {
					font.drawWordWrap(new StringTextComponent(CAHCachedData.playersCards.get(cardNumber).getCardMessage()), (128 * i) + 29, yHeight, 63, 0x000000);
				}
				GL11.glScalef(2F, 2F, 2F);
			}
		}
		
		String blackCardMessage = CAHCachedData.blackCard;
		String activeWhiteCard = "missingno";
		
		for (int i = 0; i < buttons.size();i++) {
			Button button = (Button) buttons.get(i);
			if (button.active && mouseX >= button.x && mouseX <= button.x + button.getWidth() && mouseY >= button.y && mouseY <= button.y + button.getHeight()) {
				if (CAHCachedData.playersCards != null && i < CAHCachedData.playersCards.size() && CAHCachedData.playersCards.get(i) != null && CAHCachedData.playersCards.get(i).getCardMessage() != null) {
					activeWhiteCard = CAHCachedData.playersCards.get(i).getCardMessage();
				}
				break;
			}
		}
		
		if (activeWhiteCard.endsWith(".")) {
			activeWhiteCard.substring(0, activeWhiteCard.length() - 1);
		}
		
			if (blackCardMessage.contains("_")) {
				if (CAHCachedData.submittedCards != null && CAHCachedData.submittedCards.size() > 0) {
					int index = 0;
					while (index < CAHCachedData.submittedCards.size()) {
						blackCardMessage = blackCardMessage.replaceFirst("_",CAHCachedData.submittedCards.get(index));
						index++;
					}
				} else {
					if (!activeWhiteCard.equalsIgnoreCase("missingno")) {
						blackCardMessage = blackCardMessage.replaceFirst("_",activeWhiteCard);
					}
				}
			} else {
				if (CAHCachedData.submittedCards != null && CAHCachedData.submittedCards.size() > 0) {
					int index = 0;
					while (index < CAHCachedData.submittedCards.size()) {
						blackCardMessage = blackCardMessage + " " + CAHCachedData.submittedCards.get(index);
						index++;
					}
				} else {
					if (!activeWhiteCard.equalsIgnoreCase("missingno")) {
						blackCardMessage = blackCardMessage + " " + activeWhiteCard;
					}
				}
			}


		if (blackCardMessage.endsWith("..") || blackCardMessage.endsWith(".!")) {
			char append = blackCardMessage.charAt(blackCardMessage.length() - 1);
			blackCardMessage = blackCardMessage.substring(0,blackCardMessage.length() - 2);
			blackCardMessage.concat(""+append);
		}
		
		if (blackCardMessage.contains(".!")) {
			blackCardMessage.replaceAll(".!", "!");
		}
		
		if (blackCardMessage.contains(".?"))
			blackCardMessage.replaceAll(".?", "?");

		if (blackCardMessage.contains(".:"))
			blackCardMessage.replaceAll(".:", ":");

		
		if (CAHCachedData.blackCard != null && !CAHCachedData.blackCard.equalsIgnoreCase("missingno") && CAHCachedData.roundState != null && CAHCachedData.roundState == CAHGame.RoundState.PLAYING_CARDS) {
			GL11.glColor4f(1F, 1F, 1F, 1F);
			drawModalRectWithCustomSizedTexture(320D, 30D, 43D*2, 64D*2,blackCard);
			GL11.glScalef(0.5F,0.5F,0.5F);
			font.drawWordWrap(new StringTextComponent(blackCardMessage), (329 * 2) + 12, 36 * 2, 128, 0xFFFFFF);
			GL11.glScalef(2F,2F,2F);
		}

		super.render(stack,mouseX,mouseY,partialTicks);
	}

	public void init() {
		super.init();

		PacketHandler.INSTANCE.sendToServer(new CardRequestPacket(player.getScoreboardName()));
		PacketHandler.INSTANCE.sendToServer(new GameRequestPacket(player.getScoreboardName()));
		PacketHandler.INSTANCE.sendToServer(new BlackCardRequestPacket(player.getScoreboardName()));
		PacketHandler.INSTANCE.sendToServer(new WhiteCardsRequestPacket(player.getScoreboardName()));
		
		card1 = new IdButton(0,17,100,20,20,plus,new HandCard());
		card2 = new IdButton(1,82,100,20,20,plus,new HandCard());
		card3 = new IdButton(2,145,100,20,20,plus,new HandCard());
		card4 = new IdButton(3,212,100,20,20,plus,new HandCard());

		card5 = new IdButton(4,17,215,20,20,plus,new HandCard());
		card6 = new IdButton(5,82,215,20,20,plus,new HandCard());
		card7 = new IdButton(6,145,215,20,20,plus,new HandCard());
		card8 = new IdButton(7,212,215,20,20,plus,new HandCard());

		addButton(card1);
		addButton(card2);
		addButton(card3);
		addButton(card4);
		addButton(card5);
		addButton(card6);
		addButton(card7);
		addButton(card8);
	}
	
	protected class HandCard implements IPressable {

		@Override
		public void onPress(Button but) {
			IdButton button = (IdButton)but;
			PacketHandler.INSTANCE.sendToServer(new CardChosenPacket(player.getScoreboardName(), button.ID,CAHCachedData.playersCards.get(button.ID).getCardMessage()));
			button.active = false;
			button.sent = true;
			PacketHandler.INSTANCE.sendToServer(new CardRequestPacket(player.getScoreboardName()));
			PacketHandler.INSTANCE.sendToServer(new GameRequestPacket(player.getScoreboardName()));
			PacketHandler.INSTANCE.sendToServer(new BlackCardRequestPacket(player.getScoreboardName()));
			PacketHandler.INSTANCE.sendToServer(new WhiteCardsRequestPacket(player.getScoreboardName()));
			
			if (CAHCachedData.hasInputted) {
				getMinecraft().setScreen(null);
			}
		}
		
	}

	/**private void drawTexturedRect(MatrixStack stack,ResourceLocation texture, double x, double y, int u, int v, int width, int height, int imageWidth, int imageHeight, double scale) {
		//minecraft.gameRenderer.bindTexture(texture);
		
		double minU = u / imageWidth;
		double maxU = (u + width) / imageWidth;
		double minV = v / imageHeight;
		double maxV = (v + height) / imageHeight;

        RenderSystem.color4f(1F,1F,1F,1F);
        getMinecraft().getTextureManager().bind(texture);

        //int x = (width - this.xSize) / 2;
        //int y = (height - this.ySize) / 2;
		blit(stack,((int)x), ((int)y), u, v, (width * (int)scale), (height*(int)scale));
		
		
/**		Tessellator tesselator = Tessellator.getInstance();
		BufferBuilder buffer = tesselator.getBuffer();
		buffer.startDrawingQuads();
		blit(x + scale * width, y + scale * height, 0, maxU, maxV,);
		tesselator.addVertexWithUV(x + scale*width,y, 0,maxU,minV);
		tesselator.addVertexWithUV(x,y, 0,minU,minV);
		tesselator.addVertexWithUV(x,y + scale*height, 0,minU,maxV);
		tesselator.draw();
	}*/
	
	@SuppressWarnings("deprecation")
    public void drawModalRectWithCustomSizedTexture(double leftSideX, double topY, double width, double height, ResourceLocation texture) {
        double rightSideX = leftSideX + width;
        double bottomY = topY + height;
    	
        double generalOffset = 12D;
        
        RenderSystem.enableBlend();
        RenderSystem.disableDepthTest();
    	RenderSystem.depthMask(false);
        RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.disableAlphaTest();
        minecraft.getTextureManager().bind(texture);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuilder();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
//botlef  bufferbuilder.pos(0.0D, (double)mc.mainWindow.getScaledHeight(), -90.0D).tex(0.0D, 1.0D).endVertex();
        bufferbuilder.vertex(leftSideX + generalOffset, bottomY, -90.0D).uv(0.0F, 1.0F).endVertex();
        
//botr    bufferbuilder.pos((double)mc.mainWindow.getScaledWidth(), (double)mc.mainWindow.getScaledHeight(), -90.0D).tex(1.0D, 1.0D).endVertex();
        bufferbuilder.vertex(rightSideX + generalOffset, bottomY, -90.0D).uv(1.0F, 1.0F).endVertex();
        
//topr      bufferbuilder.pos((double)mc.mainWindow.getScaledWidth(), 0.0D, -90.0D).tex(1.0D, 0.0D).endVertex();
        bufferbuilder.vertex(rightSideX + generalOffset, topY, -90.0D).uv(1.0F, 0.0F).endVertex();
        
//topl    bufferbuilder.pos(0.0D, 0.0D, -90.0D).tex(0.0D, 0.0D).endVertex();
        bufferbuilder.vertex(leftSideX + generalOffset, topY, -90.0D).uv(0.0F, 0.0F).endVertex();

        tessellator.end();
        RenderSystem.depthMask(true);
        RenderSystem.enableDepthTest();
        RenderSystem.enableAlphaTest();
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
  }

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	@Override
	public void tick() {
		super.tick();
		if (CAHCachedData.playersCards == null || !CAHCachedData.serverExists) {
			PacketHandler.INSTANCE.sendToServer(new GameRequestPacket(player.getScoreboardName()));
			PacketHandler.INSTANCE.sendToServer(new BlackCardRequestPacket(player.getScoreboardName()));
			PacketHandler.INSTANCE.sendToServer(new CardRequestPacket(player.getScoreboardName()));
			getMinecraft().setScreen(null);
			return;
		}
		
		
		if (CAHCachedData.hasInputted || CAHCachedData.isCzar || CAHGame.RoundState.toInt(CAHCachedData.roundState) > 0) {
			card1.active = card2.active = card3.active = card4.active = card5.active = card6.active = card7.active = card8.active = false;
		} else {

			card1.active = card2.active = card3.active = card4.active = card5.active = card6.active = card7.active = card8.active = true;
			
			for (Widget widget : buttons) {
				if (widget instanceof IdButton) {
					IdButton button = (IdButton)widget;
						button.active = !button.sent && CAHCachedData.roundState == CAHGame.RoundState.PLAYING_CARDS;
				}
			}
			
			if (CAHCachedData.playersCards != null) {
				
				for (int i = 0; i < CAHCachedData.playersCards.size(); i++) {
					if (CAHCachedData.playersCards.get(i) == null || CAHCachedData.playersCards.get(i).getCardMessage() == null)
						buttons.get(i).active = buttons.get(i).visible = false;
				}
				

				for (int i = 0; i < buttons.size();i++) {
					if (i >= CAHCachedData.playersCards.size()) {
						buttons.get(i).active = buttons.get(i).visible = false;
						continue;
					}
					if (CAHCachedData.playersCards.get(i) != null && CAHCachedData.submittedCards != null && CAHCachedData.submittedCards.size() > 0) {
						for (String string : CAHCachedData.submittedCards) {
							if (CAHCachedData.playersCards.get(i).getCardMessage().equalsIgnoreCase(string)) {
								buttons.get(i).active = false;
								break;
							}
						}
					}
				}
			}
		}
		

		/**if (CAHCachedData.isCzar) {
			GuiHandler.openGui(GuiHandler.IDS.CHOICE, player);
		}*/

	}

}
