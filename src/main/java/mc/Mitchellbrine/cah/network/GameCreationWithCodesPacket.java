package mc.Mitchellbrine.cah.network;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.CardpackHandler;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.game.CAHGame;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class GameCreationWithCodesPacket {

		public List<Deck> deckCodes;
		public String playerName;
		public int maxPoints;
		public int botNumber;
		public int numDecks;

		public GameCreationWithCodesPacket(){}

		public GameCreationWithCodesPacket(String creator, int maxPoints, int botNum, List<Deck> packs) {
			deckCodes = packs;
			playerName = creator;
			this.maxPoints = maxPoints;
			botNumber = botNum;
			numDecks = deckCodes.size();
		}
		
		@SuppressWarnings("resource")
		public static void handle(final GameCreationWithCodesPacket packet, Supplier<NetworkEvent.Context> context) {
			context.get().enqueueWork(() ->
	        {
	            ServerPlayerEntity player = context.get().getSender();
	            if (player == null)
	            {
	                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
	            }
	            else
	            {
	            	if (CAHMod.game != null) {
	        			CAHMod.game.sendMessage("Console",packet.playerName,"A game is already running on this server!");
	        		} else {
	        		CAHMod.game = new CAHGame(ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByName(packet.playerName).level);
	        		CAHMod.game.setMaxPoints(packet.maxPoints);
	        		
	        		/**
	        		 * Add in the packs.
	        		 */
	        		
	        		CAHMod.game.setDecks(packet.deckCodes);
	        		
	        		CAHMod.game.addPlayer(packet.playerName);
	        		CAHMod.game.setHost(packet.playerName);
	        		for (int i = 0; i < packet.botNumber;i++) {
	        			CAHMod.game.addPlayer("~Bot" + i);
	        		}
	    				PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new GamePacket(CAHMod.game != null, CAHMod.game.getGameState(), CAHMod.game.roundState, CAHMod.game != null ? CAHMod.game.getCzarIndex() : -1,CAHMod.game != null && CAHMod.game.getCzar().equalsIgnoreCase(packet.playerName)));
	    				//CAHMod.logger.info("Sent Game Packet after creation");
	        		}
	        	}
	        });
	        context.get().setPacketHandled(true);
		}

		public static void encode(GameCreationWithCodesPacket packet, PacketBuffer buffer) {
			buffer.writeUtf(packet.playerName);
			buffer.writeInt(packet.maxPoints);
			buffer.writeInt(packet.botNumber);
			buffer.writeInt(packet.numDecks);
			
			/**for (int i = 0; i < packet.numDecks;i++) {
				buffer.writeInt(packet.deckCodes.get(i).getWhiteCards().size());
			}
			
			for (int i = 0; i < packet.numDecks;i++) {
				buffer.writeInt(packet.deckCodes.get(i).getBlackCards().size());
			}*/
			
			for (Deck deck : packet.deckCodes) {
				
				buffer.writeUtf(deck.getName());
				buffer.writeInt(deck.getId());
				buffer.writeUtf(deck.getDatabaseID());
			}
			
		}

		public static GameCreationWithCodesPacket decode(PacketBuffer buffer) {
			String maker = buffer.readUtf(32767);
			int maxPoint = buffer.readInt();
			int botNumber = buffer.readInt();
			int indexNum = buffer.readInt();
			
			List<Deck> allDecks = new ArrayList<Deck>();
			
			for (int i = 0; i < indexNum;i++) {
				String deckName = buffer.readUtf(32767);
				int deckID = buffer.readInt();
				String databaseID = buffer.readUtf(32767);
				Deck deck = CardpackHandler.INSTANCE.getDatabase(databaseID).getDeckFromName(deckName);
				if (CardpackHandler.INSTANCE.getDatabase(databaseID) == null)
					throw new NullPointerException("Database " + databaseID + " somehow doesn't exist?");
				
				if (deck != null)
					allDecks.add(deck);
				else
					throw new NullPointerException("Deck requested from " + databaseID + " with name " + deckName + " does not exist!");
			
			}
			
			return new GameCreationWithCodesPacket(maker, maxPoint, botNumber, allDecks);
		}

	/**	@Override
		public void fromBytes(ByteBuf buf) {
			playerName = ByteBufUtils.readUTF8String(buf);
			maxPoints = buf.readInt();
			List<String> packs = new ArrayList<String>();
			while (buf.readableBytes() > 0) {
				packs.add(ByteBufUtils.readUTF8String(buf));
			}
			deckCodes = packs;
		}

		@Override
		public void toBytes(ByteBuf buf) {
			ByteBufUtils.writeUTF8String(buf,playerName);
			buf.writeInt(maxPoints);
			for (String pack : deckCodes) {
				ByteBufUtils.writeUTF8String(buf,pack);
			}
		}

		@Override
		public IMessage onMessage(GameCreationPacket message, MessageContext ctx) {
			if (CAHMod.game != null) {
				CAHMod.game.sendMessage("Console",message.playerName,"A game is already running on this server!");
				return null;
			}
			CAHMod.game = new CAHGame(MinecraftServer.getServer().getEntityWorld());
			CAHMod.game.setMaxPoints(message.maxPoints);
			for (String pack : message.deckCodes) {
				CAHMod.game.addDeck(pack);
			}
			CAHMod.game.addPlayer(message.playerName);
			return null;
		}*/

	
}
