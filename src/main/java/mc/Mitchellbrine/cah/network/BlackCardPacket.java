package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.client.CAHCachedData;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Created by Mitchellbrine on 2015.
 */
public class BlackCardPacket{

	public String cardString;
	public boolean hasInputted;

	public BlackCardPacket(){}

	public BlackCardPacket(String card, boolean inputted) {
		cardString = card;
		hasInputted = inputted;
	}
	
	public static void handle(final BlackCardPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
    		CAHCachedData.blackCard = packet.cardString;
    		CAHCachedData.hasInputted = packet.hasInputted;        		
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(BlackCardPacket packet, PacketBuffer buffer) {
		buffer.writeUtf(packet.cardString);
		buffer.writeBoolean(packet.hasInputted);
	}

	public static BlackCardPacket decode(PacketBuffer buffer) {
		return new BlackCardPacket(buffer.readUtf(32767),buffer.readBoolean());
	}

/**	@Override
	public void fromBytes(ByteBuf buf) {
		cardString = ByteBufUtils.readUTF8String(buf);
		hasInputted = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,cardString);
		buf.writeBoolean(hasInputted);
	}

	@Override
	public IMessage onMessage(BlackCardPacket message, MessageContext ctx) {
		CAHCachedData.blackCard = packet.cardString;
		CAHCachedData.hasInputted = packet.hasInputted;
		return null;
	}*/
}
