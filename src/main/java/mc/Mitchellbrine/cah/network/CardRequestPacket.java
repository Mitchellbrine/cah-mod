package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;

/**
 * Created by Mitchellbrine on 2015.
 */
public class CardRequestPacket {

	public String player;

	public CardRequestPacket(){}

	public CardRequestPacket(String username) {
		player = username;
	}
	
	public static void handle(final CardRequestPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            ServerPlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		if (CAHMod.game == null || !CAHMod.game.playerExists(packet.player) || !CAHMod.game.hasCards(packet.player)) {
        			CAHMod.logger.debug("Someone sent a CardRequestPacket erroneously.");
        		} else {
        		//PacketHandler.INSTANCE.sendTo(new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.player));
        		PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new CardPacket(CAHMod.game.getPlayerPoints(packet.player), CAHMod.game.getPlayerCards(packet.player)));
        		}
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(CardRequestPacket packet, PacketBuffer buffer) {
		buffer.writeUtf(packet.player);
	}

	public static CardRequestPacket decode(PacketBuffer buffer) {
		return new CardRequestPacket(buffer.readUtf(32767));
	}

/**	@Override
	public void fromBytes(ByteBuf buf) {
		player = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,player);
	}

	@Override
	public IMessage onMessage(CardRequestPacket message, MessageContext ctx) {
		if (CAHMod.game == null || !CAHMod.game.playerExists(message.player) || !CAHMod.game.hasCards(message.player)) { return null; }
		//PacketHandler.INSTANCE.sendTo(new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.player));
		return new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player));
	}*/
}
