package mc.Mitchellbrine.cah.network;

import mc.Mitchellbrine.cah.client.CAHCachedData;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by Mitchellbrine on 2015.
 */
public class CardTextPacket {

	public List<String> texts;

	public CardTextPacket(){}

	public CardTextPacket(List<String> cardText) {
		this.texts = cardText;
	}

	public static void handle(final CardTextPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            /**PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
            	if (CAHMod.game != null) {
    			List<Card> cards = CAHMod.game.getPlayedCards(packet.index);
    			List<String> strings = new ArrayList<String>();
    			for (Card card : cards) {
    				strings.add(card.getCardMessage());
    			}
    			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(() -> player),new CardTextPacket(strings));
            	}
            	//PacketHandler.INSTANCE.sendTo(new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.player));
//        		PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(() -> player),new CardPacket(CAHMod.game.getPlayerCards(packet.player),CAHMod.game.getPlayerPoints(packet.player)));

        	}*/
    		CAHCachedData.lastWhiteCard = packet.texts;
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(CardTextPacket packet, PacketBuffer buffer) {
		if (packet.texts != null) {
			for (String card : packet.texts) {
				buffer.writeUtf(card);
			}
		}
	}

	public static CardTextPacket decode(PacketBuffer buffer) {
		List<String> cards = new ArrayList<String>();
		while (buffer.readableBytes() > 0) {
			cards.add(buffer.readUtf(32767));
		}		
		return new CardTextPacket(cards);
	}

	
/**	@Override
	public void fromBytes(ByteBuf buf) {
		texts = new ArrayList<String>();
		while (buf.readableBytes() > 0) {
			texts.add(ByteBufUtils.readUTF8String(buf));
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		for (String card : texts) {
			ByteBufUtils.writeUTF8String(buf,card);
		}
	}

	@Override
	public IMessage onMessage(CardTextPacket message, MessageContext ctx) {
		CAHCachedData.lastWhiteCard = message.texts;
		return null;
	} */
}
