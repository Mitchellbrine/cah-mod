package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;

/**
 * Created by Mitchellbrine on 2015.
 */
public class BlackCardRequestPacket {

	public String user;

	public BlackCardRequestPacket(){}

	public BlackCardRequestPacket(String string) {
		user = string;
	}

	public static void handle(final BlackCardRequestPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            ServerPlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		String card = CAHMod.game != null ? CAHMod.game.getBlackCard() : "missingno";
        		boolean hasInputted = CAHMod.game != null && CAHMod.game.playerHasInputted(packet.user);
        		PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new BlackCardPacket(card,hasInputted));
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(BlackCardRequestPacket packet, PacketBuffer buffer) {
		buffer.writeUtf(packet.user);
	}

	public static BlackCardRequestPacket decode(PacketBuffer buffer) {
		return new BlackCardRequestPacket(buffer.readUtf(32767));
	}	
	
/**	@Override
	public void fromBytes(ByteBuf buf) {
		user = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,user);
	}

	@Override
	public IMessage onMessage(BlackCardRequestPacket message, MessageContext ctx) {
		String card = CAHMod.game != null ? CAHMod.game.getBlackCard() : "missingno";
		boolean hasInputted = CAHMod.game != null && CAHMod.game.playerHasInputted(message.user);
		//PacketHandler.INSTANCE.sendTo(new BlackCardPacket(card,hasInputted), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.user));
		return new BlackCardPacket(card,hasInputted);
	}*/
}
