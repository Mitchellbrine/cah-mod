package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;

/**
 * Created by Mitchellbrine on 2015.
 */
public class WhiteCardsRequestPacket {

	public String user;

	public WhiteCardsRequestPacket(){}

	public WhiteCardsRequestPacket(String username) {
		user = username;
	}

	public static void handle(final WhiteCardsRequestPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            ServerPlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		if (CAHMod.game == null || !CAHMod.game.playerExists(packet.user) || !CAHMod.game.hasCards(packet.user)) {
        			CAHMod.logger.debug("Someone sent a CardRequestPacket erroneously.");
        		} else {
        		//PacketHandler.INSTANCE.sendTo(new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.player));
        		PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player),new WhiteCardsPacket(CAHMod.game != null ? CAHMod.game.getAmountInputted() : -1));
        		}
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(WhiteCardsRequestPacket packet, PacketBuffer buffer) {
		buffer.writeUtf(packet.user);
	}

	public static WhiteCardsRequestPacket decode(PacketBuffer buffer) {
		return new WhiteCardsRequestPacket(buffer.readUtf(32767));
	}

/**	@Override
	public void fromBytes(ByteBuf buf) {
		user = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,user);
	}

	@Override
	public IMessage onMessage(WhiteCardsRequestPacket message, MessageContext ctx) {
		return new WhiteCardsPacket(CAHMod.game != null ? CAHMod.game.getAmountInputted() : -1);
	}*/
}
