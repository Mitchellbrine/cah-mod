package mc.Mitchellbrine.cah.network;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import mc.Mitchellbrine.cah.client.CAHCachedData;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

public class LastSubmittedCardPacket {
	public List<String> cards;

	public LastSubmittedCardPacket(){}

	public LastSubmittedCardPacket(List<String> cardText) {
		this.cards = cardText;
	}

	public static void handle(final LastSubmittedCardPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            /**PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
            	if (CAHMod.game != null) {
    			List<Card> cards = CAHMod.game.getPlayedCards(packet.index);
    			List<String> strings = new ArrayList<String>();
    			for (Card card : cards) {
    				strings.add(card.getCardMessage());
    			}
    			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(() -> player),new CardTextPacket(strings));
            	}
            	//PacketHandler.INSTANCE.sendTo(new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.player));
//        		PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(() -> player),new CardPacket(CAHMod.game.getPlayerCards(packet.player),CAHMod.game.getPlayerPoints(packet.player)));

        	}*/
    		CAHCachedData.submittedCards = packet.cards;
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(LastSubmittedCardPacket packet, PacketBuffer buffer) {
		if (packet.cards != null) {
			for (String card : packet.cards) {
				buffer.writeUtf(card);
			}
		}
	}

	public static LastSubmittedCardPacket decode(PacketBuffer buffer) {
		List<String> cards = new ArrayList<String>();
		while (buffer.readableBytes() > 0) {
			cards.add(buffer.readUtf(32767));
		}		
		return new LastSubmittedCardPacket(cards);
	}
}
