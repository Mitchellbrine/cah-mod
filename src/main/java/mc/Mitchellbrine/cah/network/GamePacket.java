package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.client.CAHCachedData;
import mc.Mitchellbrine.cah.game.CAHGame;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GamePacket{

	public boolean doesGameExist;
	public CAHGame.RoundState state;
	public int gameState;
	public int czar;
	public boolean isCzar;

	public GamePacket(){}

	public GamePacket(boolean isThere, int gameState, CAHGame.RoundState roundState, int czarIndex, boolean czarBookean) {
		doesGameExist = isThere;
		state = roundState;
		czar = czarIndex;
		isCzar = czarBookean;
		this.gameState = gameState;
	}
	
	public static void handle(final GamePacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
    		CAHCachedData.serverExists = packet.doesGameExist;
    		CAHCachedData.roundState = packet.state;
    		CAHCachedData.gameState = packet.gameState;
    		CAHCachedData.cardCzar = packet.czar;
    		CAHCachedData.isCzar = packet.isCzar;
    		//CAHMod.logger.info("Does server exist? " + CAHCachedData.serverExists);
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(GamePacket packet, PacketBuffer buffer) {
		buffer.writeBoolean(packet.doesGameExist);
		buffer.writeInt(packet.gameState);
		buffer.writeInt(CAHGame.RoundState.toInt(packet.state));
		buffer.writeInt(packet.czar);
		buffer.writeBoolean(packet.isCzar);	
	}

	public static GamePacket decode(PacketBuffer buffer) {
		return new GamePacket(buffer.readBoolean(),buffer.readInt(), CAHGame.RoundState.getState(buffer.readInt()),buffer.readInt(),buffer.readBoolean());
	}


/**	@Override
	public void fromBytes(ByteBuf buf) {
		doesGameExist = buf.readBoolean();
		state = buf.readInt();
		czar = buf.readInt();
		isCzar = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(doesGameExist);
		buf.writeInt(state);
		buf.writeInt(czar);
		buf.writeBoolean(isCzar);
	}

	@Override
	public IMessage onMessage(GamePacket message, MessageContext ctx) {
		CAHCachedData.serverExists = message.doesGameExist;
		CAHCachedData.roundState = message.state;
		CAHCachedData.cardCzar = message.czar;
		CAHCachedData.isCzar = message.isCzar;
		return null;
	} */
}
