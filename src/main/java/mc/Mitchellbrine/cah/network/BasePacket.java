package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;

public abstract class BasePacket {

	public static void handle(BasePacket packet, Supplier<Context> context) {}
	
	public static void encode(BasePacket packet, PacketBuffer buffer) {}
	
	public static BasePacket decode(PacketBuffer buffer) {return null;}
	
}
