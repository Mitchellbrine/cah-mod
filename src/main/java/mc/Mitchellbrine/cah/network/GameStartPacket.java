package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

public class GameStartPacket {


	public String user;

	public GameStartPacket() {}
	
	public GameStartPacket(String userName) {
		user = userName;
	}

	public static void handle(final GameStartPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
            	if (CAHMod.game.getGameState() > 0) {
        			CAHMod.game.sendMessage("Console",packet.user,"A game is already running on this server!");
        		} else {
        			if (CAHMod.game.getHost().equalsIgnoreCase(packet.user)) {
        				CAHMod.game.startGame();
        			} else {
        				CAHMod.logger.error("Non-host (" + packet.user + ") trying to start game");
        			}
        		}
        	}
        });
        context.get().setPacketHandled(true);
	}

	public static void encode(GameStartPacket packet, PacketBuffer buffer) {
		buffer.writeUtf(packet.user);

	}

	public static GameStartPacket decode(PacketBuffer buffer) {
		return new GameStartPacket(buffer.readUtf(32767));
	}

	
}
