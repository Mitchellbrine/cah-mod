package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GameJoinPacket {

	public String user;

	public GameJoinPacket(){}

	public GameJoinPacket(String username) {
		user = username;
	}

	public static void handle(final GameJoinPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		if (CAHMod.game != null) {
        			CAHMod.game.addPlayer(packet.user);
        		}
        	}
        });
        context.get().setPacketHandled(true);
	}

	public static void encode(GameJoinPacket packet, PacketBuffer buffer) {
		buffer.writeUtf(packet.user);
		
	}

	public static GameJoinPacket decode(PacketBuffer buffer) {
		return new GameJoinPacket(buffer.readUtf(32767));
	}

	
/**	@Override
	public void fromBytes(ByteBuf buf) {
		user = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,user);
	}

	@Override
	public IMessage onMessage(GameJoinPacket message, MessageContext ctx) {
		if (CAHMod.game != null) {
			CAHMod.game.addPlayer(message.user);
		}
		return null;
	} */
}
