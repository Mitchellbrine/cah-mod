package mc.Mitchellbrine.cah.event;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.thread.EffectiveSide;

/**
 * Created by Mitchellbrine on 2015.
 */
public class UniversalEventManager {

	@SubscribeEvent
	public void playerLeave(PlayerLoggedOutEvent event) {
		if (EffectiveSide.get() != LogicalSide.CLIENT) {
			if (CAHMod.game != null && CAHMod.game.playerExists(event.getPlayer().getScoreboardName())) {
				CAHMod.game.removePlayer(event.getPlayer().getScoreboardName());
			}
		}
	}

}
