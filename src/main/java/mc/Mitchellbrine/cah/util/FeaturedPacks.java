package mc.Mitchellbrine.cah.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.cardcast.CardcastReader;

public class FeaturedPacks {

	private static final String rep0 = "https://bitbucket.org/Mitchellbrine/cah-mod/raw/master/";

	private static final int GET_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(3);

	private static CardcastReader reader;
	
	public static List<Deck> getFeaturedPacks() {
		if (reader == null)
			reader = new CardcastReader();
		try {
		JsonParser parser = new JsonParser();

		String featCards = getUrlContent(String.format(rep0 + "featured.json"));
		if (featCards == null) {
			return null;
		}
		final JsonObject cards = (JsonObject) parser.parse(featCards);
		final JsonArray list = cards.get("featuredPacks").getAsJsonArray();
		
		String[] deckIDs = new String[list.size()];
		List<Deck> finalDecks = new ArrayList<Deck>();
		
		for (int i = 0; i < list.size(); i++) {
			//CAHMod.logger.info(deckIDs[i]);
			deckIDs[i] = list.get(i).getAsString();
		}
		
		for (String ids : deckIDs) {
			Deck deck = reader.loadSet(ids);
			if (deck != null)
				finalDecks.add(deck);
		}
		
		return finalDecks;
		
		} catch (IOException ex) {
			CAHMod.logger.error(String.format("Caught an exception while processing featured packs: %s", ex));
			ex.printStackTrace();
			return null;
		}
	}
	
	private static String getUrlContent(final String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(false);
		conn.setRequestMethod("GET");
		conn.setInstanceFollowRedirects(true);
		conn.setReadTimeout(GET_TIMEOUT);
		conn.setConnectTimeout(GET_TIMEOUT);

		final int code = conn.getResponseCode();
		if (HttpURLConnection.HTTP_OK != code) {
			CAHMod.logger.error(String.format("Got HTTP response code %d from Favorite Packs for %s", code, urlStr));
			return null;
		}
		/**String contentType = conn.getContentType();
		if (!"application/json".equals(contentType)) {
			CAHMod.logger.error(String.format("Got content-type %s from Cardcast for %s", contentType, urlStr));
			return null;
		}*/

		InputStream is = conn.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(isr);
		StringBuilder builder = new StringBuilder(4096);
		String line;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append('\n');
		}
		reader.close();
		isr.close();
		is.close();

		return builder.toString();
	}

}
