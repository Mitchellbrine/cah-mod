package mc.Mitchellbrine.cah.util;

/**
 * Created by Mitchellbrine on 2015.
 */
public class TranslationHelper {

	public static String CHOOSEAWINNER = "cah.winnerChoose";
	public static String CLICKTOCHOOSE = "cah.clickToChoose";
	public static String CLICKTOVIEW = "cah.clickToView";
	public static String CANNOTCHOOSE = "cah.cannotChoose";
	public static String[] ISSUETRANSLATIONS = new String[]{"cah.cannotCreateGame","","cah.cannotCreateGameReason1","cah.cannotCreateGameReason2"};
	public static String ALREADYPLAYED = "cah.alreadyPlayed";
	public static String YOUARECZAR = "cah.youAreCzar";
	public static String SUBMITTED = "cah.submittedCards";
	public static String CARDSAREREADY = "cah.readyCards";
	public static String NOTENOUGHPLAYERS = "cah.notEnoughPlayes";
	//public static String

}
