package mc.Mitchellbrine.cah.cardcast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Card;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.api.PackDatabase;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mitchellbrine on 2015.
 */
public class CardcastReader {

	private static final String HOSTNAME = "raw.githubusercontent.com/jakelauer/allbadcards/master/cardcast-packs/";
	//private static final String BASE_URL = "https://" + HOSTNAME;

	//private static final String CARD_SET_INFO_URL_FORMAT_STRING = BASE_URL + "%s";
	//private static final String CARD_SET_CARDS_URL_FORMAT_STRING = CARD_SET_INFO_URL_FORMAT_STRING + ".json";

	private static final int GET_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(3);

	private static final long INVALID_SET_CACHE_LIFETIME = TimeUnit.MINUTES.toMillis(5);

	private static final long VALID_SET_CACHE_LIFETIME = TimeUnit.MINUTES.toMillis(15);

	//private static final Pattern validIdPattern = Pattern.compile("[A-Z0-9]{5}");

	private static final Map<String, SoftReference<CardcastCacheEntry>> cache = Collections.synchronizedMap(new HashMap<String, SoftReference<CardcastCacheEntry>>());

	private class CardcastCacheEntry {
		final long expires;
		final Deck deck;

		CardcastCacheEntry(final long cacheLifetime, final Deck deck) {
			this.expires = System.currentTimeMillis() + cacheLifetime;
			this.deck = deck;
		}
	}

	private CardcastCacheEntry checkCache(final String setURL) {
		final SoftReference<CardcastCacheEntry> soft = cache.get(setURL);
		if (null == soft) {
			return null;
		}
		return soft.get();
	}

	public Deck loadSet(final String setId) {
	/**if (!validIdPattern.matcher(setId).matches()) {
		return null;
	}*/
	final CardcastCacheEntry cached = checkCache(setId);
	if (null != cached && cached.expires > System.currentTimeMillis()) {
		CAHMod.logger.debug(String.format("Using cache: %s=%s", setId, cached.deck));
		return cached.deck;
	} else if (null != cached) {
		CAHMod.logger.debug(String.format("Cache stale: %s", setId));
	} else {
		//CAHMod.logger.debug(String.format("Cache miss: %s", setId));
	}

	try {

		JsonParser parser = new JsonParser();

		/**final String infoContent = getUrlContent(String
			.format(CARD_SET_INFO_URL_FORMAT_STRING, setId));
		if (infoContent == null) {
			cacheMissingSet(setId);
			return null;
		}
		JsonObject info = (JsonObject) parser.parse(infoContent);*/

		String cardContent = getUrlContent(String.format(setId));
		if (cardContent == null) {
			cacheMissingSet(setId);
			return null;
		}
		final JsonObject cards = (JsonObject) parser.parse(cardContent);

		final JsonObject packDetails = cards.get("pack").getAsJsonObject();
		
		final String name = packDetails.get("name").getAsString();
		final String ID = packDetails.get("id").getAsString();
		if (name == null || ID == null || name.isEmpty()) {
			cacheMissingSet(setId);
			return null;
		}
		final Deck deck = new Deck(name, setId);

		final JsonElement packDescription = packDetails.get("description");
		
		if (packDescription != null)
			deck.setDescription(packDescription.getAsString());

		final JsonElement packAuthors = packDetails.get("authors");
		
		if (packAuthors != null) {
			JsonArray authorsArray = packAuthors.getAsJsonArray();
			String[] authorStrings = new String[authorsArray.size()];
			
			for (int i = 0; i < authorsArray.size();i++) {
				authorStrings[i] = "" + authorsArray.get(i);
			}
			
			deck.setCreator(authorStrings);
		}
		
		final JsonArray blackCards = cards.get("black").getAsJsonArray();
		if (blackCards != null) {
			for (JsonElement black : blackCards) {
				String text = ((JsonObject) black).get("content").getAsString();
				Integer pick = ((JsonObject)black).get("pick").getAsInt();
				deck.getBlackCards().add(text);
				deck.getBlackCardPicks().put(text, pick);
			}
		}

		final JsonArray whiteCards = (JsonArray) cards.get("white");
		if (whiteCards != null) {
			//List<String> strs = new ArrayList<String>();
			for (JsonElement white : whiteCards) {
						String cardCastString = ((JsonPrimitive)white).getAsString();
						StringBuilder newString = new StringBuilder();

						newString.append(cardCastString.substring(0, 1).toUpperCase());
						newString.append(cardCastString.substring(1));

						if (Character.isLetterOrDigit(cardCastString.charAt(cardCastString.length() - 1))) {
							newString.append('.');
						}

						//strs.add(newString.toString());
					//}
					//String text = StringUtils.join(strs, "");
					Card card = new Card(newString.toString(),deck);
					deck.getWhiteCards().add(card);
				//}
			}
		}

		cacheSet(setId, deck);

		
		return deck;
	} catch (Exception e) {
		CAHMod.logger.debug(String.format("Unable to load deck %s from AllBadCards", setId), e);
		e.printStackTrace();
		cacheMissingSet(setId);
		return null;
	}
}
	
	public Deck loadAndSaveSet(final String setId, PackDatabase database) {
	/**if (!validIdPattern.matcher(setId).matches()) {
		return null;
	}*/
	final CardcastCacheEntry cached = checkCache(setId);
	if (null != cached && cached.expires > System.currentTimeMillis()) {
		CAHMod.logger.debug(String.format("Using cache: %s=%s", setId, cached.deck));
		return cached.deck;
	} else if (null != cached) {
		CAHMod.logger.debug(String.format("Cache stale: %s", setId));
	} else {
		//CAHMod.logger.debug(String.format("Cache miss: %s", setId));
	}

	try {

		JsonParser parser = new JsonParser();

		/**final String infoContent = getUrlContent(String
			.format(CARD_SET_INFO_URL_FORMAT_STRING, setId));
		if (infoContent == null) {
			cacheMissingSet(setId);
			return null;
		}
		JsonObject info = (JsonObject) parser.parse(infoContent);*/

		String cardContent = getUrlContent(String.format(setId));
		if (cardContent == null) {
			cacheMissingSet(setId);
			return null;
		}
		final JsonObject cards = (JsonObject) parser.parse(cardContent);

		final JsonObject packDetails = cards.get("pack").getAsJsonObject();
		
		final String name = packDetails.get("name").getAsString();
		final String ID = packDetails.get("id").getAsString();
		if (name == null || ID == null || name.isEmpty()) {
			cacheMissingSet(setId);
			return null;
		}
		final Deck deck = new Deck(name, ID);

		final JsonElement packDescription = packDetails.get("description");
		
		if (packDescription != null)
			deck.setDescription(packDescription.getAsString());

		final JsonElement packAuthors = packDetails.get("authors");
		
		if (packAuthors != null) {
			JsonArray authorsArray = packAuthors.getAsJsonArray();
			String[] authorStrings = new String[authorsArray.size()];
			
			for (int i = 0; i < authorsArray.size();i++) {
				authorStrings[i] = "" + authorsArray.get(i);
			}
			
			deck.setCreator(authorStrings);
		}
		
		final JsonArray blackCards = cards.get("black").getAsJsonArray();
		if (blackCards != null) {
			for (JsonElement black : blackCards) {
				String text = ((JsonObject) black).get("content").getAsString();
				Integer pick = ((JsonObject)black).get("pick").getAsInt();
				deck.getBlackCards().add(text);
				deck.getBlackCardPicks().put(text, pick);
			}
		}

		final JsonArray whiteCards = (JsonArray) cards.get("white");
		if (whiteCards != null) {
			//List<String> strs = new ArrayList<String>();
			for (JsonElement white : whiteCards) {
						String cardCastString = ((JsonPrimitive)white).getAsString();
						StringBuilder newString = new StringBuilder();

						newString.append(cardCastString.substring(0, 1).toUpperCase());
						newString.append(cardCastString.substring(1));

						if (Character.isLetterOrDigit(cardCastString.charAt(cardCastString.length() - 1))) {
							newString.append('.');
						}

						//strs.add(newString.toString());
					//}
					//String text = StringUtils.join(strs, "");
					Card card = new Card(newString.toString(),deck);
					deck.getWhiteCards().add(card);
				//}
			}
		}

		cacheSet(setId, deck);
		database.addDeck(deck);
		
		File packFile = new File(database.getPackFolder() + ID + ".json");
		
		if (!packFile.exists()) {
			packFile.createNewFile();
			
			PrintWriter writer = new PrintWriter(packFile);
			writer.println(cardContent);
			writer.close();
		}
		
		return deck;
	} catch (Exception e) {
		CAHMod.logger.debug(String.format("Unable to load deck %s from AllBadCards", setId), e);
		e.printStackTrace();
		cacheMissingSet(setId);
		return null;
	}
}

	private void cachePut(final String setId, final Deck deck, final long timeout) {
		CAHMod.logger.debug(String.format("Caching %s=%s for %d ms", setId, deck, timeout));
		cache.put(setId, new SoftReference<CardcastCacheEntry>(new CardcastCacheEntry(timeout, deck)));
	}

	private void cacheSet(final String URL, final Deck deck) {
		cachePut(URL, deck, VALID_SET_CACHE_LIFETIME);
	}

	private void cacheMissingSet(final String URL) {
		cachePut(URL, null, INVALID_SET_CACHE_LIFETIME);
	}

	private String getUrlContent(final String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(false);
		conn.setRequestMethod("GET");
		conn.setInstanceFollowRedirects(true);
		conn.setReadTimeout(GET_TIMEOUT);
		conn.setConnectTimeout(GET_TIMEOUT);

		final int code = conn.getResponseCode();
		if (HttpURLConnection.HTTP_OK != code) {
			CAHMod.logger.error(String.format("Got HTTP response code %d from AllBadCards for %s", code, urlStr));
			return null;
		}
		/**String contentType = conn.getContentType();
		if (!"application/json".equals(contentType)) {
			CAHMod.logger.error(String.format("Got content-type %s from Cardcast for %s", contentType, urlStr));
			return null;
		}*/

		InputStream is = conn.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(isr);
		StringBuilder builder = new StringBuilder(4096);
		String line;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append('\n');
		}
		reader.close();
		isr.close();
		is.close();

		return builder.toString();
	}

	public static void hackSslVerifier() {
		// The SSL verifier that PXY uses (I didn't know how to fix the issue without it)
		// FIXME: My JVM doesn't like the certificate. I should go add StartSSL's root certificate to
		// its trust store, and document steps. For now, I'm going to disable SSL certificate checking.

		// Create a trust manager that does not validate certificate chains
		final TrustManager[] trustAllCerts = new TrustManager[] {
			new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(final X509Certificate[] certs, final String authType) {
				}

				@Override
				public void checkServerTrusted(final X509Certificate[] certs, final String authType) {
				}
			}
		};

		try {
			final SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (final Exception e) {
			CAHMod.logger.error("Unable to install trust-all security manager", e);
		}

		// Create host name verifier that only trusts cardcast
		final HostnameVerifier allHostsValid = new HostnameVerifier() {
			@Override
			public boolean verify(final String hostname, final SSLSession session) {
				return HOSTNAME.equals(hostname);
			}
		};

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

}
