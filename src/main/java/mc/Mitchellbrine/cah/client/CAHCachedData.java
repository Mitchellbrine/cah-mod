package mc.Mitchellbrine.cah.client;


import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import java.util.List;

import mc.Mitchellbrine.cah.api.Card;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.game.CAHGame;

/**
 * Created by Mitchellbrine on 2015.
 */
@OnlyIn(Dist.CLIENT)
public class CAHCachedData {

	public static List<Card> playersCards = null;
	public static List<String> submittedCards = null;
	public static List<Deck> featuredDecks = null;

	public static boolean serverExists = false;

	public static String blackCard = "missingno";

	public static CAHGame.RoundState roundState = CAHGame.RoundState.NOT_FOUND;

	public static boolean hasInputted = false;

	public static int whiteCards = -1;

	public static int cardCzar = -1;

	public static boolean isCzar = false;

	public static int points = 0;

	public static List<String> lastWhiteCard;

	public static int gameState = -1;

}
