package mc.Mitchellbrine.cah;

import mc.Mitchellbrine.cah.api.CardpackHandler;
import mc.Mitchellbrine.cah.api.PackDatabase;
import mc.Mitchellbrine.cah.challenge.ItemResources;
import mc.Mitchellbrine.cah.event.EventManager;
import mc.Mitchellbrine.cah.event.UniversalEventManager;
import mc.Mitchellbrine.cah.game.CAHGame;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.proxy.ClientProxy;
import mc.Mitchellbrine.cah.util.References;
import net.minecraft.block.Block;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Mitchellbrine on 2015.
 */
@SuppressWarnings("deprecation")
@Mod(References.MODID)
public class CAHMod {

	public static CAHGame game;

	public static Logger logger = LogManager.getLogger(References.MODID);

	public CAHMod() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the enqueueIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        // Register the processIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::loaded);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
        
        logger.debug("Registered this class to pick up events.");
     
        
        DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable(){

			@Override
			public void run() {
				new CardpackHandler();
				logger.debug("Initiated the Cardpack Handler");
			}
        	
        });
        
        DistExecutor.runWhenOn(Dist.DEDICATED_SERVER, () -> new Runnable(){

			@Override
			public void run() {
				new CardpackHandler();
				logger.debug("Initiated the Cardpack Handler");
			}
        	
        });
        
        ItemResources.registerItems();

	}
	
    private void setup(final FMLCommonSetupEvent event) {
        // some preinit code
        //logger.info("HELLO FROM PREINIT");
        //logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());

		MinecraftForge.EVENT_BUS.register(new UniversalEventManager());
		//CardcastReader.hackSslVerifier();
		PacketHandler.init();
		
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable(){

			@Override
			public void run() {
				
				new ClientProxy();
				
				//logger.debug("Registering packs.");
				
				CardpackHandler.registerCardpack(new PackDatabase("official","Official CAH Packs",PackDatabase.officialBaseURL,true));
				CardpackHandler.registerCardpack(new PackDatabase("challenge","Challenge Packs",PackDatabase.challengeBaseURL,true));
				CardpackHandler.INSTANCE.scanClientPacks("local");
				CardpackHandler.INSTANCE.scanClientPacks("online");
				
				//logger.debug("Registered cardpacks");

				MinecraftForge.EVENT_BUS.register(new EventManager());
			
				//logger.debug("Registered event manager");
				
			}
			
		});
		
		DistExecutor.runWhenOn(Dist.DEDICATED_SERVER, () -> new Runnable() {
			
			@Override
			public void run() {
				CardpackHandler.registerCardpack(new PackDatabase("official","Official CAH Packs",PackDatabase.officialBaseURL,true));
				CardpackHandler.registerCardpack(new PackDatabase("challenge","Challenge Packs",PackDatabase.challengeBaseURL,true));
				CardpackHandler.INSTANCE.scanClientPacks("local");
				CardpackHandler.INSTANCE.scanClientPacks("online");
			}
		});
        
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        // do something that can only be done on the client
        //logger.info("Got game settings {}", event.getMinecraftSupplier().get().gameSettings);
    }

    private void enqueueIMC(final InterModEnqueueEvent event)
    {
        // some example code to dispatch IMC to another mod
        //InterModComms.sendTo("examplemod", "helloworld", () -> { logger.info("Hello world from the MDK"); return "Hello world";});
    }

    private void processIMC(final InterModProcessEvent event)
    {
        // some example code to receive and process InterModComms from other mods
        //logger.info("Got IMC {}", event.getIMCStream().
                //map(m->m.getMessageSupplier().get()).
                //collect(Collectors.toList()));
    }
    
    private void loaded(final FMLLoadCompleteEvent event) {
    	DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable() {
    		public void run() {
    			//logger.debug("Performing final generation");
    			CardpackHandler.finalGenerate();
    			//logger.debug("Finally generated.");
    		}
    	});
    	
    	DistExecutor.runWhenOn(Dist.DEDICATED_SERVER, () -> new Runnable() {
    		public void run() {
    			//logger.debug("Performing final generation");
    			CardpackHandler.finalGenerate();
    			//logger.debug("Finally generated.");
    		}
    	});
    }
    
    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        // do something when the server starts
        //logger.info("HELLO from server starting");
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
            // register a new block here
            //logger.info("HELLO from Register Block");
        }
    }

	/**
	@SidedProxy(clientSide = "mc.Mitchellbrine.cah.proxy.ClientProxy",serverSide = "mc.Mitchellbrine.cah.proxy.CommonProxy")
	public static CommonProxy proxy;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		proxy.registerStuff();
		if (event.getSide() == Side.CLIENT) {
			FMLCommonHandler.instance().bus().register(new EventManager());
		}
		FMLCommonHandler.instance().bus().register(new UniversalEventManager());
		CardcastReader.hackSslVerifier();
		PacketHandler.init();
	}
**/
}
