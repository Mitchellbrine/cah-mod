package mc.Mitchellbrine.cah.proxy;

import org.lwjgl.glfw.GLFW;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.client.registry.ClientRegistry;

/**
 * Created by Mitchellbrine on 2015.
 */
@OnlyIn(Dist.CLIENT)
public class ClientProxy extends CommonProxy {

	public ClientProxy() {
		registerStuff();
	}
	
	public static KeyBinding openFriends;
	public static KeyBinding openCards;
	public static KeyBinding joinGame;

	public void registerStuff() {
		openFriends = new KeyBinding("key.createGame", GLFW.GLFW_KEY_KP_DECIMAL,"key.categories.multiplayer");
		openCards = new KeyBinding("key.openCards", GLFW.GLFW_KEY_KP_ENTER,"key.categories.multiplayer");
		joinGame = new KeyBinding("key.joinGame", GLFW.GLFW_KEY_KP_ADD,"key.categories.multiplayer");

		ClientRegistry.registerKeyBinding(openFriends);
		ClientRegistry.registerKeyBinding(openCards);
		ClientRegistry.registerKeyBinding(joinGame);

		//CAHMod.logger.info("Made it through the registering");

	}


}
