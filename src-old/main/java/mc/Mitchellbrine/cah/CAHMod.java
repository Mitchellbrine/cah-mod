package mc.Mitchellbrine.cah;

import mc.Mitchellbrine.cah.api.CardpackHandler;
import mc.Mitchellbrine.cah.api.PackDatabase;
import mc.Mitchellbrine.cah.cardcast.CardcastReader;
import mc.Mitchellbrine.cah.event.EventManager;
import mc.Mitchellbrine.cah.event.UniversalEventManager;
import mc.Mitchellbrine.cah.game.CAHGame;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.proxy.ClientProxy;
import mc.Mitchellbrine.cah.proxy.CommonProxy;
import mc.Mitchellbrine.cah.util.References;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Mitchellbrine on 2015.
 */
@Mod(References.MODID)
public class CAHMod {

	public static CAHGame game;

	public static Logger logger = LogManager.getLogger(References.MODID);

	public CAHMod() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the enqueueIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        // Register the processIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::loaded);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
        
        DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable(){

			@Override
			public void run() {
				new CardpackHandler();
			}
        	
        });

	}
	
    private void setup(final FMLCommonSetupEvent event)
    {
        // some preinit code
        //logger.info("HELLO FROM PREINIT");
        //logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
        
        DistExecutor.runForDist(() -> () -> new ClientProxy(), () -> () -> new CommonProxy());
		MinecraftForge.EVENT_BUS.register(new UniversalEventManager());
		//CardcastReader.hackSslVerifier();
		PacketHandler.init();
		
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable(){

			@Override
			public void run() {
				CardpackHandler.registerCardpack(new PackDatabase("official","Official CAH Packs",PackDatabase.officialBaseURL));
			}
			
		});
        
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        // do something that can only be done on the client
        //logger.info("Got game settings {}", event.getMinecraftSupplier().get().gameSettings);
		MinecraftForge.EVENT_BUS.register(new EventManager());
    }

    private void enqueueIMC(final InterModEnqueueEvent event)
    {
        // some example code to dispatch IMC to another mod
        //InterModComms.sendTo("examplemod", "helloworld", () -> { logger.info("Hello world from the MDK"); return "Hello world";});
    }

    private void processIMC(final InterModProcessEvent event)
    {
        // some example code to receive and process InterModComms from other mods
        //logger.info("Got IMC {}", event.getIMCStream().
                //map(m->m.getMessageSupplier().get()).
                //collect(Collectors.toList()));
    }
    
    private void loaded(final FMLLoadCompleteEvent event) {
    	DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable() {
    		public void run() {
    			CardpackHandler.finalGenerate();
    		}
    	});
    }
    
    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        // do something when the server starts
        //logger.info("HELLO from server starting");
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
            // register a new block here
            //logger.info("HELLO from Register Block");
        }
    }

	/**
	@SidedProxy(clientSide = "mc.Mitchellbrine.cah.proxy.ClientProxy",serverSide = "mc.Mitchellbrine.cah.proxy.CommonProxy")
	public static CommonProxy proxy;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		proxy.registerStuff();
		if (event.getSide() == Side.CLIENT) {
			FMLCommonHandler.instance().bus().register(new EventManager());
		}
		FMLCommonHandler.instance().bus().register(new UniversalEventManager());
		CardcastReader.hackSslVerifier();
		PacketHandler.init();
	}
**/
}
