package mc.Mitchellbrine.cah.event;


import java.util.ArrayList;
import java.util.List;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.client.CAHCachedData;
import mc.Mitchellbrine.cah.gui.GuiHandler;
import mc.Mitchellbrine.cah.network.BlackCardRequestPacket;
import mc.Mitchellbrine.cah.network.CardRequestPacket;
import mc.Mitchellbrine.cah.network.GameJoinPacket;
import mc.Mitchellbrine.cah.network.GameRequestPacket;
import mc.Mitchellbrine.cah.network.GameStartPacket;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.network.WhiteCardsRequestPacket;
import mc.Mitchellbrine.cah.proxy.ClientProxy;
import mc.Mitchellbrine.cah.util.FeaturedPacks;
import mc.Mitchellbrine.cah.util.References;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.MainMenuScreen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.InputEvent.KeyInputEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

/**
 * Created by Mitchellbrine on 2015.
 */
@Mod.EventBusSubscriber(modid=References.MODID,bus=Bus.FORGE)
public class EventManager {
	
	List<String> cardCreateDecks;

	public EventManager() {
		CAHCachedData.featuredDecks = FeaturedPacks.getFeaturedPacks();
	}
	
	@SubscribeEvent
	public void onKeyInput(KeyInputEvent event) {
		if(ClientProxy.openFriends.isPressed()) {
			if (Minecraft.getInstance().world != null) {
				PacketHandler.INSTANCE.sendToServer(new GameRequestPacket(Minecraft.getInstance().player.getScoreboardName()));
				if (!CAHCachedData.serverExists) {
					GuiHandler.openGui(GuiHandler.IDS.CREATE, Minecraft.getInstance().player);
				} else {
					if (CAHCachedData.gameState == 0) {
						PacketHandler.INSTANCE.sendToServer(new GameStartPacket(Minecraft.getInstance().player.getScoreboardName()));
					} else {
						//CAHMod.logger.info("Not Game State = 0; Round State is actually " + CAHCachedData.roundState);
					}
				}
			}
		}
		if (ClientProxy.openCards.isPressed()) {
			if (Minecraft.getInstance().world != null) {
				PacketHandler.INSTANCE.sendToServer(new CardRequestPacket(Minecraft.getInstance().player.getScoreboardName()));
				PacketHandler.INSTANCE.sendToServer(new GameRequestPacket(Minecraft.getInstance().player.getScoreboardName()));
				PacketHandler.INSTANCE.sendToServer(new BlackCardRequestPacket(Minecraft.getInstance().player.getScoreboardName()));
				PacketHandler.INSTANCE.sendToServer(new WhiteCardsRequestPacket(Minecraft.getInstance().player.getScoreboardName()));
				if (CAHCachedData.serverExists) {
					if (CAHCachedData.roundState == 1) {
						GuiHandler.openGui(GuiHandler.IDS.CHOICE, Minecraft.getInstance().player);
					} else {
						//Minecraft.getInstance().player.world.playS(Minecraft.getInstance().player, References.MODID + ":cardOpen", 1.0F, 1.0F);
						GuiHandler.openGui(GuiHandler.IDS.CARDS, Minecraft.getInstance().player);
					}
				}
			}
		}
		if (ClientProxy.joinGame.isPressed()) {
			if (Minecraft.getInstance().world != null) {
				Minecraft.getInstance().player.sendMessage(new TranslationTextComponent("cah.game.attemptJoin"));
				PacketHandler.INSTANCE.sendToServer(new GameJoinPacket(Minecraft.getInstance().player.getScoreboardName()));
			}
		}
	}
	
	@SubscribeEvent
	public void guiInit(GuiScreenEvent.InitGuiEvent event) {
		if (event.getGui() != null && event.getGui() instanceof MainMenuScreen && Float.valueOf(References.VERSION) >= 2.1) {
			event.addWidget(new ImageButton(event.getGui().width / 2 + 104, (event.getGui().height / 4 + 48) + 24, 20, 20, 0, 0, 20, new ResourceLocation(References.MODID + ":textures/gui/cardbutton.png".toLowerCase()), 32, 64, new Button.IPressable() {

				@Override
				public void onPress(Button p_onPress_1_) {
					System.out.println("Hello World!");
				}
				
			}));
			cardCreateDecks = new ArrayList<String>();
			cardCreateDecks.add("Create Card Decks");
			cardCreateDecks.add("(Feature Not Yet Available)");
		}
	}
	
	@SubscribeEvent
	public void guiRender(GuiScreenEvent.DrawScreenEvent.Post event) {
		if (event.getGui() != null && event.getGui() instanceof MainMenuScreen && Float.valueOf(References.VERSION) >= 2.1) {
			if (event.getMouseX() >= (event.getGui().width / 2 + 104) && event.getMouseX() < (event.getGui().width / 2 + 124) && event.getMouseY() >= (event.getGui().height / 4 + 72) && event.getMouseY() < (event.getGui().height / 4 + 92)) {
				event.getGui().renderTooltip(cardCreateDecks, event.getMouseX(), event.getMouseY());
			}
		}
	}

}
