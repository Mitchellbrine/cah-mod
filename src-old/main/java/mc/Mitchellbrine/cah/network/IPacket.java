package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

public interface IPacket {

	public static void encode(Object packet, PacketBuffer buffer) {}
	
	public static Object decode(PacketBuffer buffer) { return null; }
	
	public static void handle(Object packet, Supplier<NetworkEvent.Context> context) {}
	
}
