package mc.Mitchellbrine.cah.network;

import io.netty.buffer.ByteBuf;
import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Card;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by Mitchellbrine on 2015.
 */
public class CardTextRequestPacket {

	public int index;

	public CardTextRequestPacket(){}

	public CardTextRequestPacket(int index) {
		this.index = index;
	}

	public static void handle(final CardTextRequestPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
            	if (CAHMod.game != null) {
    			List<Card> cards = CAHMod.game.getPlayedCards(packet.index);
    			List<String> strings = new ArrayList<String>();
    			for (Card card : cards) {
    				strings.add(card.getCardMessage());
    			}
    			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player),new CardTextPacket(strings));
            	}
            	//PacketHandler.INSTANCE.sendTo(new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.player));
//        		PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(() -> player),new CardPacket(CAHMod.game.getPlayerCards(packet.player),CAHMod.game.getPlayerPoints(packet.player)));

        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(CardTextRequestPacket packet, PacketBuffer buffer) {
		buffer.writeInt(packet.index);
	}

	public static CardTextRequestPacket decode(PacketBuffer buffer) {
		return new CardTextRequestPacket(buffer.readInt());
	}

	
/**	@Override
	public void fromBytes(ByteBuf buf) {
		this.index = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.index);
	}

	@Override
	public IMessage onMessage(CardTextRequestPacket message, MessageContext ctx) {
		if (CAHMod.game != null) {
			List<Card> cards = CAHMod.game.getPlayedCards(message.index);
			List<String> strings = new ArrayList<String>();
			for (Card card : cards) {
				strings.add(card.getCardMessage());
			}
			return new CardTextPacket(strings);
		} else {
			return null;
		}
	}*/
}
