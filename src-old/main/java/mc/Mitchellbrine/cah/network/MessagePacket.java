package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Created by Mitchellbrine on 2015.
 */
public class MessagePacket {

	public String user, message;

	public MessagePacket(){}

	public MessagePacket(String username, String words) {
		user = username;
		message = words;
	}

	public static void handle(final MessagePacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		if (CAHMod.game != null) {
        			CAHMod.game.sendMessage("Console",packet.user,packet.message);
        		}
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(MessagePacket packet, PacketBuffer buffer) {
		buffer.writeString(packet.user);
		buffer.writeString(packet.message);
	}

	public static MessagePacket decode(PacketBuffer buffer) {
		return new MessagePacket(buffer.readString(), buffer.readString());
	}

	/**
	@Override
	public void fromBytes(ByteBuf buf) {
		user = ByteBufUtils.readUTF8String(buf);
		message = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,user);
		ByteBufUtils.writeUTF8String(buf,message);
	}

	@Override
	public IMessage onMessage(MessagePacket message, MessageContext ctx) {
		if (CAHMod.game != null) {
			CAHMod.game.sendMessage("Console",message.user,message.message);
		}
		return null;
	}*/
	
	
}
