package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Created by Mitchellbrine on 2015.
 */
public class WinningCardPacket {

	public int card;

	public WinningCardPacket(){}

	public WinningCardPacket(int id) {
		this.card = id;
	}
	
	public static void handle(final WinningCardPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		if (CAHMod.game != null && packet.card != -1) {
        			CAHMod.game.endRound(CAHMod.game.getPlayedCards(packet.card));
        		} else if (CAHMod.game != null && packet.card == -1) {
        			CAHMod.logger.info("Out of bounds card selected. Something is wrong.");
        		}
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(WinningCardPacket packet, PacketBuffer buffer) {
		buffer.writeInt(packet.card);
	}

	public static WinningCardPacket decode(PacketBuffer buffer) {
		return new WinningCardPacket(buffer.readInt());
	}

/**	@Override
	public void fromBytes(ByteBuf buf) {
		card = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(card);
	}

	@Override
	public IMessage onMessage(WinningCardPacket message, MessageContext ctx) {
		if (CAHMod.game != null) {
			CAHMod.game.endRound(CAHMod.game.getPlayedCards(message.card));
		}
		return null;
	}*/
}
