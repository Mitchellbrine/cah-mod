package mc.Mitchellbrine.cah.network;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.game.CAHGame;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GameCreationPacket{

	public List<String> deckCodes;
	public String playerName;
	public int maxPoints;
	public int botNumber;

	public GameCreationPacket(){}

	public GameCreationPacket(String creator, int maxPoints, int botNum, List<String> packs) {
		deckCodes = packs;
		playerName = creator;
		this.maxPoints = maxPoints;
		botNumber = botNum;
	}
	
	public static void handle(final GameCreationPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
            	if (CAHMod.game != null) {
        			CAHMod.game.sendMessage("Console",packet.playerName,"A game is already running on this server!");
        		} else {
        		CAHMod.game = new CAHGame(ServerLifecycleHooks.getCurrentServer().getWorld(ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(packet.playerName).dimension));
        		CAHMod.game.setMaxPoints(packet.maxPoints);
        		for (String pack : packet.deckCodes) {
        			CAHMod.game.addDeck(pack);
        		}
        		CAHMod.game.addPlayer(packet.playerName);
        		CAHMod.game.setHost(packet.playerName);
        		for (int i = 0; i < packet.botNumber;i++) {
        			CAHMod.game.addPlayer("~Bot" + i);
        		}
    				PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player),new GamePacket(CAHMod.game != null, CAHMod.game.getGameState(), CAHMod.game.roundState, CAHMod.game != null ? CAHMod.game.getCzarIndex() : -1,CAHMod.game != null && CAHMod.game.getCzar().equalsIgnoreCase(packet.playerName)));
    				//CAHMod.logger.info("Sent Game Packet after creation");
        		}
        	}
        });
        context.get().setPacketHandled(true);
	}

	public static void encode(GameCreationPacket packet, PacketBuffer buffer) {
		buffer.writeString(packet.playerName);
		buffer.writeInt(packet.maxPoints);
		buffer.writeInt(packet.botNumber);
		for (String pack : packet.deckCodes) {
			buffer.writeString(pack);
		}
		
	}

	public static GameCreationPacket decode(PacketBuffer buffer) {
		String maker = buffer.readString();
		int maxPoint = buffer.readInt();
		int botNumber = buffer.readInt();
		List<String> packs = new ArrayList<String>();
		while (buffer.readableBytes() > 0) {
			packs.add(buffer.readString());
		}
		return new GameCreationPacket(maker, maxPoint, botNumber, packs);
	}

/**	@Override
	public void fromBytes(ByteBuf buf) {
		playerName = ByteBufUtils.readUTF8String(buf);
		maxPoints = buf.readInt();
		List<String> packs = new ArrayList<String>();
		while (buf.readableBytes() > 0) {
			packs.add(ByteBufUtils.readUTF8String(buf));
		}
		deckCodes = packs;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,playerName);
		buf.writeInt(maxPoints);
		for (String pack : deckCodes) {
			ByteBufUtils.writeUTF8String(buf,pack);
		}
	}

	@Override
	public IMessage onMessage(GameCreationPacket message, MessageContext ctx) {
		if (CAHMod.game != null) {
			CAHMod.game.sendMessage("Console",message.playerName,"A game is already running on this server!");
			return null;
		}
		CAHMod.game = new CAHGame(MinecraftServer.getServer().getEntityWorld());
		CAHMod.game.setMaxPoints(message.maxPoints);
		for (String pack : message.deckCodes) {
			CAHMod.game.addDeck(pack);
		}
		CAHMod.game.addPlayer(message.playerName);
		return null;
	}*/
}
