package mc.Mitchellbrine.cah.network;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Card;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.List;
import java.util.function.Supplier;

/**
 * Created by Mitchellbrine on 2015.
 */
public class CardMessagePacket {

	public String user;
	public int cardIndex;

	public CardMessagePacket(){}

	public CardMessagePacket(String username, int index) {
		this.user = username;
		this.cardIndex = index;
	}
	
	public static void handle(final CardMessagePacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
            	if (CAHMod.game != null) {
        			List<Card> cards = CAHMod.game.getPlayedCards(packet.cardIndex);
        			for (Card card : cards) {
        				CAHMod.game.sendMessage("Console", packet.user, (cards.indexOf(card) + 1) + ". " + card.getCardMessage());
        			}
        		}
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(CardMessagePacket packet, PacketBuffer buffer) {
		buffer.writeString(packet.user);
		buffer.writeInt(packet.cardIndex);
	}

	public static CardMessagePacket decode(PacketBuffer buffer) {
		return new CardMessagePacket(buffer.readString(), buffer.readInt());
	}


/**	@Override
	public void fromBytes(ByteBuf buf) {
		user = ByteBufUtils.readUTF8String(buf);
		cardIndex = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,user);
		buf.writeInt(cardIndex);
	}

	@Override
	public IMessage onMessage(CardMessagePacket message, MessageContext ctx) {
		if (CAHMod.game != null) {
			List<Card> cards = CAHMod.game.getPlayedCards(message.cardIndex);
			for (Card card : cards) {
				CAHMod.game.sendMessage("Console", message.user, (cards.indexOf(card) + 1) + ". " + card.getCardMessage());
			}
		}
		return null;
	}*/
}
