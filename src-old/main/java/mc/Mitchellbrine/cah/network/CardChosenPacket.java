package mc.Mitchellbrine.cah.network;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Created by Mitchellbrine on 2015.
 */
public class CardChosenPacket {

	public String playerName;
	public int cards;

	public CardChosenPacket(){}

	public CardChosenPacket(String playerName,int cardChoices) {
		this.playerName = playerName;
		this.cards = cardChoices;
	}

	public static void handle(final CardChosenPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		if (CAHMod.game != null) {
        			CAHMod.game.playCard(packet.playerName,packet.cards);
        			if (!packet.playerName.equalsIgnoreCase(CAHMod.game.getCzar())) {
        				CAHMod.game.sendMessage("Console", packet.playerName, "You have submitted a card");
        			}
        		}
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(CardChosenPacket packet, PacketBuffer buffer) {
		buffer.writeString(packet.playerName);
		buffer.writeInt(packet.cards);
	}

	public static CardChosenPacket decode(PacketBuffer buffer) {
		return new CardChosenPacket(buffer.readString(), buffer.readInt());
	}

/**	
	@Override
	public void fromBytes(ByteBuf buf) {
		playerName = ByteBufUtils.readUTF8String(buf);
		cards = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf,playerName);
		buf.writeInt(cards);
	}

	@Override
	public IMessage onMessage(CardChosenPacket message, MessageContext ctx) {
		if (CAHMod.game == null) { return null; }
		CAHMod.game.playCard(message.playerName,message.cards);
		if (!message.playerName.equalsIgnoreCase(CAHMod.game.getCzar())) {
			CAHMod.game.sendMessage("Console", message.playerName, "You have submitted a card");
		}
			return null;
	}*/
}
