package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GameRequestPacket {

	public String user;

	public GameRequestPacket(){}

	public GameRequestPacket(String playerName) {
		user = playerName;
	}

	public static void handle(final GameRequestPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
        		int gameState = CAHMod.game != null ? CAHMod.game.getGameState() : -1;
        		if (gameState != -1){
        			//PacketHandler.INSTANCE.sendTo(new GamePacket(CAHMod.game != null, roundState, CAHMod.game != null ? CAHMod.game.getCzarIndex() : -1,CAHMod.game != null && CAHMod.game.getCzar().equalsIgnoreCase(message.user)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.user));
        		
        			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player),new GamePacket(CAHMod.game != null, gameState,CAHMod.game.roundState, CAHMod.game != null ? CAHMod.game.getCzarIndex() : -1,CAHMod.game != null && CAHMod.game.getCzar().equalsIgnoreCase(packet.user)));
        		}
        	}
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(GameRequestPacket packet, PacketBuffer buffer) {
		buffer.writeString(packet.user);
	}

	public static GameRequestPacket decode(PacketBuffer buffer) {
		return new GameRequestPacket(buffer.readString());
	}

	
/**	@Override
	public void fromBytes(ByteBuf buf) {
		user = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, user);
	}

	@Override
	public IMessage onMessage(GameRequestPacket message, MessageContext ctx) {
		int roundState = CAHMod.game != null ? CAHMod.game.roundState : -1;
		if (roundState == -1)
			return null;
		//PacketHandler.INSTANCE.sendTo(new GamePacket(CAHMod.game != null, roundState, CAHMod.game != null ? CAHMod.game.getCzarIndex() : -1,CAHMod.game != null && CAHMod.game.getCzar().equalsIgnoreCase(message.user)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.user));
		return new GamePacket(CAHMod.game != null, roundState, CAHMod.game != null ? CAHMod.game.getCzarIndex() : -1,CAHMod.game != null && CAHMod.game.getCzar().equalsIgnoreCase(message.user));
	}*/
}
