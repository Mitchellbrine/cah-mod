package mc.Mitchellbrine.cah.network;

import mc.Mitchellbrine.cah.api.Card;
import mc.Mitchellbrine.cah.client.CAHCachedData;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by Mitchellbrine on 2015.
 */
public class CardPacket {

	public List<Card> cards;
	public int playerPoints;

	public CardPacket(){}

	public CardPacket(int points, List<Card> cards) {
		this.cards = cards;
		playerPoints = points;
	}
	
	public static void handle(final CardPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
            /**PlayerEntity player = context.get().getSender();
            if (player == null)
            {
                CAHMod.logger.error("Client sending packet does not exist, aborting function for packet labelled ID " + PacketHandler.idLibrary.get(packet.getClass()));
            }
            else
            {
            	if (CAHMod.game != null) {
    			List<Card> cards = CAHMod.game.getPlayedCards(packet.index);
    			List<String> strings = new ArrayList<String>();
    			for (Card card : cards) {
    				strings.add(card.getCardMessage());
    			}
    			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(() -> player),new CardTextPacket(strings));
            	}
            	//PacketHandler.INSTANCE.sendTo(new CardPacket(CAHMod.game.getPlayerCards(message.player),CAHMod.game.getPlayerPoints(message.player)), MinecraftServer.getServer().getConfigurationManager().func_152612_a(message.player));
//        		PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(() -> player),new CardPacket(CAHMod.game.getPlayerCards(packet.player),CAHMod.game.getPlayerPoints(packet.player)));

        	}*/
    		CAHCachedData.playersCards = packet.cards;
    		CAHCachedData.points = packet.playerPoints;
        });

        context.get().setPacketHandled(true);

	}

	public static void encode(CardPacket packet, PacketBuffer buffer) {
		buffer.writeInt(packet.playerPoints);
		if (packet.cards != null) {
			for (Card card : packet.cards) {
				buffer.writeString(card.getCardMessage());
			}
		}
	}

	public static CardPacket decode(PacketBuffer buffer) {
		int playerPoints = buffer.readInt();
		List<Card> cards = new ArrayList<Card>();
		while (buffer.readableBytes() > 0) {
			cards.add(new Card(buffer.readString()));
		}		
		return new CardPacket(playerPoints,cards);
	}


/**	@Override
	public void fromBytes(ByteBuf buf) {
		playerPoints = buf.readInt();
		List<Card> cards = new ArrayList<Card>();
		while (buf.readableBytes() > 0) {
			cards.add(new Card(ByteBufUtils.readUTF8String(buf)));
		}
		this.cards = cards;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(playerPoints);
		if (cards != null) {
			for (Card card : cards) {
				ByteBufUtils.writeUTF8String(buf, card.getCardMessage());
			}
		}
	}

	@Override
	public IMessage onMessage(CardPacket message, MessageContext ctx) {
		CAHCachedData.playersCards = message.cards;
		CAHCachedData.points = message.playerPoints;
		return null;
	} */
}
