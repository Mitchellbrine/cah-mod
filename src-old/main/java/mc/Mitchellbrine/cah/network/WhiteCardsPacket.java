package mc.Mitchellbrine.cah.network;

import java.util.function.Supplier;

import mc.Mitchellbrine.cah.client.CAHCachedData;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Created by Mitchellbrine on 2015.
 */
public class WhiteCardsPacket{

	public int cards;

	public WhiteCardsPacket(){}

	public WhiteCardsPacket(int size) {
		cards = size;
	}

	public static WhiteCardsPacket decode(PacketBuffer buf) {
		return new WhiteCardsPacket(buf.readInt());
	}

	public static void encode(WhiteCardsPacket packet, PacketBuffer buf) {
		buf.writeInt(packet.cards);
	}
	
	public static void handle(final WhiteCardsPacket packet, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() ->
        {
    		CAHCachedData.whiteCards = packet.cards;
        });

        context.get().setPacketHandled(true);

	}

}
