package mc.Mitchellbrine.cah.network;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import mc.Mitchellbrine.cah.CAHMod;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

/**
 * Created by Mitchellbrine on 2015.
 */
public class PacketHandler {

	private static final String PROTOCOL_VERSION = "1";
	public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(new ResourceLocation("cahmod", "main"),
		    () -> PROTOCOL_VERSION,
		    PROTOCOL_VERSION::equals,
		    PROTOCOL_VERSION::equals);
	
	@SuppressWarnings("rawtypes")
	protected static Map<Class, Integer> idLibrary;

	public static void init() {
		
		idLibrary = new HashMap<Class, Integer>();
		
		registerMessage(0, CardChosenPacket.class, CardChosenPacket::encode,CardChosenPacket::decode,CardChosenPacket::handle);
		registerMessage(1, GameCreationPacket.class,GameCreationPacket::encode,GameCreationPacket::decode,GameCreationPacket::handle);
		registerMessage(6, GameJoinPacket.class,GameJoinPacket::encode,GameJoinPacket::decode,GameJoinPacket::handle);
		registerMessage(11, MessagePacket.class, MessagePacket::encode,MessagePacket::decode,MessagePacket::handle);
		registerMessage(12, CardMessagePacket.class,CardMessagePacket::encode,CardMessagePacket::decode,CardMessagePacket::handle);
		registerMessage(13, WinningCardPacket.class,WinningCardPacket::encode, WinningCardPacket::decode, WinningCardPacket::handle);

		// Request Packets

		registerMessage(2, CardRequestPacket.class,CardRequestPacket::encode, CardRequestPacket::decode, CardRequestPacket::handle);
		registerMessage(3, CardPacket.class,CardPacket::encode,CardPacket::decode,CardPacket::handle); // CLIENT
		registerMessage(4, GameRequestPacket.class,GameRequestPacket::encode,GameRequestPacket::decode,GameRequestPacket::handle);
		registerMessage(5, GamePacket.class,GamePacket::encode,GamePacket::decode,GamePacket::handle); // CLIENT

		registerMessage(7, BlackCardRequestPacket.class,BlackCardRequestPacket::encode,BlackCardRequestPacket::decode,BlackCardRequestPacket::handle);
		registerMessage(8, BlackCardPacket.class,BlackCardPacket::encode,BlackCardPacket::decode,BlackCardPacket::handle); // CLIENT

		registerMessage(9, WhiteCardsRequestPacket.class,WhiteCardsRequestPacket::encode,WhiteCardsRequestPacket::decode,WhiteCardsRequestPacket::handle);
		registerMessage(10, WhiteCardsPacket.class,WhiteCardsPacket::encode,WhiteCardsPacket::decode,WhiteCardsPacket::handle); // CLIENT

		registerMessage(14, CardTextRequestPacket.class,CardTextRequestPacket::encode,CardTextRequestPacket::decode,CardTextRequestPacket::handle);
		registerMessage(15, CardTextPacket.class,CardTextPacket::encode, CardTextPacket::decode, CardTextPacket::handle); // CLIENT

		registerMessage(16, GameStartPacket.class,GameStartPacket::encode, GameStartPacket::decode, GameStartPacket::handle); // CLIENT
		registerMessage(17, GameCreationWithDecksPacket.class,GameCreationWithDecksPacket::encode, GameCreationWithDecksPacket::decode, GameCreationWithDecksPacket::handle);

	}

	public static <MSG> void registerMessage(int index, Class<MSG> messageType, BiConsumer<MSG, PacketBuffer> encoder, Function<PacketBuffer, MSG> decoder, BiConsumer<MSG, Supplier<NetworkEvent.Context>> messageConsumer) {
		if (INSTANCE.registerMessage(index, messageType, encoder, decoder, messageConsumer) != null) {
			CAHMod.logger.info("Registered packet under the name " + messageType);
			idLibrary.put(messageType, index);
		}
	}
	
}
