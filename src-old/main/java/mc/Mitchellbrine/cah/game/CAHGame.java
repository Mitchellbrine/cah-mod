package mc.Mitchellbrine.cah.game;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Card;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.cardcast.CardcastReader;
import mc.Mitchellbrine.cah.network.BlackCardPacket;
import mc.Mitchellbrine.cah.network.CardPacket;
import mc.Mitchellbrine.cah.network.GamePacket;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.network.WhiteCardsPacket;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Everything here MUST be handled on the server. If it isn't, it can cause issues with client-server desync. #PACKETSFTW
 * Created by Mitchellbrine on 2015.
 */
public class CAHGame {

	private List<Deck> decks;
	private String currentCard = "";
	
	private String host;

	private List<List<Card>> playedCards;
	private Map<String,int[]> indexesToRemove;

	private List<Card> possibleWhiteCards;
	private List<String> possibleBlackCards;
	private int inputNeeded;
	private Map<String, Integer> inputPlayed;

	private List<String> players;
	private Map<String, Integer> playerPoints;
	private Map<String, List<Card>> playerCards;
	private int cardCzar = -1;

	private World world;

	private int gameState = -1;
	public int roundState = -1;

	private int MAX_POINTS = 8;
	private static final int MAX_PLAYERS = 8;
	
	private static Random random;

	private static final CardcastReader reader = new CardcastReader();

	public CAHGame(World world) {
		decks = new ArrayList<Deck>();
		players = new ArrayList<String>();
		this.world = world;
		gameState = 0;
		random = new Random(world.getGameTime());
	}

	public void setMaxPoints(int max) {
		this.MAX_POINTS = max;
	}

	public void startGame() {
		buildDeck();
		broadcastMessage("Game beginning in 5 seconds!");
		scheduler.schedule(new Runnable() {
			@Override
			public void run() {
				startNewRound();
				gameState = 1;
			}
		}, 5, TimeUnit.SECONDS);
	}

	public void buildDeck() {
		possibleWhiteCards = new ArrayList<Card>();
		possibleBlackCards = new ArrayList<String>();
		for (Deck deck : decks) {
			for (Card card : deck.getWhiteCards()) {
				possibleWhiteCards.add(card);
			}
			for (String card : deck.getBlackCards()) {
				possibleBlackCards.add(card);
			}
		}
	}

	public void startNewRound() {
		//CAHMod.logger.info("Started new round method");
		//Map<String,List<String>> cardsToRemove = new HashMap<String, List<String>>();
		if (playedCards == null) {
			System.out.println("New cards!");
			playedCards = new ArrayList<List<Card>>();
			playerPoints = new HashMap<String, Integer>();
			inputPlayed = new HashMap<String, Integer>();
			for (String player : players) {
				playerPoints.put(player,0);
			}
			playerCards = new HashMap<String, List<Card>>();
			indexesToRemove = new HashMap<String,int[]>();
		} else {

			/*for (List<Card> cardList : playedCards) {
				List<String> strings = new ArrayList<String>();
				String player = "";
				for (Card card : cardList) {
					if (players.contains(card.getCardHolder())) {
						strings.add(card.getCardMessage());
						player = card.getCardHolder();
					}
				}
				cardsToRemove.put(player,strings);
			}
			playedCards.clear();
			inputPlayed.clear();
		}

		for (String player : cardsToRemove.keySet()) {
			List<Card> cardsToRemoveFromInv = new ArrayList<Card>();
			for (String card : cardsToRemove.get(player)) {
				if (playerCards.containsKey(player)) {
					for (Card card1 : playerCards.get(player)) {
						if (card1.getCardMessage().equalsIgnoreCase(card)) {
							cardsToRemoveFromInv.add(card1);
						}
					}
				}
			}
			if (playerCards.containsKey(player)) {
				List<Card> cards = new ArrayList<Card>(playerCards.get(player));
				for (Card card : cardsToRemoveFromInv) {
					cards.remove(card);
				}
				playerCards.remove(player);
				playerCards.put(player,cards);
			} */

			playedCards.clear();
			inputPlayed.clear();
		}
			
			for (String player : playerCards.keySet()) {
				if (!getCzar().equalsIgnoreCase(player) && indexesToRemove.get(player) != null) {
					List<Card> cardsToRemove = new ArrayList<Card>();
					
					for (int i : indexesToRemove.get(player)) {
						System.out.println(player + " " + i);
						if (i >= 0) {
							possibleWhiteCards.add(playerCards.get(player).get(i));
							//playerCards.get(player);
							cardsToRemove.add(playerCards.get(player).get(i));
						}
					}
					
					for (Card card : cardsToRemove) {
						playerCards.get(player).remove(card);
					}
					
				}
			}
			
			CAHMod.logger.info(indexesToRemove.size());
			
			indexesToRemove.clear();

		CAHMod.logger.info("We have reached the point where cards should start to be rewarded.");

		Random pRandom = new Random(world.getGameTime());
		
		for (String player : players) {
			ArrayList<Card> playersCards = new ArrayList<Card>();
			CAHMod.logger.info("Starting awarding cards to " + player);
			if (playerCards.containsKey(player)) {
				playersCards = (ArrayList<Card>)playerCards.get(player);
				for (int i = 0; i < (8 - playersCards.size()); i++) {
					int cardIndex = pRandom.nextInt(possibleWhiteCards.size());
					playersCards.add(possibleWhiteCards.get(cardIndex));
					possibleWhiteCards.remove(cardIndex);
					CAHMod.logger.info("For player (" + player + "), we have added a new card.");
				}
			} else {
				CAHMod.logger.info("Player doesn't exist, starting cards distribution.");
				for (int i = 0; i < 8; i++) {
					CAHMod.logger.info("Starting finding card #" + i);
					CAHMod.logger.info("Possible White Cards to Select From:" + possibleWhiteCards.size());
					int cardIndex = pRandom.nextInt(possibleWhiteCards.size());
					CAHMod.logger.info(player + " got the index " + cardIndex);
					playersCards.add(possibleWhiteCards.get(cardIndex));
					possibleWhiteCards.remove(cardIndex);
					CAHMod.logger.info("Player " + player + " got a card with index " + cardIndex);
				}
			}
			playerCards.put(player, playersCards);
		}
		
		roundState = 0;
		
		CAHMod.logger.info("Round State is now reset to beginning.");

		Random blackCardRandom = new Random(world.getGameTime());

		int index = blackCardRandom.nextInt(possibleBlackCards.size());

		this.currentCard = possibleBlackCards.get(index);
		
		CAHMod.logger.info("Black Card has been chosen.");

		inputNeeded = StringUtils.countMatches(this.currentCard,"______");

		//broadcastMessage("Made it past the input needed, got " + inputNeeded);
		
		if (cardCzar < 0) {
			cardCzar = 0;
		} else {
			if (cardCzar < (players.size() - 1)) {
				cardCzar++;
			} else {
				cardCzar = 0;
			}
		}

		sendMessage("Console",players.get(cardCzar),"You are the Card Czar!");

		for (int i = 0; i < players.size();i++) {
			if (cardCzar == i) continue;

			sendMessage("Console",players.get(i),"The Card Czar is now " + players.get(cardCzar));
		}

		if (inputNeeded > 1) {
			broadcastMessage("This round requires you to play " + inputNeeded + " cards.");
		}

		for (String playerName : players) {
			if (!playerName.startsWith("~Bot")) {
				ServerPlayerEntity player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(playerName);			
				PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player), new CardPacket(playerPoints.get(playerName),playerCards.get(playerName)));
				PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player),new GamePacket(true,gameState,roundState,cardCzar,cardCzar == players.indexOf(playerName)));
				PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player),new BlackCardPacket(currentCard,false));
				PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player),new WhiteCardsPacket(playedCards.size()));
				System.out.println("Sent the packets for " + player.getScoreboardName());
			} else {
				// The bots choose their cards.
				for (int i = 0; i < inputNeeded;i++) {
					System.out.println(playerName + " has started to play card #" + (i + 1));
					playCard(playerName, random.nextInt(playerCards.get(playerName).size()));
					System.out.println("Finished playing card #" + (i + 1));
				}
			}
		}
		CAHMod.logger.info("Got through the round setup.");
	}

	public void endRound(List<Card> cards) {
		String winner = null;
		for (String player : players) {
			if (player.equalsIgnoreCase(cards.get(0).getCardHolder())) {
				winner = player;
			}
		}
		if (winner != null) {
			roundState = 2;
			if (playerPoints.containsKey(winner)) {
				broadcastMessage("The winning cards were: ");
				for (Card card : cards) {
					broadcastMessage("- " + card.getCardMessage());
				}
				if (playerPoints.get(winner) + 1 >= MAX_POINTS) {
					broadcastMessage(TextFormatting.GOLD + winner + " has won the game!");
					int newPoints = playerPoints.get(winner) + 1;
					playerPoints.remove(winner);
					playerPoints.put(winner,newPoints);
					endGame();
				} else {
					broadcastMessage(winner + " won the round!");
					int newPoints = playerPoints.get(winner) + 1;
					playerPoints.remove(winner);
					playerPoints.put(winner,newPoints);
					CAHMod.logger.info("Reached the Schedule Method");
					scheduleNextRound();
				}
			} else {
				broadcastMessage(winner + " won the round!");
				playerPoints.put(winner,1);
				scheduleNextRound();
			}
		}
		for (List<Card> cardList : playedCards) {
			for (Card whiteCard : cardList) {
				whiteCard.removeCardBearer();
				this.possibleWhiteCards.add(whiteCard);
			}
		}
	}

	public void addPlayer(String username) {
		if (players.contains(username))
			sendMessage("Console",username,"You already joined the game, silly!");
		else {
			if (players.size() == MAX_PLAYERS) {
				sendMessage("Console", username, "The max amount of players has been reached!");
			} else {
				players.add(username);
				if (playerCards != null && !playerCards.containsKey(username)) {
					ArrayList<Card> playersCards = new ArrayList<Card>();
					for (int i = 0; i < 8; i++) {
						int cardIndex = new Random(world.getGameTime()).nextInt(possibleWhiteCards.size());
						playersCards.add(possibleWhiteCards.get(cardIndex));
						possibleWhiteCards.remove(cardIndex);
					}
					playerCards.put(username, playersCards);
				}
				broadcastMessage(username + " has joined the game");
				/**if (players.size() >= 3 && gameState == 0) {
					startGame();
				}*/
			}
		}
	}

	public void removePlayer(String username) {
		players.remove(username);

		if (playerCards != null && playerCards.containsKey(username)) {
			for (Card card : playerCards.get(username)) {
				possibleWhiteCards.add(card);
			}
		}
		if (playerPoints != null && playerPoints.containsKey(username)) {
			playerPoints.remove(username);
		}
		if (inputPlayed != null && inputPlayed.containsKey(username)) {
			ArrayList<List<Card>> cardsToRemove = new ArrayList<List<Card>>();
			for (List<Card> cardList : playedCards) {
				for (Card card : cardList) {
					if (card.getCardHolder().equalsIgnoreCase(username)) {
						cardsToRemove.add(cardList);
					}
				}
			}
			for (List<Card> card : cardsToRemove) {
				for (Card cards : card) {
					cards.removeCardBearer();
					possibleWhiteCards.add(cards);
				}
				playedCards.remove(card);
			}
			inputPlayed.remove(username);
		}
		broadcastMessage(username + " has left the game");
		invalidatePlayer(username);

		if (cardCzar == players.indexOf(username)) {

			if (cardCzar < 0) {
				cardCzar = 0;
			} else {
				if (cardCzar < players.size() - 1) {
					cardCzar++;
				} else {
					cardCzar = 0;
				}
			}

			sendMessage("Console",players.get(cardCzar),username + " has logged out, making you the Card Czar!");
			if (inputPlayed != null && inputPlayed.containsKey(players.get(cardCzar))) {
				ArrayList<List<Card>> cardsToRemove = new ArrayList<List<Card>>();
				for (List<Card> cardList : playedCards) {
					for (Card card : cardList) {
						if (card.getCardHolder().equalsIgnoreCase(players.get(cardCzar))) {
							cardsToRemove.add(cardList);
						}
					}
				}
				List<Card> playersCards = playerCards.get(players.get(cardCzar));
				for (List<Card> card : cardsToRemove) {
					for (Card cards : card) {
						cards.removeCardBearer();
						playersCards.add(cards);
					}
					playedCards.remove(card);
				}
				playerCards.remove(players.get(cardCzar));
				playerCards.put(players.get(cardCzar),playersCards);
				inputPlayed.remove(players.get(cardCzar));
			}
		}
		if (players.size() < 3) {
			broadcastMessage("There are not enough players to continue. Ending the game!");
			endGame();
		}
	}

	public void broadcastMessage(String message) {
		for (String player : players) {
			if (!player.startsWith("~Bot"))
				ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(player).sendMessage(new TranslationTextComponent("cah.broadcast", message));
		}
	}

	public void sendMessage(String sender, String player, String message) {
		if (!player.startsWith("~Bot"))
			ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(player).sendMessage(new TranslationTextComponent("cah.message",sender, message));
	}

	public void sendMessage(String sender, String player, String message, boolean gold) {
		if (gold) {
			if (!player.startsWith("~Bot"))
				ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(player).sendMessage(new TranslationTextComponent("cah.message", sender, message).setStyle(new Style().setColor(TextFormatting.GOLD)));
		} else {
			if (!player.startsWith("~Bot"))
				ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(player).sendMessage(new TranslationTextComponent("cah.message", sender, message));
		}
	}

	final ScheduledExecutorService scheduler =
		Executors.newScheduledThreadPool(1);

	private void endGame() {
		scheduler.schedule(new Runnable() {
			@Override
			public void run() {
				invalidateAllPlayers();
				CAHMod.game = null;
			}
		}, 5, TimeUnit.SECONDS);
	}

	public void playCard(String player, int index) {
		playCard(player,playerCards.get(player).get(index));
	}

	public void playCard(String player, Card card) {
		if (card == null) {
			CAHMod.logger.fatal("Could not retrieve card");
			return;
		}
		if (inputPlayed.containsKey(player) && inputPlayed.get(player) == inputNeeded) {
			sendMessage("Console",player,"You have already played your cards this round!");
			return;
		}
		if (cardCzar == (players.indexOf(player))) {
			sendMessage("Console",player,"You are the Card Czar!");
			return;
		}
		List<Card> cardList = new ArrayList<Card>();
		boolean isNew = true;

		for (List<Card> cards : playedCards) {
			boolean markForBreak = false;
			for (Card card1 : cards) {
				if (card1.getCardHolder().equalsIgnoreCase(player)) {
					cardList = cards;
					markForBreak = true;
					isNew = false;
					break;
				}
			}
			if (markForBreak) {
				break;
			}
		}

		card.setCardBearer(player);
		cardList.add(card);

		if (!isNew) {
			playedCards.remove(cardList);
		}

		playedCards.add(cardList);

		if (inputPlayed.containsKey(player)) {
			int input = inputPlayed.get(player) + 1;
			inputPlayed.remove(player);
			inputPlayed.put(player,input);
		} else {
			inputPlayed.put(player,1);
		}

		int amountOfReady = 0;

		for (Integer inputs : inputPlayed.values()) {
			if (inputs == inputNeeded) {
				amountOfReady++;
			}
		}

		int[] intArray = new int[inputNeeded];

		for (int i = 0; i < inputNeeded;i++) {
			intArray[i] = -1;
		}
		
		System.out.println(intArray.length);

		if (indexesToRemove.containsKey(player)) {
			for (int i = 0; i < indexesToRemove.get(player).length;i++) {
				intArray[i] = indexesToRemove.get(player)[i];
			}
			for (int i = 0; i < intArray.length;i++) {
				if (intArray[i] == -1) {
					intArray[i] = playerCards.get(player).indexOf(card);
					break;
				}
			}
		} else {
			if (playerCards.get(player) != null && intArray.length > 0)
				intArray[0] = playerCards.get(player).indexOf(card);
			else
				CAHMod.logger.fatal("For some reason, the cards for the player don't... exist?");
		}
		indexesToRemove.put(player,intArray);

		if (amountOfReady == players.size() - 1) {
			finalCardInputted();
		}
	}

	public void finalCardInputted() {
		/**
		 * This is where I need to take all the cards from played cards out and get a random one, add it to played cards, and remove it from the index
		 */

		List<List<Card>> newList = new ArrayList<List<Card>>(playedCards);
		playedCards.removeAll(newList);

		Random random = new Random(world.getGameTime());

		do {
			int index = random.nextInt(newList.size());
			playedCards.add(newList.get(index));
			newList.remove(index);
		} while (newList.size() > 0);

		sendMessage("Console",players.get(cardCzar),"The cards are ready for you! Press 'Show Cards' to choose!",true);
		roundState = 1;
		if (!players.get(cardCzar).startsWith("~Bot")) {
			PlayerEntity player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(players.get(cardCzar));
			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player), new GamePacket(true,gameState,roundState,cardCzar,true));
			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> player), new WhiteCardsPacket(playedCards.size()));
		} else {
			endRound(playedCards.get(random.nextInt(playedCards.size())));
		}
	}

	public void addDeck(String id) {
		Deck deck = reader.loadSet(id);
		if (deck != null) {
			decks.add(deck);
		}
	}

	private void scheduleNextRound() {
		//CAHMod.logger.info("Reached schedule method");
		scheduler.schedule(new Runnable() {
			@Override
			public void run() {
				//CAHMod.logger.info("Executed the runnable");
				startNewRound();
			}
		}, 5, TimeUnit.SECONDS);
		//CAHMod.logger.info("Scheduled the runnable");
	}

	public List<Card> getPlayerCards(String playerName) {
		return playerCards.get(playerName);
	}

	public List<Card> getPlayedCards(int index) {
		return playedCards.get(index);
	}

	public boolean hasCards(String playerName) {
		return playerCards != null && playerCards.containsKey(playerName);
	}

	public boolean playerExists(String username) {
		return players.contains(username);
	}

	public String getBlackCard() {
		return this.currentCard;
	}

	// a.k.a. Force Quit
	protected void invalidateAllPlayers() {
		for (String player : players) {
			invalidatePlayer(player);
		}
	}

	private void invalidatePlayer(String player) {
		if (!player.startsWith("~Bot")) {
			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(player)), new GamePacket(false,-1,-1,-1,false));
			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(player)), new CardPacket(0,null));
			PacketHandler.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(player)), new BlackCardPacket("missingno",false));
		}
	}

	public boolean playerHasInputted(String username) {
		return inputPlayed != null && inputPlayed.containsKey(username) && inputPlayed.get(username) >= inputNeeded;
	}

	public int getAmountInputted() {
		return playedCards != null ? playedCards.size() : -1;
	}

	public String getCzar() {
		return cardCzar != -1 ? players.get(cardCzar) : "";
	}

	public int getCzarIndex() { return cardCzar; }

	public int getPlayerPoints(String user) {
		return playerPoints.get(user);
	}

	public void setHost(String playerName) {
		host= playerName;
	}

	public String getHost() {
		return host;
	}
	
	public int getGameState() {
		return gameState;
	}
	
	public boolean setDecks(List<Deck> newDecks) {
		this.decks = newDecks;
		return decks.size() == newDecks.size();
	}
	
	@Override
	public String toString() {
		return "Cards vs. Crafters | " + players.size() + " players, " + possibleBlackCards.size() + " black cards, " + possibleWhiteCards.size() + " black cards | Game State: " + gameState;
	}

}
