package mc.Mitchellbrine.cah.api;

/**
 * Created by Mitchellbrine on 2015.
 */
public class Card {

	private String cardMessage;
	private String cardBearer;

	public Card(String cardMessage) {
		this.cardMessage = cardMessage;
	}

	public void setCardBearer(String username) {
		this.cardBearer = username;
	}

	public String getCardHolder() {
		return this.cardBearer;
	}

	public String getCardMessage() {
		return this.cardMessage;
	}

	public void removeCardBearer() {
		this.cardBearer = "";
	}

}
