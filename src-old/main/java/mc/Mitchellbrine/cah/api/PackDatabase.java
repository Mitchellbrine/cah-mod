package mc.Mitchellbrine.cah.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PackDatabase {

	public String id, name;
	
	private String baseURL, packsJsonPath, decksPath;
	
	protected Map<String, Deck> cardPacks;
	
	public static final String officialBaseURL = "https://raw.githubusercontent.com/greencoast-studios/cards-against-humanity-api/master/data/official/";
	protected static final String defaultPackJson = "packs.json";
	protected static final String defaultPackFolder = "packs/";
		
	public PackDatabase(String id, String name, String baseURL, String packsJsonPath, String decksPath) {
		this.id = id;
		this.name = name;
		this.baseURL = baseURL;
		this.packsJsonPath = packsJsonPath;
		this.decksPath = decksPath;
		
		this.cardPacks = new HashMap<String,Deck>();
		
	}
	
	public PackDatabase(String id, String name, String baseURL, String decksPath) {
		this(id,name,baseURL,defaultPackJson,decksPath);
	}
	
	public PackDatabase(String id, String name, String baseURL) {
		this(id,name,baseURL,defaultPackJson,defaultPackFolder);
	}
	
	public PackDatabase(String id, String baseURL) {
		this(id,id,baseURL,defaultPackJson,defaultPackFolder);
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPackJSONFile() {
		return this.baseURL + this.packsJsonPath;
	}
	
	public String getPackFolder() {
		return this.baseURL + this.decksPath;
	}
	
	@SuppressWarnings({"rawtypes","unchecked"})
	public List<Deck> getDecks() {
		return new ArrayList(this.cardPacks.values());
	}
	
}
