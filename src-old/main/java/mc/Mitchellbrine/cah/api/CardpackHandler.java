package mc.Mitchellbrine.cah.api;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import mc.Mitchellbrine.cah.CAHMod;

import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class CardpackHandler {

	private static final int GET_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(3);
	private static final JsonParser parser = new JsonParser();

	private static boolean generated;
	
	private static final Logger logger = LogManager.getLogger("CvC-Cardpack Handler");
	
	public List<PackDatabase> packDatabases;
	
	public static CardpackHandler INSTANCE;
	
	public CardpackHandler() {
		INSTANCE = this;
		packDatabases = new ArrayList<PackDatabase>();
		generated = false;
	}

	public static void registerCardpack(PackDatabase pack) {
		 if (!INSTANCE.packDatabases.add(pack))
			 logger.error("Pack Database could not be registered");
	}
	
	/**
	 * This should ONLY be launched after CommonSetupEvent and only on FMLClientSetupEvent and ONLY be CvC
	 */
	public static void finalGenerate() {
		for (PackDatabase base : INSTANCE.packDatabases) {
			if (!generatePacks(base))
				logger.error(String.format("Deck couldn't be generated from database %s",base.id));
		}
	}
	
	private static boolean generatePacks(PackDatabase database) {
		
		String decksJSON = null;
		
		try {
			decksJSON = INSTANCE.getUrlContent(database.getPackJSONFile());
		} catch (IOException ex) {
		}
			if (decksJSON == null) {
				logger.error("Database didn't exist");
				return false;
			}
		
		try {
			JsonObject base = parser.parse(decksJSON).getAsJsonObject();

			JsonArray packs = base.get("packs").getAsJsonArray();
			
			for (JsonElement element : packs) {
				if (element != null && element.getAsJsonObject() != null) {
					JsonObject pack = element.getAsJsonObject();
					
					String id = pack.get("id").getAsString();
					
					try {
						String deckFile = INSTANCE.getUrlContent(database.getPackFolder() + id + ".json");
						
						if (deckFile == null) {
							logger.error(String.format("Pack (%s) didn't exist, skipping...", id));
						}
						
						JsonObject deckJSON = parser.parse(deckFile).getAsJsonObject();
						
						final JsonObject packDetails = deckJSON.get("pack").getAsJsonObject();
						
						final String name = packDetails.get("name").getAsString();
						final String ID = packDetails.get("id").getAsString();
						if (name == null || ID == null || name.isEmpty() || !id.equalsIgnoreCase(ID)) {
							logger.error(String.format("Name or ID of pack under previous ID %s does not exist...",id));
							continue;
						}
						final Deck deck = new Deck(name, ID).setDatabase(database);

						final JsonArray blackCards = deckJSON.get("black").getAsJsonArray();
						if (blackCards != null) {
							for (JsonElement black : blackCards) {
								String text = ((JsonObject) black).get("content").getAsString();
								deck.getBlackCards().add(text);
							}
						}

						final JsonArray whiteCards = (JsonArray) deckJSON.get("white");
						if (whiteCards != null) {
							//List<String> strs = new ArrayList<String>();
							for (JsonElement white : whiteCards) {
										String cardCastString = ((JsonPrimitive)white).getAsString();
										StringBuilder newString = new StringBuilder();

										newString.append(cardCastString.substring(0, 1).toUpperCase());
										newString.append(cardCastString.substring(1));

										if (Character.isLetterOrDigit(cardCastString.charAt(cardCastString.length() - 1))) {
											newString.append('.');
										}

										//strs.add(newString.toString());
									//}
									//String text = StringUtils.join(strs, "");
									Card card = new Card(newString.toString());
									deck.getWhiteCards().add(card);
								//}
							}
						}
						
						database.cardPacks.put(ID, deck);
					} catch (IOException ex2) {
						logger.error(String.format("Pack (%s) didn't exist, skipping...",id));
						continue;
					}
					
				}
			}
			
		} catch (JsonIOException ex2) {
			ex2.printStackTrace();
		}
		return false;
	}
	
	private String getUrlContent(final String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(false);
		conn.setRequestMethod("GET");
		conn.setInstanceFollowRedirects(true);
		conn.setReadTimeout(GET_TIMEOUT);
		conn.setConnectTimeout(GET_TIMEOUT);

		final int code = conn.getResponseCode();
		if (HttpURLConnection.HTTP_OK != code) {
			CAHMod.logger.error(String.format("Got HTTP response code %d from AllBadCards for %s", code, urlStr));
			return null;
		}
		/**String contentType = conn.getContentType();
		if (!"application/json".equals(contentType)) {
			CAHMod.logger.error(String.format("Got content-type %s from Cardcast for %s", contentType, urlStr));
			return null;
		}*/

		InputStream is = conn.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(isr);
		StringBuilder builder = new StringBuilder(4096);
		String line;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append('\n');
		}
		reader.close();
		isr.close();
		is.close();

		return builder.toString();
	}
	
	public PackDatabase getDatabase(String id) {
		for (PackDatabase pack : INSTANCE.packDatabases)
			if (pack.id.equalsIgnoreCase(id))
				return pack;
		return null;
	}

	
}
