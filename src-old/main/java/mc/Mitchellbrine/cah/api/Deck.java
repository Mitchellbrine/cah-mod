package mc.Mitchellbrine.cah.api;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Mitchellbrine on 2015.
 */
public class Deck {
	private final String name;
	private final String code;
	private final String description;
	private String creator;
	private String databaseID;
	private final Set<String> blackCards = new HashSet<String>();
	private final Set<Card> whiteCards = new HashSet<Card>();

	public Deck(final String name, final String code, final String description) {
		this.name = name;
		this.code = code;
		this.description = description;
		this.creator = "missingno";
	}
	
	public Deck(final String name, final String code) {
		this.name = name;
		this.code = code;
		this.description = "missingno";
		this.creator = "missingno";
	}

	public int getId() {
		return -Integer.parseInt(code, 36);
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isActive() {
		return true;
	}

	public boolean isBaseDeck() {
		return false;
	}

	public int getWeight() {
		return Integer.MAX_VALUE;
	}

	public Set<String> getBlackCards() {
		return blackCards;
	}

	public Set<Card> getWhiteCards() {
		return whiteCards;
	}
	
	public String getCreator() {
		return creator;
	}
	
	public String setCreator(String newCreator) {
		return creator = newCreator;
	}
	
	public Deck setDatabase(PackDatabase pack) {
		return setDatabase(pack.id);
	}
	
	public Deck setDatabase(String packID) {
		this.databaseID = packID;
		return this;
	}
	
	public String getDatabaseID() {
		return this.databaseID;
	}
	
}
