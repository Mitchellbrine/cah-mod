package mc.Mitchellbrine.cah.gui;

import com.mojang.blaze3d.systems.RenderSystem;

import mc.Mitchellbrine.cah.api.Deck;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.list.ExtendedList;
public class DeckList extends ExtendedList<DeckList.DeckEntry> {

    private static String stripControlCodes(String value) { return net.minecraft.util.StringUtils.stripControlCodes(value); }
    private final int listWidth;

    private GuiDatabaseSelection parent;

    public DeckList(GuiDatabaseSelection parent, int listWidth, int top, int bottom)
    {
        super(parent.getMinecraft(), listWidth, parent.height, top, bottom, parent.getFontRenderer().FONT_HEIGHT * 2 + 8);
        this.parent = parent;
        this.listWidth = listWidth;
        this.refreshList();
    }
    
    @Override
    protected int getScrollbarPosition()
    {
        return this.listWidth;
    }

    @Override
    public int getRowWidth()
    {
        return this.listWidth;
    }

    public void refreshList() {
        this.clearEntries();
        parent.buildDeckList(this::addEntry, mod->new DeckEntry(mod, this.parent));
    }
    
    @Override
    protected void renderBackground()
    {
        this.parent.parentScreen.renderBackground(0);
    }
    
    public class DeckEntry extends ExtendedList.AbstractListEntry<DeckEntry> {
        private final Deck deck;
        private final GuiDatabaseSelection parent;

        DeckEntry(Deck deck, GuiDatabaseSelection parent) {
            this.deck = deck;
            this.parent = parent;
        }
        
        @Override
        public void render(int entryIdx, int top, int left, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean p_194999_5_, float partialTicks)
        {
            String name = stripControlCodes(deck.getName());
            String creator = stripControlCodes(deck.getCreator());
            FontRenderer font = this.parent.getFontRenderer();
            font.drawString(font.trimStringToWidth(name, listWidth),left + 3, top + 2, 0xFFFFFF);
            if (!creator.equalsIgnoreCase("missingno"))
            	font.drawString(font.trimStringToWidth(creator, listWidth), left + 3 , top + 2 + font.FONT_HEIGHT, 0xCCCCCC);
        }

        @Override
        public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_)
        {
            parent.setSelected(this);
            DeckList.this.setSelected(this);
            return false;
        }

        public Deck getDeck()
        {
            return deck;
        }
    }

	
}
