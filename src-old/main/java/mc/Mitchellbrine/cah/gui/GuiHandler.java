package mc.Mitchellbrine.cah.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GuiHandler {

	public static final class IDS {
		public static final int POINTS = 0;
		public static final int CARDS = 1;
		public static final int CHOICE = 2;
		public static final int CREATE = 3;
	}

	public static void openGui(int id, PlayerEntity player) {
		switch (id) {
			case IDS.POINTS:
				Minecraft.getInstance().displayGuiScreen(new GuiPoints(player));
				break;
			case IDS.CARDS:
				Minecraft.getInstance().displayGuiScreen(new GuiPlayerCards(player));
				break;
			case IDS.CHOICE:
				Minecraft.getInstance().displayGuiScreen(new GuiCardChoices(player));
				break;
			case IDS.CREATE:
				Minecraft.getInstance().displayGuiScreen(new GuiCreateGame(player));
				break;
		}
	}

}
