package mc.Mitchellbrine.cah.gui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.mojang.blaze3d.systems.RenderSystem;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.api.PackDatabase;
import mc.Mitchellbrine.cah.gui.DeckList.DeckEntry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.RenderComponentsUtil;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.client.gui.ScrollPanel;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.client.ConfigGuiHandler;
import net.minecraftforge.fml.loading.StringUtils;

public class GuiDatabaseSelection extends GuiCreationExtension {

	private static String stripControlCodes(String value) { return net.minecraft.util.StringUtils.stripControlCodes(value); }

    private enum SortType implements Comparator<Deck>
    {
        NORMAL,
        A_TO_Z{ @Override protected int compare(String name1, String name2){ return name1.compareTo(name2); }},
        Z_TO_A{ @Override protected int compare(String name1, String name2){ return name2.compareTo(name1); }};

        Button button;
        protected int compare(String name1, String name2){ return 0; }
        @Override
        public int compare(Deck o1, Deck o2) {
            String name1 = StringUtils.toLowerCase(stripControlCodes(o1.getCode()));
            String name2 = StringUtils.toLowerCase(stripControlCodes(o2.getCode()));
            return compare(name1, name2);
        }

        String getButtonText() {
            return I18n.format("fml.menu.mods."+StringUtils.toLowerCase(name()));
        }
    }
    
    private static final int PADDING = 6;
    
	public PackDatabase database;

	private final List<Deck> unsortedDecks;
	private List<Deck> decks; 
    private InfoPanel modInfo;
    private DeckList.DeckEntry selected = null;
    private int listWidth;
    private Button openModsFolderButton;
    private DeckList deckList;

    private int buttonMargin = 1;
    private int numButtons = SortType.values().length;
    private String lastFilterText = "";

    private TextFieldWidget search;

    private boolean sorted = false;
    private SortType sortType = SortType.NORMAL;

	
	public GuiDatabaseSelection(PlayerEntity player, String guiName, GuiCreateGame parent, PackDatabase database) {
		super(player, guiName, parent);
		
		this.database = database;
		this.unsortedDecks = Collections.unmodifiableList(database.getDecks());
		this.decks = Collections.unmodifiableList(this.unsortedDecks);
	}
	
    class InfoPanel extends ScrollPanel {
        private List<ITextComponent> lines = Collections.emptyList();

        InfoPanel(Minecraft mcIn, int widthIn, int heightIn, int topIn)
        {
            super(mcIn, widthIn, heightIn, topIn, deckList.getRight() + PADDING);
        }

        void setInfo(List<String> lines)
        {
            this.lines = resizeContent(lines);
        }

        void clearInfo()
        {
            this.lines = Collections.emptyList();
        }

        private List<ITextComponent> resizeContent(List<String> lines)
        {
            List<ITextComponent> ret = new ArrayList<ITextComponent>();
            for (String line : lines)
            {
                if (line == null)
                {
                    ret.add(null);
                    continue;
                }

                ITextComponent chat = ForgeHooks.newChatWithLinks(line, false);
                int maxTextLength = this.width - 12;
                if (maxTextLength >= 0)
                {
                    ret.addAll(RenderComponentsUtil.splitText(chat, maxTextLength, font, false, true));
                }
            }
            return ret;
        }

        @Override
        public int getContentHeight()
        {
            int height = 50;
            height += (lines.size() * font.FONT_HEIGHT);
            if (height < this.bottom - this.top - 8)
                height = this.bottom - this.top - 8;
            return height;
        }

        @Override
        protected int getScrollAmount()
        {
            return font.FONT_HEIGHT * 3;
        }

        @Override
        protected void drawPanel(int entryRight, int relativeY, Tessellator tess, int mouseX, int mouseY)
        {

            for (ITextComponent line : lines)
            {
                if (line != null)
                {
                    RenderSystem.enableBlend();
                    font.drawStringWithShadow(line.getFormattedText(), left + PADDING, relativeY, 0xFFFFFF);
                    RenderSystem.disableAlphaTest();
                    RenderSystem.disableBlend();
                }
                relativeY += font.FONT_HEIGHT;
            }

            final ITextComponent component = findTextLine(mouseX, mouseY);
            if (component!=null) {
                renderComponentHoverEffect(component, mouseX, mouseY);
            }
        }

        private ITextComponent findTextLine(final int mouseX, final int mouseY) {
            double offset = (mouseY - top) + border + scrollDistance + 1;
            if (offset <= 0)
                return null;

            int lineIdx = (int) (offset / font.FONT_HEIGHT);
            if (lineIdx >= lines.size() || lineIdx < 1)
                return null;

            ITextComponent line = lines.get(lineIdx-1);
            if (line != null)
            {
                int k = left + border;
                for (ITextComponent part : line) {
                    if (!(part instanceof StringTextComponent))
                        continue;
                    k += font.getStringWidth(((StringTextComponent)part).getText());
                    if (k >= mouseX)
                    {
                        return part;
                    }
                }
            }
            return null;
        }

        @Override
        public boolean mouseClicked(final double mouseX, final double mouseY, final int button) {
            final ITextComponent component = findTextLine((int) mouseX, (int) mouseY);
            if (component != null) {
                GuiDatabaseSelection.this.handleComponentClicked(component);
                return true;
            }
            return super.mouseClicked(mouseX, mouseY, button);
        }

        @Override
        protected void drawBackground() {
        }
    }

	
	@Override
	public void init() {
		super.init();	
        for (Deck mod : unsortedDecks)
        {
            listWidth = Math.max(listWidth,font.getStringWidth(mod.getName()) + 10);
            listWidth = Math.max(listWidth,font.getStringWidth(mod.getCreator()) + 5);
        }
        listWidth = Math.max(Math.min(listWidth, width/3), 100);
        listWidth += listWidth % numButtons != 0 ? (numButtons - listWidth % numButtons) : 0;

        int modInfoWidth = this.width - this.listWidth - (PADDING * 3);
        int doneButtonWidth = Math.min(modInfoWidth, 200);
        int y = this.height - 20 - PADDING;
        this.addButton(new Button(((listWidth + PADDING + this.width - doneButtonWidth) / 2), y, doneButtonWidth, 20,
                I18n.format("gui.done"), b -> GuiDatabaseSelection.this.onClose()));
        y -= 20 + PADDING;
        this.addButton(this.openModsFolderButton = new Button(6, y, this.listWidth, 20,
                I18n.format("gui.cah.addPack"), new DeckButton()));
        y -= 14 + PADDING + 1;
        search = new TextFieldWidget(font, PADDING + 1, y, listWidth - 2, 14, I18n.format("fml.menu.mods.search"));

        int fullButtonHeight = PADDING + 20 + PADDING;
        this.deckList = new DeckList(this, listWidth, fullButtonHeight, search.y - font.FONT_HEIGHT - PADDING);
        this.deckList.setLeftPos(6);

        this.modInfo = new InfoPanel(this.minecraft, modInfoWidth, this.height - PADDING - fullButtonHeight, PADDING);

        children.add(search);
        children.add(deckList);
        children.add(modInfo);
        search.setFocused2(false);
        search.setCanLoseFocus(true);

        final int width = listWidth / numButtons;
        int x = PADDING;
        addButton(SortType.NORMAL.button = new Button(x, PADDING, width - buttonMargin, 20, SortType.NORMAL.getButtonText(), b -> resortMods(SortType.NORMAL)));
        x += width + buttonMargin;
        addButton(SortType.A_TO_Z.button = new Button(x, PADDING, width - buttonMargin, 20, SortType.A_TO_Z.getButtonText(), b -> resortMods(SortType.A_TO_Z)));
        x += width + buttonMargin;
        addButton(SortType.Z_TO_A.button = new Button(x, PADDING, width - buttonMargin, 20, SortType.Z_TO_A.getButtonText(), b -> resortMods(SortType.Z_TO_A)));
        resortMods(SortType.NORMAL);
        updateCache();
        
	
	}
	
	@Override
	public void tick() {
		super.tick();
		search.tick();
		deckList.setSelected(selected);

        if (!search.getText().equals(lastFilterText))
        {
            reloadMods();
            sorted = false;
        }

        if (!sorted)
        {
            reloadMods();
            decks.sort(sortType);
            deckList.refreshList();
            if (selected != null)
            {
                selected = deckList.children().stream().filter(e -> e == selected).findFirst().orElse(null);
                updateCache();
            }
            sorted = true;
        }
	}
	
	@Override
    public void render(int mouseX, int mouseY, float partialTicks)
    {
		this.renderBackground(0);
        this.deckList.render(mouseX, mouseY, partialTicks);
        if (this.modInfo != null)
            this.modInfo.render(mouseX, mouseY, partialTicks);

        String text = I18n.format("fml.menu.mods.search");
        int x = deckList.getLeft() + ((deckList.getRight() - deckList.getLeft()) / 2) - (getFontRenderer().getStringWidth(text) / 2);
        getFontRenderer().drawString(text, x, search.y - getFontRenderer().FONT_HEIGHT, 0xFFFFFF);
        this.search.render(mouseX, mouseY, partialTicks);
        super.render(mouseX, mouseY, partialTicks);
    }
	
	private void reloadMods()
    {
        this.decks = this.unsortedDecks.stream().
                filter(mi->StringUtils.toLowerCase(stripControlCodes(mi.getCode())).contains(StringUtils.toLowerCase(search.getText()))).collect(Collectors.toList());
        lastFilterText = search.getText();
    }

    private void resortMods(SortType newSort)
    {
        this.sortType = newSort;

        for (SortType sort : SortType.values())
        {
            if (sort.button != null)
                sort.button.active = sortType != sort;
        }
        sorted = false;
    }
    
    public void setSelected(DeckEntry deckEntry)
    {
        this.selected = deckEntry == this.selected ? null : deckEntry;
        updateCache();
    }

    private void updateCache()
    {
        if (selected == null) {
            this.openModsFolderButton.active = false;
            this.modInfo.clearInfo();
            return;
        }
        
        Deck deck = selected.getDeck();
        this.openModsFolderButton.active = selected != null && !parentScreen.getDecks().contains(deck);
        
        List<String> lines = new ArrayList<>();

        lines.add(deck.getName());
        lines.add(I18n.format("cah.mod.database.code", deck.getCode()));
        if (!deck.getCreator().equalsIgnoreCase("missingno"))
        	lines.add(I18n.format("cah.mod.database.author",selected.getDeck().getCreator()));
        lines.add(null);
        if (deck.getDescription() != "missingno")
        	lines.add(selected.getDeck().getDescription());

       modInfo.setInfo(lines);
    }

    @Override
    public void resize(Minecraft mc, int width, int height)
    {
        String s = this.search.getText();
        SortType sort = this.sortType;
        DeckEntry selected = this.selected;
        this.init(mc, width, height);
        this.search.setText(s);
        this.selected = selected;
        if (!this.search.getText().isEmpty())
            reloadMods();
        if (sort != SortType.NORMAL)
            resortMods(sort);
        updateCache();
    }
    
	@Override
	public boolean isPauseScreen() {
		return false;
	}

    
    public <T extends ExtendedList.AbstractListEntry<T>> void buildDeckList(Consumer<T> modListViewConsumer, Function<Deck, T> newEntry)
    {
        decks.forEach(mod->modListViewConsumer.accept(newEntry.apply(mod)));
    }
    
    public FontRenderer getFontRenderer() {
    	return font;
    }
	
	public class DeckButton implements IPressable {

		public DeckButton() {
		}
		
		@Override
		public void onPress(Button p_onPress_1_) {
			if (selected != null) {
				Deck deck = selected.getDeck();
				CAHMod.logger.info(deck.getWhiteCards().size());
				if (deck != null && parentScreen.loadDeck(deck) != null) {
					p_onPress_1_.active = false;
				} else {
					p_onPress_1_.active = true;
				}
			}
		}
		
	}

}
