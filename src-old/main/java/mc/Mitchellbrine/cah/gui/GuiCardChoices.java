package mc.Mitchellbrine.cah.gui;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.client.CAHCachedData;
import mc.Mitchellbrine.cah.network.BlackCardRequestPacket;
import mc.Mitchellbrine.cah.network.CardRequestPacket;
import mc.Mitchellbrine.cah.network.CardTextRequestPacket;
import mc.Mitchellbrine.cah.network.GameRequestPacket;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.network.WhiteCardsRequestPacket;
import mc.Mitchellbrine.cah.network.WinningCardPacket;
import mc.Mitchellbrine.cah.util.References;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GuiCardChoices extends Screen {

	private PlayerEntity player;
	private int indexChosen = -1;

	private ResourceLocation blackCard = new ResourceLocation(References.MODID + ":textures/gui/blackCard.png".toLowerCase());

	public GuiCardChoices(PlayerEntity player) {
		super(new StringTextComponent("Card Choices"));
		this.player = player;
	}

	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		for (int i = 0; i < CAHCachedData.whiteCards;i++) {
			this.addButton(new IdButton(i,30*(i+1),30,20,20,(i + 1) + "",new CardButton()));
		}

	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		this.renderBackground();

		GL11.glColor4f(1F, 1F, 1F, 1F);

		drawCenteredString(font,"Choose a Winner",width / 2,4,0xFFFFFF);
		drawCenteredString(font,"(Click to view, Shift-Click to select winner)",width / 2,12,0xFFFFFF);

		drawModalRectWithCustomSizedTexture(width / 2 - 42, 50, 74, 128,blackCard);


		GL11.glScalef(0.5F,0.5F,0.5F);
		if (indexChosen == -1 || CAHCachedData.lastWhiteCard == null || CAHCachedData.lastWhiteCard.size() == 0) {
			font.drawSplitString(CAHCachedData.blackCard, (width / 2 - 24) * 2 + 3, 110, 128, 0xFFFFFF);
		} else {
			int index = 0;
			String finalString = CAHCachedData.blackCard;
			while (index < CAHCachedData.lastWhiteCard.size()) {
				finalString = finalString.replaceFirst("______",CAHCachedData.lastWhiteCard.get(index));
				index++;
			}
			if (finalString.endsWith("..")) {
				finalString = finalString.substring(0,finalString.length() - 1);
			}
			font.drawSplitString(finalString, (width / 2 - 24) * 2 + 3, 110, 128, 0xFFFFFF);
		}
		GL11.glScalef(2F,2F,2F);

		super.render(mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	protected class CardButton implements Button.IPressable {
	
		public void onPress(Button button) {
			if (hasShiftDown()) {
				if (CAHCachedData.isCzar) {
					PacketHandler.INSTANCE.sendToServer(new WinningCardPacket(indexChosen));
					minecraft.currentScreen.onClose();
					minecraft.player.closeScreen();
				} else {
					//PacketHandler.INSTANCE.sendToServer(new CardMessagePacket(player.getCommandSenderName(),p_146284_1_.id));
					PacketHandler.INSTANCE.sendToServer(new CardTextRequestPacket(((IdButton)button).ID));
					indexChosen = ((IdButton)button).ID;
					//CAHMod.logger.info("Index chosen: " + indexChosen);
				}
			} else {
				//PacketHandler.INSTANCE.sendToServer(new CardMessagePacket(player.getCommandSenderName(),p_146284_1_.id));
				PacketHandler.INSTANCE.sendToServer(new CardTextRequestPacket(((IdButton)button).ID));
				indexChosen = ((IdButton)button).ID;
				//CAHMod.logger.info("Index chosen: " + indexChosen);
			}
		}
	}

	@Override
	public void onClose() {
		super.onClose();
		PacketHandler.INSTANCE.sendToServer(new CardRequestPacket(getMinecraft().player.getScoreboardName()));
		PacketHandler.INSTANCE.sendToServer(new GameRequestPacket(getMinecraft().player.getScoreboardName()));
		PacketHandler.INSTANCE.sendToServer(new BlackCardRequestPacket(getMinecraft().player.getScoreboardName()));
		PacketHandler.INSTANCE.sendToServer(new WhiteCardsRequestPacket(getMinecraft().player.getScoreboardName()));
	}

	@Override
	public void tick() {
		super.tick();
		if (CAHCachedData.roundState < 1) {
			getMinecraft().currentScreen.onClose();
			getMinecraft().player.closeScreen();
		}
	}


	private void drawTexturedRect(ResourceLocation texture, double x, double y, int u, int v, int width, int height, int imageWidth, int imageHeight, double scale) {
		//minecraft.gameRenderer.bindTexture(texture);

		double minU = u / imageWidth;
		double maxU = (u + width) / imageWidth;
		double minV = v / imageHeight;
		double maxV = (v + height) / imageHeight;

        RenderSystem.color4f(1F,1F,1F,1F);
        getMinecraft().getTextureManager().bindTexture(texture);

        //int x = (width - this.xSize) / 2;
        //int y = (height - this.ySize) / 2;
		blit(((int)x), ((int)y), u, v, (width * (int)scale), (height * (int)scale));
		
		
/**		Tessellator tesselator = Tessellator.getInstance();
		BufferBuilder buffer = tesselator.getBuffer();
		buffer.startDrawingQuads();
		blit(x + scale * width, y + scale * height, 0, maxU, maxV,);
		tesselator.addVertexWithUV(x + scale*width,y, 0,maxU,minV);
		tesselator.addVertexWithUV(x,y, 0,minU,minV);
		tesselator.addVertexWithUV(x,y + scale*height, 0,minU,maxV);
		tesselator.draw();*/
	}
	
    public void drawModalRectWithCustomSizedTexture(double leftSideX, double topY, double width, double height, ResourceLocation texture) {
        double rightSideX = leftSideX + width;
        double bottomY = topY + height;
    	
        double generalOffset = 12D;
        RenderSystem.enableBlend();
        RenderSystem.disableDepthTest();
    	RenderSystem.depthMask(false);
    	RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.disableAlphaTest();
        minecraft.getTextureManager().bindTexture(texture);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
//botlef  bufferbuilder.pos(0.0D, (double)mc.mainWindow.getScaledHeight(), -90.0D).tex(0.0D, 1.0D).endVertex();
        bufferbuilder.pos(leftSideX + generalOffset, bottomY, -90.0D).tex(0.0F, 1.0F).endVertex();
        
//botr    bufferbuilder.pos((double)mc.mainWindow.getScaledWidth(), (double)mc.mainWindow.getScaledHeight(), -90.0D).tex(1.0D, 1.0D).endVertex();
        bufferbuilder.pos(rightSideX + generalOffset, bottomY, -90.0D).tex(1.0F, 1.0F).endVertex();
        
//topr      bufferbuilder.pos((double)mc.mainWindow.getScaledWidth(), 0.0D, -90.0D).tex(1.0D, 0.0D).endVertex();
        bufferbuilder.pos(rightSideX + generalOffset, topY, -90.0D).tex(1.0F, 0.0F).endVertex();
        
//topl    bufferbuilder.pos(0.0D, 0.0D, -90.0D).tex(0.0D, 0.0D).endVertex();
        bufferbuilder.pos(leftSideX + generalOffset, topY, -90.0D).tex(0.0F, 0.0F).endVertex();

        tessellator.draw();
        RenderSystem.depthMask(true);
        RenderSystem.enableDepthTest();
        RenderSystem.enableAlphaTest();
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
  }

}
