package mc.Mitchellbrine.cah.gui;

import mc.Mitchellbrine.cah.api.CardpackHandler;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.api.PackDatabase;
import mc.Mitchellbrine.cah.cardcast.CardcastReader;
import mc.Mitchellbrine.cah.client.CAHCachedData;
import mc.Mitchellbrine.cah.network.GameCreationPacket;
import mc.Mitchellbrine.cah.network.GameCreationWithDecksPacket;
import mc.Mitchellbrine.cah.network.PacketHandler;
import mc.Mitchellbrine.cah.util.FeaturedPacks;
import net.minecraft.client.MouseHelper;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.StringTextComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.lwjgl.glfw.GLFW;

/**
 * Created by Mitchellbrine on 2015.
 */
public class GuiCreateGame extends Screen {

	private PlayerEntity player;

	private Button doneButton;
	private TextFieldWidget codeText;
	private TextFieldWidget maxPlayers;
	private Button addButton;
	private Button cancelButton;
	private Button rememberPacks;
	
	private TextFieldWidget bots;
	
	private String currentDeck;
	private String currentMax;
	private String botsNum;

	private List<Deck> decks;
	
	private List<Deck> featuredDecks;
	
	private GuiCreateGame game;

	private final CardcastReader reader = new CardcastReader();

	private static final String[] issues = new String[]{"You can't create this game for either 2 reasons: ","","1. You don't have any packs selected","or","2. A game is already in-session!"};

	public GuiCreateGame(PlayerEntity player) {
		super(new StringTextComponent("Create CAH Game"));
		this.player = player;
		decks = new ArrayList<Deck>();
		featuredDecks = new ArrayList<Deck>();
	}

	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
	    this.minecraft.keyboardListener.enableRepeatEvents(true);

		addButton(doneButton = new Button(this.width / 2 - 4 - 150, this.height / 4 + 120 + 35, 150, 20, I18n.format("gui.done"),new createGame()));
		addButton(cancelButton = new Button(this.width / 2 + 4, this.height / 4 + 120 + 35, 150, 20, I18n.format("gui.cancel"),new cancelGame()));

		this.codeText = new TextFieldWidget(font, this.width / 2 - 75, 10,150,20,"CAH Codes");
		this.codeText.setMaxStringLength(5);
		codeText.setCanLoseFocus(true);
		codeText.setResponder((responder) -> {
			this.currentDeck = codeText.getText();
		});
		children.add(codeText);
		
		this.maxPlayers = new TextFieldWidget(font,this.width / 2 + 4, this.height / 4 + 120 + 10, 20, 20,"Set Max Points");
		this.maxPlayers.setMaxStringLength(2);
		this.maxPlayers.setVisible(true);
		maxPlayers.setCanLoseFocus(true);
		this.maxPlayers.setText("8");
		maxPlayers.setResponder((responder) -> {
			this.currentMax = maxPlayers.getText();
		});
		children.add(maxPlayers);
		
		bots = new TextFieldWidget(font,this.width / 2 + 30, this.height / 4 + 120 + 10, 20, 20,"Set Bots Num");
		this.bots.setMaxStringLength(2);
		this.bots.setText("0");
		this.bots.setVisible(player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		this.bots.setEnabled(player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		this.bots.setCanLoseFocus(true);
		bots.setResponder((responder) -> {
			this.botsNum = bots.getText();
		});
		children.add(bots);
		
		this.setFocusedDefault(codeText);
		
		addButton(addButton = new Button(this.width / 2 + 77,10,75,20,I18n.format("gui.cah.addPack"), new addPack()));
		
		addButton(rememberPacks = new Button(this.width / 2 - 105,10,20,20,"*", new openRememberPacks()));
		this.rememberPacks.visible = (player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		this.rememberPacks.active = (player.getScoreboardName().equalsIgnoreCase("Dev") || player.getScoreboardName().equalsIgnoreCase("Mitchellbrine") || player.getScoreboardName().equalsIgnoreCase("LordSkittles_") || player.getScoreboardName().equalsIgnoreCase("kkaylium"));
		
		/**if (FeaturedPacks.getFeaturedPacks() != null) {
			for (Deck deck : FeaturedPacks.getFeaturedPacks()) {
				if (deck != null && !featuredDecks.contains(deck))
					featuredDecks.add(deck);
			}
		}*/
		
		markGuiDirty();
	}

	public void render(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
		this.renderBackground();
		
		//this.scriptLine.drawTextBox();

		//drawRect(this.width / 2 - 151, 50, this.width / 2 + 152, 188, -16777216);
		
		codeText.render(p_73863_1_, p_73863_2_, p_73863_3_);
		maxPlayers.render(p_73863_1_, p_73863_2_, p_73863_3_);
		bots.render(p_73863_1_, p_73863_2_, p_73863_3_);
		
		drawRightAlignedString(font, maxPlayers.getMessage(), maxPlayers.x - maxPlayers.getMessage().length(), maxPlayers.y + (maxPlayers.getHeight() / 4), 0xFFFFFF);

		
		int yPosition = 52;
		/**for (Deck deck : game.featuredDecks) {
			font.drawStringWithShadow("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)", this.width / 2 - 150, yPosition, /*0x3ADF00*/ /**0xFFFFFF);
			yPosition += 10;			
		}*/
		
		
		/**for (Deck deck : CardpackHandler.INSTANCE.getDatabase("official").getDecks()) {
			font.drawStringWithShadow("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)", this.width / 2 - 150, yPosition, /*0x3ADF00*/ /**0xFFFFFF);
			yPosition += 10;
		}*/
		
		for (Deck deck : game.decks) {
			font.drawStringWithShadow("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)", this.width / 2 - 150, yPosition, /*0x3ADF00*/ 0xFFFFFF);
			yPosition += 10;
		}

		super.render(p_73863_1_, p_73863_2_, p_73863_3_);
		
		if (!doneButton.active && p_73863_1_ >= doneButton.x && p_73863_1_ <= doneButton.x + doneButton.getWidth() && p_73863_2_ >= doneButton.y && p_73863_2_ <= doneButton.y + doneButton.getHeight()) {
			renderTooltip(Arrays.asList(issues), p_73863_1_, p_73863_2_);
			
		}
		if (p_73863_1_ >= maxPlayers.x && p_73863_1_ <= (maxPlayers.x + maxPlayers.getWidth()) && p_73863_2_ >= maxPlayers.y && p_73863_2_ <= (maxPlayers.y + maxPlayers.getHeight())) {
			List<String> strings = new ArrayList<String>();
			strings.add("Max Points");
			renderTooltip(strings,p_73863_1_,p_73863_2_);
		}
		
		if (bots.isMouseOver(p_73863_1_, p_73863_2_)) {
			renderTooltip("# of Bots", p_73863_1_,p_73863_2_);
		}
		
	}

	public void tick()
	{
		//this.scriptLine.updateCursorCounter();
		super.tick();
		codeText.tick();
		maxPlayers.tick();
		bots.tick();
		this.doneButton.active = (this.decks.size() > 0) && !CAHCachedData.serverExists;
		if (!this.maxPlayers.getText().isEmpty()) {
			try {
				Integer.parseInt(this.currentMax);
			} catch (NumberFormatException ex) {
				this.maxPlayers.setText("8");
			}
		}
	}
	

	@SuppressWarnings("unchecked")
	public boolean keyPressed(int charac, int arg2, int arg3) {
		codeText.keyPressed(charac, arg2, arg3);
		maxPlayers.keyPressed(charac, arg2, arg3);
		if (charac == GLFW.GLFW_KEY_ESCAPE) {
			cancelButton.onPress();
			return true;
		} else if (charac == GLFW.GLFW_KEY_ENTER) {
			if (!this.codeText.getText().isEmpty()) {
				addButton.onPress();
			}
			else {
				doneButton.onPress();

			}
			return true;
		} else {
			return super.keyPressed(charac, arg2, arg3);
		}
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	protected class createGame implements IPressable {

		@Override
		public void onPress(Button button) {
			List<String> packCodes = new ArrayList<String>();
			/**for (Deck deck : decks) {
				//System.out.println("- " + deck.getName() + " (" + deck.getBlackCards().size() + " black cards, " + deck.getWhiteCards().size() + " white cards)");
				packCodes.add(deck.getCode());
			}*/
			//PacketHandler.INSTANCE.sendToServer(new GameCreationPacket(player.getScoreboardName(),Integer.parseInt(currentMax),botsNum != null ? Integer.parseInt(botsNum) : 0,packCodes));
			PacketHandler.INSTANCE.sendToServer(new GameCreationWithDecksPacket(player.getScoreboardName(),Integer.parseInt(currentMax),botsNum != null ? Integer.parseInt(botsNum) : 0, decks));
			player.closeScreen();

		}
		
	}
	
	protected class addPack implements IPressable {

		@Override
		public void onPress(Button button) {
			if (!codeText.getText().isEmpty()) {
				loadDeck(codeText);
			}
		}
		
	}
	
	public Deck loadDeck(String deckCode) {
		return reader.loadSet(deckCode);
	}
	
	public Deck loadDeck(Deck deck) {
		if (deck != null) {
			decks.add(deck);
			markGuiDirty();
			return deck;
		}
		return null;
	}
	
	private Deck loadDeck(TextFieldWidget textField) {
		Deck deck = reader.loadSet(textField.getText());
		if (deck != null) {
			decks.add(deck);
			textField.setText("");
			markGuiDirty();
		}
		return deck;
	}
	
	public List<Deck> getDecks() {
		return decks;
	}
	
	protected class cancelGame implements IPressable {

		@Override
		public void onPress(Button button) {
			onClose();
			minecraft.player.closeScreen();
		}
		
	}
	
	protected class GuiTextFieldButton extends Button {
		
		public TextFieldWidget textField;
		public IPressable hook;
		
		public GuiTextFieldButton(TextFieldWidget field, Button.IPressable iHook) {
			super(field.x,field.y,field.getWidth(),field.getHeight(),"", iHook);
		}
					
		}

	public class utilizeGuiTextHooks implements IPressable {
	
		public TextFieldWidget textF;
	
		public utilizeGuiTextHooks(TextFieldWidget text) {
			textF = text;
		}
	
		public void onPress(Button button) {
			textF.onClick(minecraft.mouseHelper.getMouseX(),minecraft.mouseHelper.getMouseY());
		}
	}
	
	public class openRememberPacks implements IPressable {
		public void onPress(Button button) {
			//minecraft.displayGuiScreen(new GuiRememberPacks(player,game));
			PackDatabase database = CardpackHandler.INSTANCE.getDatabase("official");
			minecraft.displayGuiScreen(new GuiDatabaseSelection(player,database.name,GuiCreateGame.this,database));
		}
	}
	
	public void markGuiDirty() {
		this.game = this;
	}

}
