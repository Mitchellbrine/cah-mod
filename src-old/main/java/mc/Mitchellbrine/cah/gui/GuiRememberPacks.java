package mc.Mitchellbrine.cah.gui;

import java.util.List;

import org.lwjgl.opengl.GL11;

import mc.Mitchellbrine.cah.CAHMod;
import mc.Mitchellbrine.cah.api.Deck;
import mc.Mitchellbrine.cah.client.CAHCachedData;
import mc.Mitchellbrine.cah.util.FeaturedPacks;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;

public class GuiRememberPacks extends Screen {

	private GuiCreateGame parentScreen;
	private PlayerEntity player;
	
	private Button basePack;
	private Button JohnMulaneyPack;
	private Button featured1, featured2, featured3;
	private Deck[] featuredDecks;
	
	protected GuiRememberPacks(PlayerEntity player, GuiCreateGame parent) {
		super(new StringTextComponent("Check Packs"));
		this.player = player;
		this.parentScreen = parent;
				
		featuredDecks = new Deck[CAHCachedData.featuredDecks.size()];
		
		for (int i = 0; i < featuredDecks.length;i++) {
			featuredDecks[i] = CAHCachedData.featuredDecks.get(i);
		}
	}
	
	public void init() {
		super.init();
		
		//CAHMod.logger.info(this.width);
		
	    this.minecraft.keyboardListener.enableRepeatEvents(true);

	    this.addButton(basePack = new Button(10,50,20,20,"+",new IPressable() {

			@Override
			public void onPress(Button p_onPress_1_) {
				p_onPress_1_.active = false;
				Deck deck = parentScreen.loadDeck("CB5Q3");
				if (deck != null) {
					parentScreen.getDecks().add(deck);
				} else {
					p_onPress_1_.active = true;
				}
			}
	    	
	    }));
	    
	    this.addButton(JohnMulaneyPack = new Button(10,80,20,20,"+",new IPressable() {

			@Override
			public void onPress(Button p_onPress_1_) {
				p_onPress_1_.active = false;
				Deck deck = parentScreen.loadDeck("299TV");
				if (deck != null) {
					parentScreen.getDecks().add(deck);
				} else {
					p_onPress_1_.active = true;
				}
			}
	    	
	    }));
	    
	    for (int i = 0; i < featuredDecks.length;i++) {
	    	if (featuredDecks[i] != null) {
	    		if (i == 0) {
	    			addButton(featured1 = new Button((this.width / 2) + 10,50,20,20,"+",new DeckButton(featuredDecks[i])));
	    		} else if (i == 1) {
	    			addButton(featured2 = new Button((this.width / 2) + 10,80,20,20,"+",new DeckButton(featuredDecks[i])));
	    		} else if (i == 2) {
	    			addButton(featured3 = new Button((this.width / 2) + 10,110,20,20,"+",new DeckButton(featuredDecks[i])));
	    		}
	    	}
	    }
	    
	    addButton(new Button(this.width / 2 - 75, this.height / 4 + 155, 150, 20, I18n.format("gui.done"), new IPressable() {

			@Override
			public void onPress(Button p_onPress_1_) {
				onClose();
			}
	    	
	    }));
	    
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		this.renderBackground();

		GL11.glColor4f(1F,1F,1F,1F);
		
		drawCenteredString(font,"Saved Packs",width / 2, 4,0xFFFFFF);
		drawCenteredString(font,"(Press + to add to game)",width / 2, 12,0xFFFFFF);
		
		drawCenteredString(font,"Mitchellbrine's Favorite Packs", width/ 4, 30, 0xFFFFFF);
		drawCenteredString(font,"Featured Packs", 3* width/ 4, 30, 0xFFFFFF);
		
		font.drawStringWithShadow("Base Pack",35F,55F,0xFFFFFF);
		font.drawStringWithShadow("John Mulaney Pack",35F,85F,0xFFFFFF);
		
		for (int i = 0; i < featuredDecks.length;i++) {
			if (featuredDecks[i] != null) {
					font.drawStringWithShadow(featuredDecks[i].getName(), (width/2) + 35F, 55F + (30 * i), 0xFFFFFF);
			}
		}
		
		super.render(mouseX, mouseY, partialTicks);
	}
	
	@Override
	public boolean isPauseScreen() {
		return false;
	}
	
	@Override
	public void onClose() {
		minecraft.displayGuiScreen(parentScreen);
	}

	@Override
	public void tick() {
		super.tick();
		
		for (Deck deck : parentScreen.getDecks()) {
			if (JohnMulaneyPack.active && deck.getCode().equalsIgnoreCase("299TV")) {
				JohnMulaneyPack.active = false;
			} else if (basePack.active && deck.getCode().equalsIgnoreCase("CB5Q3")) {
				basePack.active = false;
			} else if (featured1.active && 0 < featuredDecks.length) {
				if (featuredDecks[0] != null && deck.getCode().equalsIgnoreCase(featuredDecks[0].getCode()))
					featured1.active = false;
			} else if (featured2.active && 1 < featuredDecks.length) {
				if (featuredDecks[1] != null && deck.getCode().equalsIgnoreCase(featuredDecks[1].getCode()))
					featured2.active = false;
			} else if (featured3.active && 2 < featuredDecks.length) {
				if (featuredDecks[2] != null && deck.getCode().equalsIgnoreCase(featuredDecks[2].getCode()))
					featured3.active = false;
			}
		}
	}
	
	public class DeckButton implements IPressable {

		public Deck deck;
		
		public DeckButton(Deck pushedButton) {
			deck = pushedButton;
		}
		
		@Override
		public void onPress(Button p_onPress_1_) {
			if (deck != null && parentScreen.getDecks().add(deck)) {
				p_onPress_1_.active = false;
			} else {
				p_onPress_1_.active = true;
			}
		}
		
	}

}
