package mc.Mitchellbrine.cah.gui;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;

public class GuiCreationExtension extends Screen {

	public GuiCreateGame parentScreen;
	public PlayerEntity player;
	
	public GuiCreationExtension (PlayerEntity player, String guiName, GuiCreateGame parent) {
		super(new StringTextComponent(guiName));
		this.player = player;
		this.parentScreen = parent;
	}
	
	public void init() {
		super.init();
		//CAHMod.logger.info(this.width);
	    this.minecraft.keyboardListener.enableRepeatEvents(true);
	}
	
	@Override
	public boolean isPauseScreen() {
		return false;
	}
	
	@Override
	public void onClose() {
		minecraft.displayGuiScreen(parentScreen);
	}

	@Override
	public void tick() {
		super.tick();	
	}
	
}
