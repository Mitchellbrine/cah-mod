package mc.Mitchellbrine.cah.gui;

import net.minecraft.client.gui.widget.button.Button;

public class IdButton extends Button {

	public int ID;
	public boolean sent;
	
	public IdButton(int id, int widthIn, int heightIn, int width, int height, String text, Button.IPressable onPress) {
		super (widthIn, heightIn, width, height, text, onPress);
		ID = id;
		sent = false;
	}
	
}
