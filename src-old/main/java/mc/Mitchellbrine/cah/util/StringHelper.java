package mc.Mitchellbrine.cah.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mitchellbrine on 2015.
 */
public class StringHelper {

	public static List<String> getSeperatedString(net.minecraft.client.gui.FontRenderer renderer, List<String> seperated, int maxLength) {
		List<String> strings = new ArrayList<String>();
		StringBuilder builder = new StringBuilder();
		for (String string : seperated) {
			if (renderer.getStringWidth(builder + string) < maxLength || (seperated.indexOf(string) == seperated.size() - 1)) {
				builder.append(string);
				builder.append(" ");
			} else {
				strings.add(builder.toString());
				builder.setLength(0);
			}
		}
		return strings;
	}

	public static List<String> seperateBySpace(String string) {
		return Arrays.asList(string.split(" "));
	}

	public static String replaceFirst(String stringToUse, CharSequence target, CharSequence replacement) {
		return Pattern.compile(target.toString(), Pattern.LITERAL).matcher(
			stringToUse).replaceFirst(Matcher.quoteReplacement(replacement.toString()));
	}

}
